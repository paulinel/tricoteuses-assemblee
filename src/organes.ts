import { CodeTypeOrgane, Organe } from "./types/acteurs_et_organes"

export function shortNameFromOrgane(organe: Organe | undefined | null): string {
  if (organe === null || organe === undefined) {
    return "Organe inconnu"
  }
  switch (organe.codeType) {
    case CodeTypeOrgane.Cmp:
      return "Commission mixte paritaire"
    case CodeTypeOrgane.Gouvernement:
      return organe.libelle
    default:
      return organe.libelleAbrege
  }
}
