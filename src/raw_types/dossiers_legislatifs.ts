// To parse this data:
//
//   import { Convert, Document, DossierParlementaire, DossiersLegislatifs } from "./file";
//
//   const document = Convert.toDocument(json);
//   const dossierParlementaire = Convert.toDossierParlementaire(json);
//   const dossiersLegislatifs = Convert.toDossiersLegislatifs(json);
//
// These functions will throw an error if the JSON doesn't
// match the expected interface, even if the JSON is valid.

export interface Document {
    document: DocumentDocument;
}

export interface DocumentDocument {
    "@xmlns":                 string;
    "@xmlns:xsi":             string;
    "@xsi:type":              DocumentXsiType;
    uid:                      string;
    legislature:              null | string;
    cycleDeVie:               CycleDeVie;
    denominationStructurelle: DocumentDenominationStructurelle;
    provenance?:              Provenance;
    titres:                   Titres;
    divisions:                PurpleDivisions | null;
    dossierRef:               string;
    redacteur:                null;
    classification:           Classification;
    auteurs:                  DocumentAuteurs;
    correction:               Correction | null;
    notice:                   Notice;
    indexation:               null;
    imprimerie:               Imprimerie | null;
    coSignataires?:           CoSignataires | null;
    depotAmendements?:        DepotAmendements | null;
}

export enum DocumentXsiType {
    AccordInternationalType = "accordInternational_Type",
    AvisConseilEtatType = "avisConseilEtat_Type",
    DocumentEtudeImpactType = "documentEtudeImpact_Type",
    RapportParlementaireType = "rapportParlementaire_Type",
    TexteLoiType = "texteLoi_Type",
}

export interface DocumentAuteurs {
    auteur: AuteurElement[] | AuteurElement;
}

export interface AuteurElement {
    acteur?: AuteurActeur;
    organe?: AuteurOrgane;
}

export interface AuteurActeur {
    acteurRef: string;
    qualite:   Qualite;
}

export enum Qualite {
    Auteur = "auteur",
    Rapporteur = "rapporteur",
    RapporteurGénéral = "rapporteur général",
    RapporteurPourAvis = "rapporteur pour avis",
    RapporteurSpécial = "rapporteur spécial",
}

export interface AuteurOrgane {
    organeRef: string;
}

export interface Classification {
    famille:        Famille | null;
    type:           ProcedureParlementaire;
    sousType:       SousType | null;
    statutAdoption: StatutAdoption | null;
}

export interface Famille {
    depot:   ProcedureParlementaire;
    classe:  ProcedureParlementaire;
    espece?: ProcedureParlementaire;
}

export interface ProcedureParlementaire {
    code:    string;
    libelle: string;
}

export interface SousType {
    code:            Code;
    libelle?:        string;
    libelleEdition?: string;
}

export enum Code {
    Accpresrp = "ACCPRESRP",
    Appart1515 = "APPART1515",
    Appart341 = "APPART341",
    Apploi = "APPLOI",
    Aue = "AUE",
    Autratconv = "AUTRATCONV",
    Comenq = "COMENQ",
    Compa = "COMPA",
    Comspcpte = "COMSPCPTE",
    Const = "CONST",
    Ctrlbudg = "CTRLBUDG",
    Divers = "DIVERS",
    Engresptxt = "ENGRESPTXT",
    Enqu = "ENQU",
    Fin = "FIN",
    Finrect = "FINRECT",
    Finssoc = "FINSSOC",
    Finssocrec = "FINSSOCREC",
    Impactlois = "IMPACTLOIS",
    Legdelegan = "LEGDELEGAN",
    Minfocomper = "MINFOCOMPER",
    Modregltan = "MODREGLTAN",
    Offparl = "OFFPARL",
    Org = "ORG",
    Propactcom = "PROPACTCOM",
    Prpdit = "PRPDIT",
    Rect = "RECT",
    Refart11 = "REFART11",
    Rgltbudg = "RGLTBUDG",
    Supp = "SUPP",
    Suspours = "SUSPOURS",
    Tvxinstiteurop = "TVXINSTITEUROP",
}

export enum StatutAdoption {
    Adoptcom = "ADOPTCOM",
}

export interface CoSignataires {
    coSignataire: CoSignataireElement[] | CoSignataireElement;
}

export interface CoSignataireElement {
    acteur?:                InitiateursClass;
    dateCosignature:        string;
    dateRetraitCosignature: null | string;
    edite:                  string;
    organe?:                CoSignataireOrgane;
}

export interface InitiateursClass {
    acteurRef: string;
}

export interface CoSignataireOrgane {
    organeRef:    string;
    etApparentes: string;
}

export interface Correction {
    typeCorrection:    TypeCorrection;
    niveauCorrection?: string;
}

export enum TypeCorrection {
    Rectifié = "Rectifié",
}

export interface CycleDeVie {
    chrono: Chrono;
}

export interface Chrono {
    dateCreation:       Date;
    dateDepot:          Date;
    datePublication:    Date | null;
    datePublicationWeb: Date | null;
}

export enum DocumentDenominationStructurelle {
    Avis = "Avis",
    Déclaration = "Déclaration",
    Lettre = "Lettre",
    ProjetDeLoi = "Projet de loi",
    PropositionDeLoi = "Proposition de loi",
    PropositionDeRésolution = "Proposition de résolution",
    Rapport = "Rapport",
    RapportDInformation = "Rapport d'information",
}

export interface DepotAmendements {
    amendementsSeance:      AmendementsSeance;
    amendementsCommission?: AmendementsCommission;
}

export interface AmendementsCommission {
    commission: CommissionElement[] | CommissionElement;
}

export interface CommissionElement {
    organeRef:       string;
    amendable:       string;
    dateLimiteDepot: null;
}

export interface AmendementsSeance {
    amendable:       string;
    dateLimiteDepot: null;
}

export interface PurpleDivisions {
    division: PurpleDivision[] | TentacledDivision;
}

export interface PurpleDivision {
    "@xsi:type":              DocumentXsiType;
    uid:                      string;
    legislature:              null | string;
    cycleDeVie:               CycleDeVie;
    denominationStructurelle: string;
    titres:                   Titres;
    divisions:                FluffyDivisions | null;
    dossierRef:               string;
    redacteur:                null;
    classification:           Classification;
    auteurs:                  PurpleAuteurs;
    correction:               Correction | null;
    notice:                   Notice;
    indexation:               Indexation | null;
    imprimerie:               Imprimerie | null;
    coSignataires?:           null;
    depotAmendements?:        null;
}

export interface PurpleAuteurs {
    auteur: AuteurElement[] | PurpleAuteur;
}

export interface PurpleAuteur {
    organe: AuteurOrgane;
}

export interface FluffyDivisions {
    division: FluffyDivision[];
}

export interface FluffyDivision {
    "@xsi:type":              DocumentXsiType;
    uid:                      string;
    legislature:              string;
    cycleDeVie:               CycleDeVie;
    denominationStructurelle: string;
    titres:                   Titres;
    divisions:                null;
    dossierRef:               string;
    redacteur:                null;
    classification:           Classification;
    auteurs:                  FluffyAuteurs;
    correction:               null;
    notice:                   Notice;
    indexation:               null;
    imprimerie:               Imprimerie | null;
    coSignataires?:           null;
    depotAmendements?:        null;
}

export interface FluffyAuteurs {
    auteur: AuteurElement[];
}

export interface Imprimerie {
    prix: string;
}

export interface Notice {
    numNotice?:       string;
    formule?:         string;
    adoptionConforme: string;
}

export interface Titres {
    titrePrincipal:      string;
    titrePrincipalCourt: string;
}

export interface TentacledDivision {
    "@xsi:type":              DocumentXsiType;
    uid:                      string;
    legislature:              null | string;
    cycleDeVie:               CycleDeVie;
    denominationStructurelle: string;
    titres:                   Titres;
    divisions:                null;
    dossierRef:               string;
    redacteur:                null;
    classification:           Classification;
    auteurs:                  FluffyAuteurs;
    correction:               Correction | null;
    notice:                   Notice;
    indexation:               Indexation | null;
    imprimerie:               Imprimerie | null;
    coSignataires?:           null;
    depotAmendements?:        null;
}

export interface Indexation {
    themes: Themes;
}

export interface Themes {
    "@niveau": string;
    theme:     Theme;
}

export interface Theme {
    libelleTheme: string;
}

export enum Provenance {
    Commission = "Commission",
    TexteDéposé = "Texte Déposé",
}

export interface DossierParlementaire {
    dossierParlementaire: DossierParlementaireDossierParlementaire;
}

export interface DossierParlementaireDossierParlementaire {
    "@xmlns":               string;
    "@xmlns:xsi":           string;
    "@xsi:type":            DossierParlementaireXsiType;
    uid:                    string;
    legislature:            string;
    titreDossier:           TitreDossier;
    procedureParlementaire: ProcedureParlementaire;
    initiateur:             DossierParlementaireInitiateur | null;
    actesLegislatifs:       PurpleActesLegislatifs;
    fusionDossier:          FusionDossier | null;
    PLF?:                   Plf;
}

export enum DossierParlementaireXsiType {
    DossierCommissionEnqueteType = "DossierCommissionEnquete_Type",
    DossierIniativeExecutifType = "DossierIniativeExecutif_Type",
    DossierLegislatifType = "DossierLegislatif_Type",
    DossierMissionControleType = "DossierMissionControle_Type",
    DossierMissionInformationType = "DossierMissionInformation_Type",
    DossierResolutionAn = "DossierResolutionAN",
}

export interface Plf {
    EtudePLF: EtudePlf[];
}

export interface EtudePlf {
    uid:             string;
    organeRef:       string;
    texteAssocie?:   string;
    rapporteur?:     RapporteursRapporteurClass[] | RapporteursRapporteurClass;
    missionMinefi?:  MissionMinefiElement;
    ordreDIQS:       string;
    ordreCommission: string;
}

export interface Missions {
    mission: MissionMinefiElement[] | MissionMinefiElement;
}

export interface MissionMinefiElement {
    typeMission:  TypeMission;
    libelleLong:  string;
    libelleCourt: string;
    typeBudget:   TypeBudget;
    missions?:    Missions;
}

export enum TypeBudget {
    BudgetAnnexe = "Budget annexe",
    BudgetGénéral = "Budget général",
    CompteDeConcoursFinancier = "Compte de concours financier",
    CompteSpécial = "Compte spécial",
    PremièrePartie = "Première partie",
}

export enum TypeMission {
    MissionPrincipale = "mission principale",
    MissionSecondaire = "mission secondaire",
    PartieDeMission = "partie de mission",
}

export interface RapporteursRapporteurClass {
    acteurRef:      string;
    typeRapporteur: Qualite;
}

export interface PurpleActesLegislatifs {
    acteLegislatif: PurpleActeLegislatif[] | AmbitiousActeLegislatif;
}

export interface PurpleActeLegislatif {
    "@xsi:type":      PurpleXsiType;
    uid:              string;
    codeActe:         HilariousCodeActe;
    libelleActe:      LibelleActe;
    organeRef:        string;
    dateActe:         null;
    actesLegislatifs: FluffyActesLegislatifs;
}

export enum PurpleXsiType {
    AdoptionEuropeType = "Adoption_Europe_Type",
    ConclusionEtapeCcType = "ConclusionEtapeCC_Type",
    DecisionRecevabiliteBureauType = "DecisionRecevabiliteBureau_Type",
    DecisionType = "Decision_Type",
    DeclarationGouvernementType = "DeclarationGouvernement_Type",
    DepotAccordInternationalType = "DepotAccordInternational_Type",
    DepotAvisConseilEtatType = "DepotAvisConseilEtat_Type",
    DepotInitiativeNavetteType = "DepotInitiativeNavette_Type",
    DepotInitiativeType = "DepotInitiative_Type",
    DepotLettreRectificativeType = "DepotLettreRectificative_Type",
    DepotMotionCensureType = "DepotMotionCensure_Type",
    EtapeType = "Etape_Type",
    EtudeImpactType = "EtudeImpact_Type",
    ProcedureAccelereType = "ProcedureAccelere_Type",
    RenvoiCmpType = "RenvoiCMP_Type",
    RetraitInitiativeType = "RetraitInitiative_Type",
    SaisineConseilConstitType = "SaisineConseilConstit_Type",
}

export interface FluffyActesLegislatifs {
    acteLegislatif: FluffyActeLegislatif[] | HilariousActeLegislatif;
}

export interface FluffyActeLegislatif {
    "@xsi:type":             PurpleXsiType;
    uid:                     string;
    codeActe:                IndigoCodeActe;
    libelleActe:             LibelleActe;
    organeRef:               string;
    dateActe:                Date | null;
    actesLegislatifs:        TentacledActesLegislatifs | null;
    texteAssocie?:           string;
    contributionInternaute?: ContributionInternaute;
    provenance?:             string;
    initiateur?:             ActeLegislatifInitiateur;
    statutConclusion?:       CasSaisine;
    reunionRef?:             null;
    voteRefs?:               null;
    casSaisine?:             CasSaisine;
    initiateurs?:            InitiateursClass | null;
    motif?:                  Motif;
    urlConclusion?:          string;
    numDecision?:            string;
    anneeDecision?:          string;
}

export interface TentacledActesLegislatifs {
    acteLegislatif: TentacledActeLegislatif[] | IndecentActeLegislatif;
}

export interface TentacledActeLegislatif {
    "@xsi:type":       FluffyXsiType;
    uid:               string;
    codeActe:          TentacledCodeActe;
    libelleActe:       LibelleActe;
    organeRef:         string;
    dateActe:          Date | null;
    actesLegislatifs:  StickyActesLegislatifs | null;
    reunionRef?:       string;
    odjRef?:           string;
    statutConclusion?: CasSaisine;
    voteRefs?:         PurpleVoteRefs | null;
    textesAssocies?:   PurpleTextesAssocies;
    rapporteurs?:      StickyRapporteurs;
    reunion?:          null;
    texteAssocie?:     string;
    texteAdopte?:      null | string;
}

export enum FluffyXsiType {
    CreationOrganeTemporaireType = "CreationOrganeTemporaire_Type",
    DecisionMotionCensureType = "DecisionMotionCensure_Type",
    DecisionType = "Decision_Type",
    DeclarationGouvernementType = "DeclarationGouvernement_Type",
    DepotInitiativeType = "DepotInitiative_Type",
    DepotMotionReferendaireType = "DepotMotionReferendaire_Type",
    DepotRapportType = "DepotRapport_Type",
    DiscussionCommissionType = "DiscussionCommission_Type",
    DiscussionSeancePubliqueType = "DiscussionSeancePublique_Type",
    EtapeType = "Etape_Type",
    MotionProcedureType = "MotionProcedure_Type",
    NominRapporteursType = "NominRapporteurs_Type",
    RenvoiPrealableType = "RenvoiPrealable_Type",
    SaisieComAvisType = "SaisieComAvis_Type",
    SaisieComFondType = "SaisieComFond_Type",
}

export interface StickyActesLegislatifs {
    acteLegislatif: ActesLegislatifsActeLegislatifClass[] | StickyActeLegislatif;
}

export interface ActesLegislatifsActeLegislatifClass {
    "@xsi:type":      FluffyXsiType;
    uid:              string;
    codeActe:         PurpleCodeActe;
    libelleActe:      LibelleActe;
    organeRef:        string;
    dateActe:         Date;
    actesLegislatifs: null;
    rapporteurs?:     PurpleRapporteurs;
    reunion?:         null;
    texteAssocie?:    string;
    texteAdopte?:     null | string;
    reunionRef?:      null | string;
    odjRef?:          null | string;
}

export enum PurpleCodeActe {
    An1ComAvisNomin = "AN1-COM-AVIS-NOMIN",
    An1ComAvisRapport = "AN1-COM-AVIS-RAPPORT",
    An1ComAvisReunion = "AN1-COM-AVIS-REUNION",
    An1ComAvisSaisie = "AN1-COM-AVIS-SAISIE",
    An1ComFondNomin = "AN1-COM-FOND-NOMIN",
    An1ComFondRapport = "AN1-COM-FOND-RAPPORT",
    An1ComFondReunion = "AN1-COM-FOND-REUNION",
    An1ComFondSaisie = "AN1-COM-FOND-SAISIE",
    An2ComAvisRapport = "AN2-COM-AVIS-RAPPORT",
    An2ComAvisReunion = "AN2-COM-AVIS-REUNION",
    An2ComAvisSaisie = "AN2-COM-AVIS-SAISIE",
    An2ComFondRapport = "AN2-COM-FOND-RAPPORT",
    An2ComFondReunion = "AN2-COM-FOND-REUNION",
    An2ComFondSaisie = "AN2-COM-FOND-SAISIE",
    AnluniComCaeNomin = "ANLUNI-COM-CAE-NOMIN",
    AnluniComCaeRapport = "ANLUNI-COM-CAE-RAPPORT",
    AnluniComCaeSaisie = "ANLUNI-COM-CAE-SAISIE",
    AnluniComFondNomin = "ANLUNI-COM-FOND-NOMIN",
    AnluniComFondRapport = "ANLUNI-COM-FOND-RAPPORT",
    AnluniComFondReunion = "ANLUNI-COM-FOND-REUNION",
    AnluniComFondSaisie = "ANLUNI-COM-FOND-SAISIE",
    AnnlecComAvisNomin = "ANNLEC-COM-AVIS-NOMIN",
    AnnlecComAvisRapport = "ANNLEC-COM-AVIS-RAPPORT",
    AnnlecComAvisReunion = "ANNLEC-COM-AVIS-REUNION",
    AnnlecComAvisSaisie = "ANNLEC-COM-AVIS-SAISIE",
    AnnlecComFondRapport = "ANNLEC-COM-FOND-RAPPORT",
    AnnlecComFondReunion = "ANNLEC-COM-FOND-REUNION",
    AnnlecComFondSaisie = "ANNLEC-COM-FOND-SAISIE",
    Sn1ComAvisNomin = "SN1-COM-AVIS-NOMIN",
    Sn1ComAvisRapport = "SN1-COM-AVIS-RAPPORT",
    Sn1ComAvisSaisie = "SN1-COM-AVIS-SAISIE",
    Sn1ComFondNomin = "SN1-COM-FOND-NOMIN",
    Sn1ComFondRapport = "SN1-COM-FOND-RAPPORT",
    Sn1ComFondSaisie = "SN1-COM-FOND-SAISIE",
    Sn2ComAvisNomin = "SN2-COM-AVIS-NOMIN",
    Sn2ComAvisRapport = "SN2-COM-AVIS-RAPPORT",
    Sn2ComAvisSaisie = "SN2-COM-AVIS-SAISIE",
    Sn2ComFondRapport = "SN2-COM-FOND-RAPPORT",
    Sn2ComFondSaisie = "SN2-COM-FOND-SAISIE",
}

export interface LibelleActe {
    nomCanonique:  string;
    libelleCourt?: string;
}

export interface PurpleRapporteurs {
    rapporteur: PurpleRapporteur[] | PurpleRapporteur;
}

export interface PurpleRapporteur {
    acteurRef:      string;
    typeRapporteur: Qualite;
    etudePLFRef?:   string;
}

export interface IndecentActesLegislatifs {
    acteLegislatif: StickyActeLegislatif[] | StickyActeLegislatif;
}

export interface IndigoActeLegislatif {
    "@xsi:type":       FluffyXsiType;
    uid:               string;
    codeActe:          FluffyCodeActe;
    libelleActe:       LibelleActe;
    organeRef:         string;
    dateActe:          Date | null;
    actesLegislatifs:  IndecentActesLegislatifs | null;
    texteAssocie?:     string;
    texteAdopte?:      null | string;
    rapporteurs?:      FluffyRapporteurs;
    reunion?:          null;
    reunionRef?:       null | string;
    odjRef?:           null | string;
    initiateurs?:      AuteurOrgane;
    statutConclusion?: CasSaisine;
    voteRefs?:         null;
    typeDeclaration?:  CasSaisine;
}

export interface IndigoActesLegislatifs {
    acteLegislatif: IndigoActeLegislatif[] | StickyActeLegislatif;
}

export interface StickyActeLegislatif {
    "@xsi:type":      FluffyXsiType;
    uid:              string;
    codeActe:         FluffyCodeActe;
    libelleActe:      LibelleActe;
    organeRef:        string;
    dateActe:         Date | null;
    actesLegislatifs: IndigoActesLegislatifs | null;
    texteAssocie?:    string;
    texteAdopte?:     null;
    initiateurs?:     AuteurOrgane;
    rapporteurs?:     TentacledRapporteurs;
    reunion?:         null;
}

export enum FluffyCodeActe {
    An1ComAvisNomin = "AN1-COM-AVIS-NOMIN",
    An1ComAvisRapport = "AN1-COM-AVIS-RAPPORT",
    An1ComAvisReunion = "AN1-COM-AVIS-REUNION",
    An1ComAvisSaisie = "AN1-COM-AVIS-SAISIE",
    An1ComFondNomin = "AN1-COM-FOND-NOMIN",
    An1ComFondRapport = "AN1-COM-FOND-RAPPORT",
    An1ComFondReunion = "AN1-COM-FOND-REUNION",
    An1ComFondSaisie = "AN1-COM-FOND-SAISIE",
    An1Depot = "AN1-DEPOT",
    An20Comenq = "AN20-COMENQ",
    An20ComenqCrea = "AN20-COMENQ-CREA",
    An20ComenqNomin = "AN20-COMENQ-NOMIN",
    An20ComenqRapport = "AN20-COMENQ-RAPPORT",
    An20Misinf = "AN20-MISINF",
    An20MisinfCrea = "AN20-MISINF-CREA",
    An20MisinfNomin = "AN20-MISINF-NOMIN",
    An20MisinfRapport = "AN20-MISINF-RAPPORT",
    An20Rapport = "AN20-RAPPORT",
    An21Apan = "AN21-APAN",
    An21Dgvt = "AN21-DGVT",
    An2ComFondNomin = "AN2-COM-FOND-NOMIN",
    An2ComFondRapport = "AN2-COM-FOND-RAPPORT",
    An2ComFondReunion = "AN2-COM-FOND-REUNION",
    An2ComFondSaisie = "AN2-COM-FOND-SAISIE",
    AnldefComFondRapport = "ANLDEF-COM-FOND-RAPPORT",
    AnldefComFondReunion = "ANLDEF-COM-FOND-REUNION",
    AnldefComFondSaisie = "ANLDEF-COM-FOND-SAISIE",
    AnluniComCaeDec = "ANLUNI-COM-CAE-DEC",
    AnluniComCaeNomin = "ANLUNI-COM-CAE-NOMIN",
    AnluniComCaeRapport = "ANLUNI-COM-CAE-RAPPORT",
    AnluniComCaeReunion = "ANLUNI-COM-CAE-REUNION",
    AnluniComCaeSaisie = "ANLUNI-COM-CAE-SAISIE",
    AnluniComFondNomin = "ANLUNI-COM-FOND-NOMIN",
    AnluniComFondRapport = "ANLUNI-COM-FOND-RAPPORT",
    AnluniComFondReunion = "ANLUNI-COM-FOND-REUNION",
    AnluniComFondSaisie = "ANLUNI-COM-FOND-SAISIE",
    AnluniDepot = "ANLUNI-DEPOT",
    AnnlecComFondNomin = "ANNLEC-COM-FOND-NOMIN",
    AnnlecComFondRapport = "ANNLEC-COM-FOND-RAPPORT",
    AnnlecComFondReunion = "ANNLEC-COM-FOND-REUNION",
    AnnlecComFondSaisie = "ANNLEC-COM-FOND-SAISIE",
    Sn1ComAvisNomin = "SN1-COM-AVIS-NOMIN",
    Sn1ComAvisRapport = "SN1-COM-AVIS-RAPPORT",
    Sn1ComAvisSaisie = "SN1-COM-AVIS-SAISIE",
    Sn1ComFondNomin = "SN1-COM-FOND-NOMIN",
    Sn1ComFondRapport = "SN1-COM-FOND-RAPPORT",
    Sn1ComFondSaisie = "SN1-COM-FOND-SAISIE",
    Sn2ComFondNomin = "SN2-COM-FOND-NOMIN",
    Sn2ComFondRapport = "SN2-COM-FOND-RAPPORT",
    Sn2ComFondSaisie = "SN2-COM-FOND-SAISIE",
    Sn3ComFondRapport = "SN3-COM-FOND-RAPPORT",
    Sn3ComFondSaisie = "SN3-COM-FOND-SAISIE",
    SnnlecComFondNomin = "SNNLEC-COM-FOND-NOMIN",
    SnnlecComFondRapport = "SNNLEC-COM-FOND-RAPPORT",
    SnnlecComFondSaisie = "SNNLEC-COM-FOND-SAISIE",
}

export interface FluffyRapporteurs {
    rapporteur: RapporteursRapporteurClass[] | RapporteursRapporteurClass;
}

export interface CasSaisine {
    fam_code: FamCode;
    libelle:  string;
}

export enum FamCode {
    Art493 = "Art.49.3",
    Ettd01 = "ETTD01",
    Tccmp01 = "TCCMP01",
    Tccmp02 = "TCCMP02",
    Tcd01 = "TCD01",
    Tcd02 = "TCD02",
    Tcd03 = "TCD03",
    Tcd04 = "TCD04",
    The02 = "02",
    Tmp02 = "TMP02",
    Tmp03 = "TMP03",
    Tmp05 = "TMP05",
    Tmp06 = "TMP06",
    Tmrc01 = "TMRC01",
    Tsccont01 = "TSCCONT01",
    Tsccont02 = "TSCCONT02",
    Tsccont03 = "TSCCONT03",
    Tsccont04 = "TSCCONT04",
    Tsccont05 = "TSCCONT05",
    Tsccont06 = "TSCCONT06",
    Tsccont07 = "TSCCONT07",
    Tsortf01 = "TSORTF01",
    Tsortf02 = "TSORTF02",
    Tsortf03 = "TSORTF03",
    Tsortf04 = "TSORTF04",
    Tsortf05 = "TSORTF05",
    Tsortf06 = "TSORTF06",
    Tsortf07 = "TSORTF07",
    Tsortf14 = "TSORTF14",
    Tsortf18 = "TSORTF18",
    Tsortf19 = "TSORTF19",
    Tsortf20 = "TSORTF20",
    Tsortf21 = "TSORTF21",
    Tsortf23 = "TSORTF23",
    Tsortmot01 = "TSORTMOT01",
    Tsortmot02 = "TSORTMOT02",
}

export interface TentacledRapporteurs {
    rapporteur: RapporteursRapporteurClass;
}

export enum TentacledCodeActe {
    An1ComAvis = "AN1-COM-AVIS",
    An1ComFond = "AN1-COM-FOND",
    An1DebatsDec = "AN1-DEBATS-DEC",
    An1DebatsMotion = "AN1-DEBATS-MOTION",
    An1DebatsMotionVote = "AN1-DEBATS-MOTION-VOTE",
    An1DebatsSeance = "AN1-DEBATS-SEANCE",
    An2ComAvis = "AN2-COM-AVIS",
    An2ComFond = "AN2-COM-FOND",
    An2DebatsDec = "AN2-DEBATS-DEC",
    An2DebatsSeance = "AN2-DEBATS-SEANCE",
    AnldefDebatsDec = "ANLDEF-DEBATS-DEC",
    AnldefDebatsSeance = "ANLDEF-DEBATS-SEANCE",
    AnluniDebatsDec = "ANLUNI-DEBATS-DEC",
    AnluniDebatsSeance = "ANLUNI-DEBATS-SEANCE",
    AnnlecComAvis = "ANNLEC-COM-AVIS",
    AnnlecComFond = "ANNLEC-COM-FOND",
    AnnlecDebatsDec = "ANNLEC-DEBATS-DEC",
    AnnlecDebatsMotionVote = "ANNLEC-DEBATS-MOTION-VOTE",
    AnnlecDebatsSeance = "ANNLEC-DEBATS-SEANCE",
    CmpComNomin = "CMP-COM-NOMIN",
    CmpComRapportAn = "CMP-COM-RAPPORT-AN",
    CmpComRapportSn = "CMP-COM-RAPPORT-SN",
    CmpDebatsAnDec = "CMP-DEBATS-AN-DEC",
    CmpDebatsAnSeance = "CMP-DEBATS-AN-SEANCE",
    CmpDebatsSnDec = "CMP-DEBATS-SN-DEC",
    CmpDebatsSnSeance = "CMP-DEBATS-SN-SEANCE",
    Sn1ComAvis = "SN1-COM-AVIS",
    Sn1ComFond = "SN1-COM-FOND",
    Sn1DebatsDec = "SN1-DEBATS-DEC",
    Sn1DebatsMotion = "SN1-DEBATS-MOTION",
    Sn1DebatsMotionVote = "SN1-DEBATS-MOTION-VOTE",
    Sn1DebatsSeance = "SN1-DEBATS-SEANCE",
    Sn2ComAvis = "SN2-COM-AVIS",
    Sn2ComFond = "SN2-COM-FOND",
    Sn2DebatsDec = "SN2-DEBATS-DEC",
    Sn2DebatsSeance = "SN2-DEBATS-SEANCE",
    Sn3DebatsDec = "SN3-DEBATS-DEC",
    Sn3DebatsSeance = "SN3-DEBATS-SEANCE",
    SnnlecDebatsDec = "SNNLEC-DEBATS-DEC",
    SnnlecDebatsSeance = "SNNLEC-DEBATS-SEANCE",
}

export interface StickyRapporteurs {
    rapporteur: RapporteursRapporteurClass[];
}

export interface PurpleTextesAssocies {
    texteAssocie: TexteAssocieElement[] | TexteAssocieElement;
}

export interface TexteAssocieElement {
    typeTexte:       TypeTexte;
    refTexteAssocie: string;
}

export enum TypeTexte {
    Bta = "BTA",
    Tap = "TAP",
}

export interface PurpleVoteRefs {
    voteRef: string[] | string;
}

export interface IndecentActeLegislatif {
    "@xsi:type":       FluffyXsiType;
    uid:               string;
    codeActe:          StickyCodeActe;
    libelleActe:       LibelleActe;
    organeRef:         string;
    dateActe:          Date | null;
    actesLegislatifs:  IndigoActesLegislatifs | null;
    statutConclusion?: CasSaisine;
    reunionRef?:       null | string;
    voteRefs?:         null;
    textesAssocies?:   FluffyTextesAssocies;
    odjRef?:           string;
}

export enum StickyCodeActe {
    An1ComFond = "AN1-COM-FOND",
    An1DebatsDec = "AN1-DEBATS-DEC",
    An1DebatsSeance = "AN1-DEBATS-SEANCE",
    An21DebatsMotionVote = "AN21-DEBATS-MOTION-VOTE",
    An21DebatsSeance = "AN21-DEBATS-SEANCE",
    An2ComFond = "AN2-COM-FOND",
    AnldefComFond = "ANLDEF-COM-FOND",
    AnldefDebatsDec = "ANLDEF-DEBATS-DEC",
    AnluniComCae = "ANLUNI-COM-CAE",
    AnluniComFond = "ANLUNI-COM-FOND",
    AnluniDebatsDec = "ANLUNI-DEBATS-DEC",
    AnluniDebatsSeance = "ANLUNI-DEBATS-SEANCE",
    AnnlecComFond = "ANNLEC-COM-FOND",
    CmpDebatsAnDec = "CMP-DEBATS-AN-DEC",
    CmpDebatsAnSeance = "CMP-DEBATS-AN-SEANCE",
    CmpDebatsSnSeance = "CMP-DEBATS-SN-SEANCE",
    Sn1ComFond = "SN1-COM-FOND",
    Sn1DebatsDec = "SN1-DEBATS-DEC",
    Sn1DebatsSeance = "SN1-DEBATS-SEANCE",
    Sn2ComFond = "SN2-COM-FOND",
    Sn2DebatsSeance = "SN2-DEBATS-SEANCE",
    Sn3ComFond = "SN3-COM-FOND",
    SnnlecComFond = "SNNLEC-COM-FOND",
}

export interface FluffyTextesAssocies {
    texteAssocie: TexteAssocieElement[];
}

export enum IndigoCodeActe {
    An1Acin = "AN1-ACIN",
    An1Avce = "AN1-AVCE",
    An1Com = "AN1-COM",
    An1Debats = "AN1-DEBATS",
    An1Depot = "AN1-DEPOT",
    An1Dgvt = "AN1-DGVT",
    An1Dptlettrect = "AN1-DPTLETTRECT",
    An1Eti = "AN1-ETI",
    An1Motion = "AN1-MOTION",
    An1Procacc = "AN1-PROCACC",
    An1Recbureau = "AN1-RECBUREAU",
    An1Rtrini = "AN1-RTRINI",
    An21Debats = "AN21-DEBATS",
    An21Dgvt = "AN21-DGVT",
    An21Motion = "AN21-MOTION",
    An2Com = "AN2-COM",
    An2Debats = "AN2-DEBATS",
    An2Depot = "AN2-DEPOT",
    AnldefCom = "ANLDEF-COM",
    AnldefDebats = "ANLDEF-DEBATS",
    AnldefDepot = "ANLDEF-DEPOT",
    AnldefDgvt = "ANLDEF-DGVT",
    AnluniCom = "ANLUNI-COM",
    AnluniDebats = "ANLUNI-DEBATS",
    AnluniDepot = "ANLUNI-DEPOT",
    AnluniRtrini = "ANLUNI-RTRINI",
    AnnlecCom = "ANNLEC-COM",
    AnnlecDebats = "ANNLEC-DEBATS",
    AnnlecDepot = "ANNLEC-DEPOT",
    AnnlecDgvt = "ANNLEC-DGVT",
    AnnlecMotion = "ANNLEC-MOTION",
    CcConclusion = "CC-CONCLUSION",
    CcSaisieAn = "CC-SAISIE-AN",
    CcSaisieDroit = "CC-SAISIE-DROIT",
    CcSaisiePan = "CC-SAISIE-PAN",
    CcSaisiePm = "CC-SAISIE-PM",
    CcSaisiePr = "CC-SAISIE-PR",
    CcSaisiePsn = "CC-SAISIE-PSN",
    CcSaisieSn = "CC-SAISIE-SN",
    CmpCom = "CMP-COM",
    CmpDebatsAn = "CMP-DEBATS-AN",
    CmpDebatsSn = "CMP-DEBATS-SN",
    CmpDec = "CMP-DEC",
    CmpDepot = "CMP-DEPOT",
    CmpSaisie = "CMP-SAISIE",
    EuDec = "EU-DEC",
    Sn1Avce = "SN1-AVCE",
    Sn1Com = "SN1-COM",
    Sn1Debats = "SN1-DEBATS",
    Sn1Depot = "SN1-DEPOT",
    Sn1Dptlettrect = "SN1-DPTLETTRECT",
    Sn1Eti = "SN1-ETI",
    Sn1Procacc = "SN1-PROCACC",
    Sn1Rtrini = "SN1-RTRINI",
    Sn2Com = "SN2-COM",
    Sn2Debats = "SN2-DEBATS",
    Sn2Depot = "SN2-DEPOT",
    Sn3Com = "SN3-COM",
    Sn3Debats = "SN3-DEBATS",
    Sn3Depot = "SN3-DEPOT",
    SnnlecCom = "SNNLEC-COM",
    SnnlecDebats = "SNNLEC-DEBATS",
    SnnlecDepot = "SNNLEC-DEPOT",
}

export interface ContributionInternaute {
    dateFermeture:  null | string;
    dateOuverture?: string;
}

export interface ActeLegislatifInitiateur {
    acteurs: PurpleActeurs;
}

export interface PurpleActeurs {
    acteur: ActeurElement;
}

export interface ActeurElement {
    acteurRef: string;
    mandatRef: string;
}

export enum Motif {
    EnApplicationDeLArticle612DeLaConstitution = "En application de l'article 61§2 de la Constitution",
}

export interface HilariousActeLegislatif {
    "@xsi:type":       TentacledXsiType;
    uid:               string;
    codeActe:          IndecentCodeActe;
    libelleActe:       LibelleActe;
    organeRef:         string;
    dateActe:          Date;
    actesLegislatifs:  null;
    texteLoiRef?:      string;
    infoJO?:           PurpleInfoJo;
    urlEcheancierLoi?: null | string;
    codeLoi?:          string;
    titreLoi?:         string;
    infoJORect?:       PurpleInfoJo;
    urlLegifrance?:    null;
    texteAssocie?:     string;
    texteAdopte?:      null;
    casSaisine?:       CasSaisine;
    initiateurs?:      null;
    motif?:            Motif;
}

export enum TentacledXsiType {
    DepotRapportType = "DepotRapport_Type",
    PromulgationType = "Promulgation_Type",
    SaisineConseilConstitType = "SaisineConseilConstit_Type",
}

export enum IndecentCodeActe {
    An20Rapport = "AN20-RAPPORT",
    AnAppliRapport = "AN-APPLI-RAPPORT",
    CcSaisieSn = "CC-SAISIE-SN",
    PromPub = "PROM-PUB",
}

export interface PurpleInfoJo {
    typeJO:        TypeJo;
    dateJO:        string;
    pageJO:        null;
    numJO:         string;
    urlLegifrance: string;
    referenceNOR:  string;
}

export enum TypeJo {
    JoLoiDecret = "JO_LOI_DECRET",
}

export enum HilariousCodeActe {
    An1 = "AN1",
    An2 = "AN2",
    An20 = "AN20",
    An21 = "AN21",
    AnAppli = "AN-APPLI",
    Anldef = "ANLDEF",
    Anluni = "ANLUNI",
    Annlec = "ANNLEC",
    Cc = "CC",
    Cmp = "CMP",
    Eu = "EU",
    Prom = "PROM",
    Sn1 = "SN1",
    Sn2 = "SN2",
    Sn3 = "SN3",
    Snnlec = "SNNLEC",
}

export interface AmbitiousActeLegislatif {
    "@xsi:type":      PurpleXsiType;
    uid:              string;
    codeActe:         HilariousCodeActe;
    libelleActe:      LibelleActe;
    organeRef:        string;
    dateActe:         null;
    actesLegislatifs: HilariousActesLegislatifs;
}

export interface HilariousActesLegislatifs {
    acteLegislatif: CunningActeLegislatif[] | StickyActeLegislatif;
}

export interface CunningActeLegislatif {
    "@xsi:type":             PurpleXsiType;
    uid:                     string;
    codeActe:                IndigoCodeActe;
    libelleActe:             LibelleActe;
    organeRef:               string;
    dateActe:                Date | null;
    actesLegislatifs:        AmbitiousActesLegislatifs | null;
    texteAssocie?:           string;
    contributionInternaute?: ContributionInternaute;
    typeMotionCensure?:      CasSaisine;
    auteurs?:                ActeLegislatifAuteurs;
    dateRetrait?:            null;
}

export interface AmbitiousActesLegislatifs {
    acteLegislatif: MagentaActeLegislatif[] | FriskyActeLegislatif;
}

export interface MagentaActeLegislatif {
    "@xsi:type":       FluffyXsiType;
    uid:               string;
    codeActe:          AmbitiousCodeActe;
    libelleActe:       LibelleActe;
    organeRef:         string;
    dateActe:          Date | null;
    actesLegislatifs:  StickyActesLegislatifs | null;
    reunionRef?:       null | string;
    odjRef?:           string;
    typeMotion?:       CasSaisine;
    auteurMotion?:     string;
    dateRetrait?:      null;
    voteRefs?:         FluffyVoteRefs | null;
    statutConclusion?: CasSaisine;
    textesAssocies?:   PurpleTextesAssocies;
    decision?:         CasSaisine;
    odSeancejRef?:     null;
}

export enum AmbitiousCodeActe {
    An1ComAvis = "AN1-COM-AVIS",
    An1ComFond = "AN1-COM-FOND",
    An1DebatsDec = "AN1-DEBATS-DEC",
    An1DebatsMotion = "AN1-DEBATS-MOTION",
    An1DebatsMotionVote = "AN1-DEBATS-MOTION-VOTE",
    An1DebatsSeance = "AN1-DEBATS-SEANCE",
    An21DebatsMotionVote = "AN21-DEBATS-MOTION-VOTE",
    An21DebatsSeance = "AN21-DEBATS-SEANCE",
    AnluniComCae = "ANLUNI-COM-CAE",
    AnluniComFond = "ANLUNI-COM-FOND",
    AnluniDebatsDec = "ANLUNI-DEBATS-DEC",
    AnluniDebatsSeance = "ANLUNI-DEBATS-SEANCE",
    Sn1ComAvis = "SN1-COM-AVIS",
    Sn1ComFond = "SN1-COM-FOND",
    Sn1DebatsDec = "SN1-DEBATS-DEC",
    Sn1DebatsSeance = "SN1-DEBATS-SEANCE",
}

export interface FluffyVoteRefs {
    voteRef: string;
}

export interface FriskyActeLegislatif {
    "@xsi:type":       FluffyXsiType;
    uid:               string;
    codeActe:          StickyCodeActe;
    libelleActe:       LibelleActe;
    organeRef:         string;
    dateActe:          Date | null;
    actesLegislatifs:  IndigoActesLegislatifs | null;
    statutConclusion?: CasSaisine;
    reunionRef?:       null | string;
    voteRefs?:         FluffyVoteRefs | null;
    textesAssocies?:   TentacledTextesAssocies;
    odjRef?:           string;
    decision?:         CasSaisine;
    odSeancejRef?:     null;
}

export interface TentacledTextesAssocies {
    texteAssocie: TexteAssocieElement;
}

export interface ActeLegislatifAuteurs {
    acteurRef: string[];
}

export interface FusionDossier {
    cause:               Cause;
    dossierAbsorbantRef: string;
}

export enum Cause {
    DossierAbsorbé = "Dossier absorbé",
    ExamenCommun = "Examen commun",
}

export interface DossierParlementaireInitiateur {
    acteurs?: FluffyActeurs;
    organes?: Organes;
}

export interface FluffyActeurs {
    acteur: ActeurElement[] | ActeurElement;
}

export interface Organes {
    organe: OrganesOrgane;
}

export interface OrganesOrgane {
    organeRef: OrganeRefClass;
}

export interface OrganeRefClass {
    uid: string;
}

export interface TitreDossier {
    titre:       string;
    titreChemin: null | string;
    senatChemin: null | string;
}

export interface DossiersLegislatifs {
    export: Export;
}

export interface Export {
    "@xmlns:xsi":        string;
    textesLegislatifs:   TextesLegislatifs;
    dossiersLegislatifs: DossiersLegislatifsClass;
}

export interface DossiersLegislatifsClass {
    dossier: Dossier[];
}

export interface Dossier {
    dossierParlementaire: DossierDossierParlementaire;
}

export interface DossierDossierParlementaire {
    "@xsi:type"?:           DossierParlementaireXsiType;
    uid:                    string;
    legislature:            string;
    titreDossier:           TitreDossier;
    procedureParlementaire: ProcedureParlementaire;
    initiateur:             DossierParlementaireInitiateur | null;
    actesLegislatifs:       CunningActesLegislatifs;
    fusionDossier:          FusionDossier | null;
    indexation?:            Indexation;
    PLF?:                   Plf;
}

export interface CunningActesLegislatifs {
    acteLegislatif: MischievousActeLegislatif[] | ActeLegislatif3;
}

export interface MischievousActeLegislatif {
    "@xsi:type":      PurpleXsiType;
    uid:              string;
    codeActe:         HilariousCodeActe;
    libelleActe:      LibelleActe;
    organeRef?:       string;
    dateActe:         null;
    actesLegislatifs: MagentaActesLegislatifs;
}

export interface MagentaActesLegislatifs {
    acteLegislatif: BraggadociousActeLegislatif[] | ActeLegislatif2;
}

export interface BraggadociousActeLegislatif {
    "@xsi:type":             PurpleXsiType;
    uid:                     string;
    codeActe:                IndigoCodeActe;
    libelleActe:             LibelleActe;
    organeRef?:              string;
    dateActe:                Date | null;
    actesLegislatifs:        FriskyActesLegislatifs | null;
    texteAssocie?:           string;
    provenance?:             string;
    contributionInternaute?: ContributionInternaute;
    initiateur?:             ActeLegislatifInitiateur;
    statutConclusion?:       CasSaisine;
    reunionRef?:             null;
    voteRefs?:               null;
    casSaisine?:             CasSaisine;
    initiateurs?:            InitiateursClass | null;
    motif?:                  Motif;
    urlConclusion?:          string;
    numDecision?:            string;
    anneeDecision?:          string;
    typeDeclaration?:        CasSaisine;
    typeMotionCensure?:      CasSaisine;
    auteurs?:                ActeLegislatifAuteurs;
    dateRetrait?:            null;
    texteEuropeen?:          TexteEuropeen;
    infoJOCE?:               InfoJoce;
    statutAdoption?:         CasSaisine;
}

export interface FriskyActesLegislatifs {
    acteLegislatif: ActeLegislatif1[] | FriskyActeLegislatif;
}

export interface ActeLegislatif1 {
    "@xsi:type":       FluffyXsiType;
    uid:               string;
    codeActe:          TentacledCodeActe;
    libelleActe:       LibelleActe;
    organeRef:         string;
    dateActe:          Date | null;
    actesLegislatifs:  MischievousActesLegislatifs | null;
    reunionRef?:       null | string;
    odjRef?:           string;
    statutConclusion?: CasSaisine;
    voteRefs?:         PurpleVoteRefs | null;
    textesAssocies?:   PurpleTextesAssocies;
    typeMotion?:       CasSaisine;
    auteurMotion?:     string;
    dateRetrait?:      null;
    rapporteurs?:      StickyRapporteurs;
    reunion?:          null;
    texteAssocie?:     string;
    texteAdopte?:      null;
    auteurs?:          InitiateursClass | null;
    decision?:         CasSaisine;
    odSeancejRef?:     null;
}

export interface MischievousActesLegislatifs {
    acteLegislatif: ActesLegislatifsActeLegislatifClass[];
}

export interface InfoJoce {
    refJOCE:  string;
    dateJOCE: string;
}

export interface TexteEuropeen {
    typeTexteEuropeen:  string;
    titreTexteEuropeen: string;
}

export interface ActeLegislatif2 {
    "@xsi:type":      TentacledXsiType;
    uid:              string;
    codeActe:         CunningCodeActe;
    libelleActe:      LibelleActe;
    organeRef:        string;
    dateActe:         Date;
    actesLegislatifs: null;
    texteLoiRef?:     string;
    infoJO?:          InfoJoElement;
    urlLegifrance?:   string;
    referenceNOR?:    string;
    codeLoi?:         string;
    titreLoi?:        string;
    infoJORect?:      InfoJoElement[] | InfoJoElement;
    texteAssocie?:    string;
    texteAdopte?:     null;
}

export enum CunningCodeActe {
    An20Rapport = "AN20-RAPPORT",
    AnAppliRapport = "AN-APPLI-RAPPORT",
    PromPub = "PROM-PUB",
}

export interface InfoJoElement {
    typeJO: TypeJo;
    dateJO: string;
    pageJO: null;
    numJO:  string;
}

export interface ActeLegislatif3 {
    "@xsi:type":      PurpleXsiType;
    uid:              string;
    codeActe:         HilariousCodeActe;
    libelleActe:      LibelleActe;
    organeRef:        string;
    dateActe:         null;
    actesLegislatifs: BraggadociousActesLegislatifs;
}

export interface BraggadociousActesLegislatifs {
    acteLegislatif: ActeLegislatif4[] | IndigoActeLegislatif;
}

export interface ActeLegislatif4 {
    "@xsi:type":             PurpleXsiType;
    uid:                     string;
    codeActe:                IndigoCodeActe;
    libelleActe:             LibelleActe;
    organeRef:               string;
    dateActe:                Date | null;
    actesLegislatifs:        ActesLegislatifs1 | null;
    texteAssocie?:           string;
    contributionInternaute?: ContributionInternaute;
    decision?:               CasSaisine;
    formuleDecision?:        string;
    typeDeclaration?:        CasSaisine;
    typeMotionCensure?:      CasSaisine;
    auteurs?:                ActeLegislatifAuteurs;
    dateRetrait?:            null;
}

export interface ActesLegislatifs1 {
    acteLegislatif: ActeLegislatif5[] | FriskyActeLegislatif;
}

export interface ActeLegislatif5 {
    "@xsi:type":       FluffyXsiType;
    uid:               string;
    codeActe:          AmbitiousCodeActe;
    libelleActe:       LibelleActe;
    organeRef:         string;
    dateActe:          Date | null;
    actesLegislatifs:  IndigoActesLegislatifs | null;
    reunionRef?:       null | string;
    odjRef?:           string;
    statutConclusion?: CasSaisine;
    voteRefs?:         FluffyVoteRefs | null;
    textesAssocies?:   TentacledTextesAssocies;
    typeMotion?:       CasSaisine;
    auteurMotion?:     string;
    dateRetrait?:      null;
    decision?:         CasSaisine;
    odSeancejRef?:     null;
}

export interface TextesLegislatifs {
    document: DocumentElement[];
}

export interface DocumentElement {
    "@xsi:type":              DocumentXsiType;
    uid:                      string;
    legislature:              null | string;
    cycleDeVie:               CycleDeVie;
    denominationStructurelle: DocumentDenominationStructurelle;
    provenance?:              Provenance;
    titres:                   Titres;
    divisions:                TentacledDivisions | null;
    dossierRef:               string;
    redacteur:                null;
    classification:           Classification;
    auteurs:                  DocumentAuteurs;
    correction:               Correction | null;
    notice:                   Notice;
    indexation:               Indexation | null;
    imprimerie:               Imprimerie | null;
    coSignataires?:           CoSignataires | null;
    depotAmendements?:        DepotAmendements | null;
}

export interface TentacledDivisions {
    division: StickyDivision[] | TentacledDivision;
}

export interface StickyDivision {
    "@xsi:type":              DocumentXsiType;
    uid:                      string;
    legislature:              null | string;
    cycleDeVie:               CycleDeVie;
    denominationStructurelle: string;
    titres:                   Titres;
    divisions:                StickyDivisions | null;
    dossierRef:               string;
    redacteur:                null;
    classification:           Classification;
    auteurs:                  PurpleAuteurs;
    correction:               Correction | null;
    notice:                   Notice;
    indexation:               Indexation | null;
    imprimerie:               Imprimerie | null;
    coSignataires?:           null;
    depotAmendements?:        null;
}

export interface StickyDivisions {
    division: IndigoDivision[];
}

export interface IndigoDivision {
    "@xsi:type":              DocumentXsiType;
    uid:                      string;
    legislature:              string;
    cycleDeVie:               CycleDeVie;
    denominationStructurelle: string;
    titres:                   Titres;
    divisions:                null;
    dossierRef:               string;
    redacteur:                null;
    classification:           Classification;
    auteurs:                  FluffyAuteurs;
    correction:               null;
    notice:                   Notice;
    indexation:               Indexation | null;
    imprimerie:               Imprimerie | null;
}

// Converts JSON strings to/from your types
// and asserts the results of JSON.parse at runtime
export class Convert {
    public static toDocument(json: string): Document {
        return cast(JSON.parse(json), r("Document"));
    }

    public static documentToJson(value: Document): string {
        return JSON.stringify(uncast(value, r("Document")), null, 2);
    }

    public static toDossierParlementaire(json: string): DossierParlementaire {
        return cast(JSON.parse(json), r("DossierParlementaire"));
    }

    public static dossierParlementaireToJson(value: DossierParlementaire): string {
        return JSON.stringify(uncast(value, r("DossierParlementaire")), null, 2);
    }

    public static toDossiersLegislatifs(json: string): DossiersLegislatifs {
        return cast(JSON.parse(json), r("DossiersLegislatifs"));
    }

    public static dossiersLegislatifsToJson(value: DossiersLegislatifs): string {
        return JSON.stringify(uncast(value, r("DossiersLegislatifs")), null, 2);
    }
}

function invalidValue(typ: any, val: any): never {
    throw Error(`Invalid value ${JSON.stringify(val)} for type ${JSON.stringify(typ)}`);
}

function jsonToJSProps(typ: any): any {
    if (typ.jsonToJS === undefined) {
        var map: any = {};
        typ.props.forEach((p: any) => map[p.json] = { key: p.js, typ: p.typ });
        typ.jsonToJS = map;
    }
    return typ.jsonToJS;
}

function jsToJSONProps(typ: any): any {
    if (typ.jsToJSON === undefined) {
        var map: any = {};
        typ.props.forEach((p: any) => map[p.js] = { key: p.json, typ: p.typ });
        typ.jsToJSON = map;
    }
    return typ.jsToJSON;
}

function transform(val: any, typ: any, getProps: any): any {
    function transformPrimitive(typ: string, val: any): any {
        if (typeof typ === typeof val) return val;
        return invalidValue(typ, val);
    }

    function transformUnion(typs: any[], val: any): any {
        // val must validate against one typ in typs
        var l = typs.length;
        for (var i = 0; i < l; i++) {
            var typ = typs[i];
            try {
                return transform(val, typ, getProps);
            } catch (_) {}
        }
        return invalidValue(typs, val);
    }

    function transformEnum(cases: string[], val: any): any {
        if (cases.indexOf(val) !== -1) return val;
        return invalidValue(cases, val);
    }

    function transformArray(typ: any, val: any): any {
        // val must be an array with no invalid elements
        if (!Array.isArray(val)) return invalidValue("array", val);
        return val.map(el => transform(el, typ, getProps));
    }

    function transformDate(_typ: any, val: any): any {
        if (val === null) {
            return null;
        }
        const d = new Date(val);
        if (isNaN(d.valueOf())) {
            return invalidValue("Date", val);
        }
        return d;
    }

    function transformObject(props: { [k: string]: any }, additional: any, val: any): any {
        if (val === null || typeof val !== "object" || Array.isArray(val)) {
            return invalidValue("object", val);
        }
        var result: any = {};
        Object.getOwnPropertyNames(props).forEach(key => {
            const prop = props[key];
            const v = Object.prototype.hasOwnProperty.call(val, key) ? val[key] : undefined;
            result[prop.key] = transform(v, prop.typ, getProps);
        });
        Object.getOwnPropertyNames(val).forEach(key => {
            if (!Object.prototype.hasOwnProperty.call(props, key)) {
                result[key] = transform(val[key], additional, getProps);
            }
        });
        return result;
    }

    if (typ === "any") return val;
    if (typ === null) {
        if (val === null) return val;
        return invalidValue(typ, val);
    }
    if (typ === false) return invalidValue(typ, val);
    while (typeof typ === "object" && typ.ref !== undefined) {
        typ = typeMap[typ.ref];
    }
    if (Array.isArray(typ)) return transformEnum(typ, val);
    if (typeof typ === "object") {
        return typ.hasOwnProperty("unionMembers") ? transformUnion(typ.unionMembers, val)
            : typ.hasOwnProperty("arrayItems")    ? transformArray(typ.arrayItems, val)
            : typ.hasOwnProperty("props")         ? transformObject(getProps(typ), typ.additional, val)
            : invalidValue(typ, val);
    }
    // Numbers can be parsed by Date but shouldn't be.
    if (typ === Date && typeof val !== "number") return transformDate(typ, val);
    return transformPrimitive(typ, val);
}

function cast<T>(val: any, typ: any): T {
    return transform(val, typ, jsonToJSProps);
}

function uncast<T>(val: T, typ: any): any {
    return transform(val, typ, jsToJSONProps);
}

function a(typ: any) {
    return { arrayItems: typ };
}

function u(...typs: any[]) {
    return { unionMembers: typs };
}

function o(props: any[], additional: any) {
    return { props, additional };
}

// function m(additional: any) {
//     return { props: [], additional };
// }

function r(name: string) {
    return { ref: name };
}

const typeMap: any = {
    "Document": o([
        { json: "document", js: "document", typ: r("DocumentDocument") },
    ], false),
    "DocumentDocument": o([
        { json: "@xmlns", js: "@xmlns", typ: "" },
        { json: "@xmlns:xsi", js: "@xmlns:xsi", typ: "" },
        { json: "@xsi:type", js: "@xsi:type", typ: r("DocumentXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "legislature", js: "legislature", typ: u(null, "") },
        { json: "cycleDeVie", js: "cycleDeVie", typ: r("CycleDeVie") },
        { json: "denominationStructurelle", js: "denominationStructurelle", typ: r("DocumentDenominationStructurelle") },
        { json: "provenance", js: "provenance", typ: u(undefined, r("Provenance")) },
        { json: "titres", js: "titres", typ: r("Titres") },
        { json: "divisions", js: "divisions", typ: u(r("PurpleDivisions"), null) },
        { json: "dossierRef", js: "dossierRef", typ: "" },
        { json: "redacteur", js: "redacteur", typ: null },
        { json: "classification", js: "classification", typ: r("Classification") },
        { json: "auteurs", js: "auteurs", typ: r("DocumentAuteurs") },
        { json: "correction", js: "correction", typ: u(r("Correction"), null) },
        { json: "notice", js: "notice", typ: r("Notice") },
        { json: "indexation", js: "indexation", typ: null },
        { json: "imprimerie", js: "imprimerie", typ: u(r("Imprimerie"), null) },
        { json: "coSignataires", js: "coSignataires", typ: u(undefined, u(r("CoSignataires"), null)) },
        { json: "depotAmendements", js: "depotAmendements", typ: u(undefined, u(r("DepotAmendements"), null)) },
    ], false),
    "DocumentAuteurs": o([
        { json: "auteur", js: "auteur", typ: u(a(r("AuteurElement")), r("AuteurElement")) },
    ], false),
    "AuteurElement": o([
        { json: "acteur", js: "acteur", typ: u(undefined, r("AuteurActeur")) },
        { json: "organe", js: "organe", typ: u(undefined, r("AuteurOrgane")) },
    ], false),
    "AuteurActeur": o([
        { json: "acteurRef", js: "acteurRef", typ: "" },
        { json: "qualite", js: "qualite", typ: r("Qualite") },
    ], false),
    "AuteurOrgane": o([
        { json: "organeRef", js: "organeRef", typ: "" },
    ], false),
    "Classification": o([
        { json: "famille", js: "famille", typ: u(r("Famille"), null) },
        { json: "type", js: "type", typ: r("ProcedureParlementaire") },
        { json: "sousType", js: "sousType", typ: u(r("SousType"), null) },
        { json: "statutAdoption", js: "statutAdoption", typ: u(r("StatutAdoption"), null) },
    ], false),
    "Famille": o([
        { json: "depot", js: "depot", typ: r("ProcedureParlementaire") },
        { json: "classe", js: "classe", typ: r("ProcedureParlementaire") },
        { json: "espece", js: "espece", typ: u(undefined, r("ProcedureParlementaire")) },
    ], false),
    "ProcedureParlementaire": o([
        { json: "code", js: "code", typ: "" },
        { json: "libelle", js: "libelle", typ: "" },
    ], false),
    "SousType": o([
        { json: "code", js: "code", typ: r("Code") },
        { json: "libelle", js: "libelle", typ: u(undefined, "") },
        { json: "libelleEdition", js: "libelleEdition", typ: u(undefined, "") },
    ], false),
    "CoSignataires": o([
        { json: "coSignataire", js: "coSignataire", typ: u(a(r("CoSignataireElement")), r("CoSignataireElement")) },
    ], false),
    "CoSignataireElement": o([
        { json: "acteur", js: "acteur", typ: u(undefined, r("InitiateursClass")) },
        { json: "dateCosignature", js: "dateCosignature", typ: "" },
        { json: "dateRetraitCosignature", js: "dateRetraitCosignature", typ: u(null, "") },
        { json: "edite", js: "edite", typ: "" },
        { json: "organe", js: "organe", typ: u(undefined, r("CoSignataireOrgane")) },
    ], false),
    "InitiateursClass": o([
        { json: "acteurRef", js: "acteurRef", typ: "" },
    ], false),
    "CoSignataireOrgane": o([
        { json: "organeRef", js: "organeRef", typ: "" },
        { json: "etApparentes", js: "etApparentes", typ: "" },
    ], false),
    "Correction": o([
        { json: "typeCorrection", js: "typeCorrection", typ: r("TypeCorrection") },
        { json: "niveauCorrection", js: "niveauCorrection", typ: u(undefined, "") },
    ], false),
    "CycleDeVie": o([
        { json: "chrono", js: "chrono", typ: r("Chrono") },
    ], false),
    "Chrono": o([
        { json: "dateCreation", js: "dateCreation", typ: Date },
        { json: "dateDepot", js: "dateDepot", typ: Date },
        { json: "datePublication", js: "datePublication", typ: u(Date, null) },
        { json: "datePublicationWeb", js: "datePublicationWeb", typ: u(Date, null) },
    ], false),
    "DepotAmendements": o([
        { json: "amendementsSeance", js: "amendementsSeance", typ: r("AmendementsSeance") },
        { json: "amendementsCommission", js: "amendementsCommission", typ: u(undefined, r("AmendementsCommission")) },
    ], false),
    "AmendementsCommission": o([
        { json: "commission", js: "commission", typ: u(a(r("CommissionElement")), r("CommissionElement")) },
    ], false),
    "CommissionElement": o([
        { json: "organeRef", js: "organeRef", typ: "" },
        { json: "amendable", js: "amendable", typ: "" },
        { json: "dateLimiteDepot", js: "dateLimiteDepot", typ: null },
    ], false),
    "AmendementsSeance": o([
        { json: "amendable", js: "amendable", typ: "" },
        { json: "dateLimiteDepot", js: "dateLimiteDepot", typ: null },
    ], false),
    "PurpleDivisions": o([
        { json: "division", js: "division", typ: u(a(r("PurpleDivision")), r("TentacledDivision")) },
    ], false),
    "PurpleDivision": o([
        { json: "@xsi:type", js: "@xsi:type", typ: r("DocumentXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "legislature", js: "legislature", typ: u(null, "") },
        { json: "cycleDeVie", js: "cycleDeVie", typ: r("CycleDeVie") },
        { json: "denominationStructurelle", js: "denominationStructurelle", typ: "" },
        { json: "titres", js: "titres", typ: r("Titres") },
        { json: "divisions", js: "divisions", typ: u(r("FluffyDivisions"), null) },
        { json: "dossierRef", js: "dossierRef", typ: "" },
        { json: "redacteur", js: "redacteur", typ: null },
        { json: "classification", js: "classification", typ: r("Classification") },
        { json: "auteurs", js: "auteurs", typ: r("PurpleAuteurs") },
        { json: "correction", js: "correction", typ: u(r("Correction"), null) },
        { json: "notice", js: "notice", typ: r("Notice") },
        { json: "indexation", js: "indexation", typ: u(r("Indexation"), null) },
        { json: "imprimerie", js: "imprimerie", typ: u(r("Imprimerie"), null) },
        { json: "coSignataires", js: "coSignataires", typ: u(undefined, null) },
        { json: "depotAmendements", js: "depotAmendements", typ: u(undefined, null) },
    ], false),
    "PurpleAuteurs": o([
        { json: "auteur", js: "auteur", typ: u(a(r("AuteurElement")), r("PurpleAuteur")) },
    ], false),
    "PurpleAuteur": o([
        { json: "organe", js: "organe", typ: r("AuteurOrgane") },
    ], false),
    "FluffyDivisions": o([
        { json: "division", js: "division", typ: a(r("FluffyDivision")) },
    ], false),
    "FluffyDivision": o([
        { json: "@xsi:type", js: "@xsi:type", typ: r("DocumentXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "legislature", js: "legislature", typ: "" },
        { json: "cycleDeVie", js: "cycleDeVie", typ: r("CycleDeVie") },
        { json: "denominationStructurelle", js: "denominationStructurelle", typ: "" },
        { json: "titres", js: "titres", typ: r("Titres") },
        { json: "divisions", js: "divisions", typ: null },
        { json: "dossierRef", js: "dossierRef", typ: "" },
        { json: "redacteur", js: "redacteur", typ: null },
        { json: "classification", js: "classification", typ: r("Classification") },
        { json: "auteurs", js: "auteurs", typ: r("FluffyAuteurs") },
        { json: "correction", js: "correction", typ: null },
        { json: "notice", js: "notice", typ: r("Notice") },
        { json: "indexation", js: "indexation", typ: null },
        { json: "imprimerie", js: "imprimerie", typ: u(r("Imprimerie"), null) },
        { json: "coSignataires", js: "coSignataires", typ: u(undefined, null) },
        { json: "depotAmendements", js: "depotAmendements", typ: u(undefined, null) },
    ], false),
    "FluffyAuteurs": o([
        { json: "auteur", js: "auteur", typ: a(r("AuteurElement")) },
    ], false),
    "Imprimerie": o([
        { json: "prix", js: "prix", typ: "" },
    ], false),
    "Notice": o([
        { json: "numNotice", js: "numNotice", typ: u(undefined, "") },
        { json: "formule", js: "formule", typ: u(undefined, "") },
        { json: "adoptionConforme", js: "adoptionConforme", typ: "" },
    ], false),
    "Titres": o([
        { json: "titrePrincipal", js: "titrePrincipal", typ: "" },
        { json: "titrePrincipalCourt", js: "titrePrincipalCourt", typ: "" },
    ], false),
    "TentacledDivision": o([
        { json: "@xsi:type", js: "@xsi:type", typ: r("DocumentXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "legislature", js: "legislature", typ: u(null, "") },
        { json: "cycleDeVie", js: "cycleDeVie", typ: r("CycleDeVie") },
        { json: "denominationStructurelle", js: "denominationStructurelle", typ: "" },
        { json: "titres", js: "titres", typ: r("Titres") },
        { json: "divisions", js: "divisions", typ: null },
        { json: "dossierRef", js: "dossierRef", typ: "" },
        { json: "redacteur", js: "redacteur", typ: null },
        { json: "classification", js: "classification", typ: r("Classification") },
        { json: "auteurs", js: "auteurs", typ: r("FluffyAuteurs") },
        { json: "correction", js: "correction", typ: u(r("Correction"), null) },
        { json: "notice", js: "notice", typ: r("Notice") },
        { json: "indexation", js: "indexation", typ: u(r("Indexation"), null) },
        { json: "imprimerie", js: "imprimerie", typ: u(r("Imprimerie"), null) },
        { json: "coSignataires", js: "coSignataires", typ: u(undefined, null) },
        { json: "depotAmendements", js: "depotAmendements", typ: u(undefined, null) },
    ], false),
    "Indexation": o([
        { json: "themes", js: "themes", typ: r("Themes") },
    ], false),
    "Themes": o([
        { json: "@niveau", js: "@niveau", typ: "" },
        { json: "theme", js: "theme", typ: r("Theme") },
    ], false),
    "Theme": o([
        { json: "libelleTheme", js: "libelleTheme", typ: "" },
    ], false),
    "DossierParlementaire": o([
        { json: "dossierParlementaire", js: "dossierParlementaire", typ: r("DossierParlementaireDossierParlementaire") },
    ], false),
    "DossierParlementaireDossierParlementaire": o([
        { json: "@xmlns", js: "@xmlns", typ: "" },
        { json: "@xmlns:xsi", js: "@xmlns:xsi", typ: "" },
        { json: "@xsi:type", js: "@xsi:type", typ: r("DossierParlementaireXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "legislature", js: "legislature", typ: "" },
        { json: "titreDossier", js: "titreDossier", typ: r("TitreDossier") },
        { json: "procedureParlementaire", js: "procedureParlementaire", typ: r("ProcedureParlementaire") },
        { json: "initiateur", js: "initiateur", typ: u(r("DossierParlementaireInitiateur"), null) },
        { json: "actesLegislatifs", js: "actesLegislatifs", typ: r("PurpleActesLegislatifs") },
        { json: "fusionDossier", js: "fusionDossier", typ: u(r("FusionDossier"), null) },
        { json: "PLF", js: "PLF", typ: u(undefined, r("Plf")) },
    ], false),
    "Plf": o([
        { json: "EtudePLF", js: "EtudePLF", typ: a(r("EtudePlf")) },
    ], false),
    "EtudePlf": o([
        { json: "uid", js: "uid", typ: "" },
        { json: "organeRef", js: "organeRef", typ: "" },
        { json: "texteAssocie", js: "texteAssocie", typ: u(undefined, "") },
        { json: "rapporteur", js: "rapporteur", typ: u(undefined, u(a(r("RapporteursRapporteurClass")), r("RapporteursRapporteurClass"))) },
        { json: "missionMinefi", js: "missionMinefi", typ: u(undefined, r("MissionMinefiElement")) },
        { json: "ordreDIQS", js: "ordreDIQS", typ: "" },
        { json: "ordreCommission", js: "ordreCommission", typ: "" },
    ], false),
    "Missions": o([
        { json: "mission", js: "mission", typ: u(a(r("MissionMinefiElement")), r("MissionMinefiElement")) },
    ], false),
    "MissionMinefiElement": o([
        { json: "typeMission", js: "typeMission", typ: r("TypeMission") },
        { json: "libelleLong", js: "libelleLong", typ: "" },
        { json: "libelleCourt", js: "libelleCourt", typ: "" },
        { json: "typeBudget", js: "typeBudget", typ: r("TypeBudget") },
        { json: "missions", js: "missions", typ: u(undefined, r("Missions")) },
    ], false),
    "RapporteursRapporteurClass": o([
        { json: "acteurRef", js: "acteurRef", typ: "" },
        { json: "typeRapporteur", js: "typeRapporteur", typ: r("Qualite") },
    ], false),
    "PurpleActesLegislatifs": o([
        { json: "acteLegislatif", js: "acteLegislatif", typ: u(a(r("PurpleActeLegislatif")), r("AmbitiousActeLegislatif")) },
    ], false),
    "PurpleActeLegislatif": o([
        { json: "@xsi:type", js: "@xsi:type", typ: r("PurpleXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "codeActe", js: "codeActe", typ: r("HilariousCodeActe") },
        { json: "libelleActe", js: "libelleActe", typ: r("LibelleActe") },
        { json: "organeRef", js: "organeRef", typ: "" },
        { json: "dateActe", js: "dateActe", typ: null },
        { json: "actesLegislatifs", js: "actesLegislatifs", typ: r("FluffyActesLegislatifs") },
    ], false),
    "FluffyActesLegislatifs": o([
        { json: "acteLegislatif", js: "acteLegislatif", typ: u(a(r("FluffyActeLegislatif")), r("HilariousActeLegislatif")) },
    ], false),
    "FluffyActeLegislatif": o([
        { json: "@xsi:type", js: "@xsi:type", typ: r("PurpleXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "codeActe", js: "codeActe", typ: r("IndigoCodeActe") },
        { json: "libelleActe", js: "libelleActe", typ: r("LibelleActe") },
        { json: "organeRef", js: "organeRef", typ: "" },
        { json: "dateActe", js: "dateActe", typ: u(Date, null) },
        { json: "actesLegislatifs", js: "actesLegislatifs", typ: u(r("TentacledActesLegislatifs"), null) },
        { json: "texteAssocie", js: "texteAssocie", typ: u(undefined, "") },
        { json: "contributionInternaute", js: "contributionInternaute", typ: u(undefined, r("ContributionInternaute")) },
        { json: "provenance", js: "provenance", typ: u(undefined, "") },
        { json: "initiateur", js: "initiateur", typ: u(undefined, r("ActeLegislatifInitiateur")) },
        { json: "statutConclusion", js: "statutConclusion", typ: u(undefined, r("CasSaisine")) },
        { json: "reunionRef", js: "reunionRef", typ: u(undefined, null) },
        { json: "voteRefs", js: "voteRefs", typ: u(undefined, null) },
        { json: "casSaisine", js: "casSaisine", typ: u(undefined, r("CasSaisine")) },
        { json: "initiateurs", js: "initiateurs", typ: u(undefined, u(r("InitiateursClass"), null)) },
        { json: "motif", js: "motif", typ: u(undefined, r("Motif")) },
        { json: "urlConclusion", js: "urlConclusion", typ: u(undefined, "") },
        { json: "numDecision", js: "numDecision", typ: u(undefined, "") },
        { json: "anneeDecision", js: "anneeDecision", typ: u(undefined, "") },
    ], false),
    "TentacledActesLegislatifs": o([
        { json: "acteLegislatif", js: "acteLegislatif", typ: u(a(r("TentacledActeLegislatif")), r("IndecentActeLegislatif")) },
    ], false),
    "TentacledActeLegislatif": o([
        { json: "@xsi:type", js: "@xsi:type", typ: r("FluffyXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "codeActe", js: "codeActe", typ: r("TentacledCodeActe") },
        { json: "libelleActe", js: "libelleActe", typ: r("LibelleActe") },
        { json: "organeRef", js: "organeRef", typ: "" },
        { json: "dateActe", js: "dateActe", typ: u(Date, null) },
        { json: "actesLegislatifs", js: "actesLegislatifs", typ: u(r("StickyActesLegislatifs"), null) },
        { json: "reunionRef", js: "reunionRef", typ: u(undefined, "") },
        { json: "odjRef", js: "odjRef", typ: u(undefined, "") },
        { json: "statutConclusion", js: "statutConclusion", typ: u(undefined, r("CasSaisine")) },
        { json: "voteRefs", js: "voteRefs", typ: u(undefined, u(r("PurpleVoteRefs"), null)) },
        { json: "textesAssocies", js: "textesAssocies", typ: u(undefined, r("PurpleTextesAssocies")) },
        { json: "rapporteurs", js: "rapporteurs", typ: u(undefined, r("StickyRapporteurs")) },
        { json: "reunion", js: "reunion", typ: u(undefined, null) },
        { json: "texteAssocie", js: "texteAssocie", typ: u(undefined, "") },
        { json: "texteAdopte", js: "texteAdopte", typ: u(undefined, u(null, "")) },
    ], false),
    "StickyActesLegislatifs": o([
        { json: "acteLegislatif", js: "acteLegislatif", typ: u(a(r("ActesLegislatifsActeLegislatifClass")), r("StickyActeLegislatif")) },
    ], false),
    "ActesLegislatifsActeLegislatifClass": o([
        { json: "@xsi:type", js: "@xsi:type", typ: r("FluffyXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "codeActe", js: "codeActe", typ: r("PurpleCodeActe") },
        { json: "libelleActe", js: "libelleActe", typ: r("LibelleActe") },
        { json: "organeRef", js: "organeRef", typ: "" },
        { json: "dateActe", js: "dateActe", typ: Date },
        { json: "actesLegislatifs", js: "actesLegislatifs", typ: null },
        { json: "rapporteurs", js: "rapporteurs", typ: u(undefined, r("PurpleRapporteurs")) },
        { json: "reunion", js: "reunion", typ: u(undefined, null) },
        { json: "texteAssocie", js: "texteAssocie", typ: u(undefined, "") },
        { json: "texteAdopte", js: "texteAdopte", typ: u(undefined, u(null, "")) },
        { json: "reunionRef", js: "reunionRef", typ: u(undefined, u(null, "")) },
        { json: "odjRef", js: "odjRef", typ: u(undefined, u(null, "")) },
    ], false),
    "LibelleActe": o([
        { json: "nomCanonique", js: "nomCanonique", typ: "" },
        { json: "libelleCourt", js: "libelleCourt", typ: u(undefined, "") },
    ], false),
    "PurpleRapporteurs": o([
        { json: "rapporteur", js: "rapporteur", typ: u(a(r("PurpleRapporteur")), r("PurpleRapporteur")) },
    ], false),
    "PurpleRapporteur": o([
        { json: "acteurRef", js: "acteurRef", typ: "" },
        { json: "typeRapporteur", js: "typeRapporteur", typ: r("Qualite") },
        { json: "etudePLFRef", js: "etudePLFRef", typ: u(undefined, "") },
    ], false),
    "IndecentActesLegislatifs": o([
        { json: "acteLegislatif", js: "acteLegislatif", typ: u(a(r("StickyActeLegislatif")), r("StickyActeLegislatif")) },
    ], false),
    "IndigoActeLegislatif": o([
        { json: "@xsi:type", js: "@xsi:type", typ: r("FluffyXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "codeActe", js: "codeActe", typ: r("FluffyCodeActe") },
        { json: "libelleActe", js: "libelleActe", typ: r("LibelleActe") },
        { json: "organeRef", js: "organeRef", typ: "" },
        { json: "dateActe", js: "dateActe", typ: u(Date, null) },
        { json: "actesLegislatifs", js: "actesLegislatifs", typ: u(r("IndecentActesLegislatifs"), null) },
        { json: "texteAssocie", js: "texteAssocie", typ: u(undefined, "") },
        { json: "texteAdopte", js: "texteAdopte", typ: u(undefined, u(null, "")) },
        { json: "rapporteurs", js: "rapporteurs", typ: u(undefined, r("FluffyRapporteurs")) },
        { json: "reunion", js: "reunion", typ: u(undefined, null) },
        { json: "reunionRef", js: "reunionRef", typ: u(undefined, u(null, "")) },
        { json: "odjRef", js: "odjRef", typ: u(undefined, u(null, "")) },
        { json: "initiateurs", js: "initiateurs", typ: u(undefined, r("AuteurOrgane")) },
        { json: "statutConclusion", js: "statutConclusion", typ: u(undefined, r("CasSaisine")) },
        { json: "voteRefs", js: "voteRefs", typ: u(undefined, null) },
        { json: "typeDeclaration", js: "typeDeclaration", typ: u(undefined, r("CasSaisine")) },
    ], false),
    "IndigoActesLegislatifs": o([
        { json: "acteLegislatif", js: "acteLegislatif", typ: u(a(r("IndigoActeLegislatif")), r("StickyActeLegislatif")) },
    ], false),
    "StickyActeLegislatif": o([
        { json: "@xsi:type", js: "@xsi:type", typ: r("FluffyXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "codeActe", js: "codeActe", typ: r("FluffyCodeActe") },
        { json: "libelleActe", js: "libelleActe", typ: r("LibelleActe") },
        { json: "organeRef", js: "organeRef", typ: "" },
        { json: "dateActe", js: "dateActe", typ: u(Date, null) },
        { json: "actesLegislatifs", js: "actesLegislatifs", typ: u(r("IndigoActesLegislatifs"), null) },
        { json: "texteAssocie", js: "texteAssocie", typ: u(undefined, "") },
        { json: "texteAdopte", js: "texteAdopte", typ: u(undefined, null) },
        { json: "initiateurs", js: "initiateurs", typ: u(undefined, r("AuteurOrgane")) },
        { json: "rapporteurs", js: "rapporteurs", typ: u(undefined, r("TentacledRapporteurs")) },
        { json: "reunion", js: "reunion", typ: u(undefined, null) },
    ], false),
    "FluffyRapporteurs": o([
        { json: "rapporteur", js: "rapporteur", typ: u(a(r("RapporteursRapporteurClass")), r("RapporteursRapporteurClass")) },
    ], false),
    "CasSaisine": o([
        { json: "fam_code", js: "fam_code", typ: r("FamCode") },
        { json: "libelle", js: "libelle", typ: "" },
    ], false),
    "TentacledRapporteurs": o([
        { json: "rapporteur", js: "rapporteur", typ: r("RapporteursRapporteurClass") },
    ], false),
    "StickyRapporteurs": o([
        { json: "rapporteur", js: "rapporteur", typ: a(r("RapporteursRapporteurClass")) },
    ], false),
    "PurpleTextesAssocies": o([
        { json: "texteAssocie", js: "texteAssocie", typ: u(a(r("TexteAssocieElement")), r("TexteAssocieElement")) },
    ], false),
    "TexteAssocieElement": o([
        { json: "typeTexte", js: "typeTexte", typ: r("TypeTexte") },
        { json: "refTexteAssocie", js: "refTexteAssocie", typ: "" },
    ], false),
    "PurpleVoteRefs": o([
        { json: "voteRef", js: "voteRef", typ: u(a(""), "") },
    ], false),
    "IndecentActeLegislatif": o([
        { json: "@xsi:type", js: "@xsi:type", typ: r("FluffyXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "codeActe", js: "codeActe", typ: r("StickyCodeActe") },
        { json: "libelleActe", js: "libelleActe", typ: r("LibelleActe") },
        { json: "organeRef", js: "organeRef", typ: "" },
        { json: "dateActe", js: "dateActe", typ: u(Date, null) },
        { json: "actesLegislatifs", js: "actesLegislatifs", typ: u(r("IndigoActesLegislatifs"), null) },
        { json: "statutConclusion", js: "statutConclusion", typ: u(undefined, r("CasSaisine")) },
        { json: "reunionRef", js: "reunionRef", typ: u(undefined, u(null, "")) },
        { json: "voteRefs", js: "voteRefs", typ: u(undefined, null) },
        { json: "textesAssocies", js: "textesAssocies", typ: u(undefined, r("FluffyTextesAssocies")) },
        { json: "odjRef", js: "odjRef", typ: u(undefined, "") },
    ], false),
    "FluffyTextesAssocies": o([
        { json: "texteAssocie", js: "texteAssocie", typ: a(r("TexteAssocieElement")) },
    ], false),
    "ContributionInternaute": o([
        { json: "dateFermeture", js: "dateFermeture", typ: u(null, "") },
        { json: "dateOuverture", js: "dateOuverture", typ: u(undefined, "") },
    ], false),
    "ActeLegislatifInitiateur": o([
        { json: "acteurs", js: "acteurs", typ: r("PurpleActeurs") },
    ], false),
    "PurpleActeurs": o([
        { json: "acteur", js: "acteur", typ: r("ActeurElement") },
    ], false),
    "ActeurElement": o([
        { json: "acteurRef", js: "acteurRef", typ: "" },
        { json: "mandatRef", js: "mandatRef", typ: "" },
    ], false),
    "HilariousActeLegislatif": o([
        { json: "@xsi:type", js: "@xsi:type", typ: r("TentacledXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "codeActe", js: "codeActe", typ: r("IndecentCodeActe") },
        { json: "libelleActe", js: "libelleActe", typ: r("LibelleActe") },
        { json: "organeRef", js: "organeRef", typ: "" },
        { json: "dateActe", js: "dateActe", typ: Date },
        { json: "actesLegislatifs", js: "actesLegislatifs", typ: null },
        { json: "texteLoiRef", js: "texteLoiRef", typ: u(undefined, "") },
        { json: "infoJO", js: "infoJO", typ: u(undefined, r("PurpleInfoJo")) },
        { json: "urlEcheancierLoi", js: "urlEcheancierLoi", typ: u(undefined, u(null, "")) },
        { json: "codeLoi", js: "codeLoi", typ: u(undefined, "") },
        { json: "titreLoi", js: "titreLoi", typ: u(undefined, "") },
        { json: "infoJORect", js: "infoJORect", typ: u(undefined, r("PurpleInfoJo")) },
        { json: "urlLegifrance", js: "urlLegifrance", typ: u(undefined, null) },
        { json: "texteAssocie", js: "texteAssocie", typ: u(undefined, "") },
        { json: "texteAdopte", js: "texteAdopte", typ: u(undefined, null) },
        { json: "casSaisine", js: "casSaisine", typ: u(undefined, r("CasSaisine")) },
        { json: "initiateurs", js: "initiateurs", typ: u(undefined, null) },
        { json: "motif", js: "motif", typ: u(undefined, r("Motif")) },
    ], false),
    "PurpleInfoJo": o([
        { json: "typeJO", js: "typeJO", typ: r("TypeJo") },
        { json: "dateJO", js: "dateJO", typ: "" },
        { json: "pageJO", js: "pageJO", typ: null },
        { json: "numJO", js: "numJO", typ: "" },
        { json: "urlLegifrance", js: "urlLegifrance", typ: "" },
        { json: "referenceNOR", js: "referenceNOR", typ: "" },
    ], false),
    "AmbitiousActeLegislatif": o([
        { json: "@xsi:type", js: "@xsi:type", typ: r("PurpleXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "codeActe", js: "codeActe", typ: r("HilariousCodeActe") },
        { json: "libelleActe", js: "libelleActe", typ: r("LibelleActe") },
        { json: "organeRef", js: "organeRef", typ: "" },
        { json: "dateActe", js: "dateActe", typ: null },
        { json: "actesLegislatifs", js: "actesLegislatifs", typ: r("HilariousActesLegislatifs") },
    ], false),
    "HilariousActesLegislatifs": o([
        { json: "acteLegislatif", js: "acteLegislatif", typ: u(a(r("CunningActeLegislatif")), r("StickyActeLegislatif")) },
    ], false),
    "CunningActeLegislatif": o([
        { json: "@xsi:type", js: "@xsi:type", typ: r("PurpleXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "codeActe", js: "codeActe", typ: r("IndigoCodeActe") },
        { json: "libelleActe", js: "libelleActe", typ: r("LibelleActe") },
        { json: "organeRef", js: "organeRef", typ: "" },
        { json: "dateActe", js: "dateActe", typ: u(Date, null) },
        { json: "actesLegislatifs", js: "actesLegislatifs", typ: u(r("AmbitiousActesLegislatifs"), null) },
        { json: "texteAssocie", js: "texteAssocie", typ: u(undefined, "") },
        { json: "contributionInternaute", js: "contributionInternaute", typ: u(undefined, r("ContributionInternaute")) },
        { json: "typeMotionCensure", js: "typeMotionCensure", typ: u(undefined, r("CasSaisine")) },
        { json: "auteurs", js: "auteurs", typ: u(undefined, r("ActeLegislatifAuteurs")) },
        { json: "dateRetrait", js: "dateRetrait", typ: u(undefined, null) },
    ], false),
    "AmbitiousActesLegislatifs": o([
        { json: "acteLegislatif", js: "acteLegislatif", typ: u(a(r("MagentaActeLegislatif")), r("FriskyActeLegislatif")) },
    ], false),
    "MagentaActeLegislatif": o([
        { json: "@xsi:type", js: "@xsi:type", typ: r("FluffyXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "codeActe", js: "codeActe", typ: r("AmbitiousCodeActe") },
        { json: "libelleActe", js: "libelleActe", typ: r("LibelleActe") },
        { json: "organeRef", js: "organeRef", typ: "" },
        { json: "dateActe", js: "dateActe", typ: u(Date, null) },
        { json: "actesLegislatifs", js: "actesLegislatifs", typ: u(r("StickyActesLegislatifs"), null) },
        { json: "reunionRef", js: "reunionRef", typ: u(undefined, u(null, "")) },
        { json: "odjRef", js: "odjRef", typ: u(undefined, "") },
        { json: "typeMotion", js: "typeMotion", typ: u(undefined, r("CasSaisine")) },
        { json: "auteurMotion", js: "auteurMotion", typ: u(undefined, "") },
        { json: "dateRetrait", js: "dateRetrait", typ: u(undefined, null) },
        { json: "voteRefs", js: "voteRefs", typ: u(undefined, u(r("FluffyVoteRefs"), null)) },
        { json: "statutConclusion", js: "statutConclusion", typ: u(undefined, r("CasSaisine")) },
        { json: "textesAssocies", js: "textesAssocies", typ: u(undefined, r("PurpleTextesAssocies")) },
        { json: "decision", js: "decision", typ: u(undefined, r("CasSaisine")) },
        { json: "odSeancejRef", js: "odSeancejRef", typ: u(undefined, null) },
    ], false),
    "FluffyVoteRefs": o([
        { json: "voteRef", js: "voteRef", typ: "" },
    ], false),
    "FriskyActeLegislatif": o([
        { json: "@xsi:type", js: "@xsi:type", typ: r("FluffyXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "codeActe", js: "codeActe", typ: r("StickyCodeActe") },
        { json: "libelleActe", js: "libelleActe", typ: r("LibelleActe") },
        { json: "organeRef", js: "organeRef", typ: "" },
        { json: "dateActe", js: "dateActe", typ: u(Date, null) },
        { json: "actesLegislatifs", js: "actesLegislatifs", typ: u(r("IndigoActesLegislatifs"), null) },
        { json: "statutConclusion", js: "statutConclusion", typ: u(undefined, r("CasSaisine")) },
        { json: "reunionRef", js: "reunionRef", typ: u(undefined, u(null, "")) },
        { json: "voteRefs", js: "voteRefs", typ: u(undefined, u(r("FluffyVoteRefs"), null)) },
        { json: "textesAssocies", js: "textesAssocies", typ: u(undefined, r("TentacledTextesAssocies")) },
        { json: "odjRef", js: "odjRef", typ: u(undefined, "") },
        { json: "decision", js: "decision", typ: u(undefined, r("CasSaisine")) },
        { json: "odSeancejRef", js: "odSeancejRef", typ: u(undefined, null) },
    ], false),
    "TentacledTextesAssocies": o([
        { json: "texteAssocie", js: "texteAssocie", typ: r("TexteAssocieElement") },
    ], false),
    "ActeLegislatifAuteurs": o([
        { json: "acteurRef", js: "acteurRef", typ: a("") },
    ], false),
    "FusionDossier": o([
        { json: "cause", js: "cause", typ: r("Cause") },
        { json: "dossierAbsorbantRef", js: "dossierAbsorbantRef", typ: "" },
    ], false),
    "DossierParlementaireInitiateur": o([
        { json: "acteurs", js: "acteurs", typ: u(undefined, r("FluffyActeurs")) },
        { json: "organes", js: "organes", typ: u(undefined, r("Organes")) },
    ], false),
    "FluffyActeurs": o([
        { json: "acteur", js: "acteur", typ: u(a(r("ActeurElement")), r("ActeurElement")) },
    ], false),
    "Organes": o([
        { json: "organe", js: "organe", typ: r("OrganesOrgane") },
    ], false),
    "OrganesOrgane": o([
        { json: "organeRef", js: "organeRef", typ: r("OrganeRefClass") },
    ], false),
    "OrganeRefClass": o([
        { json: "uid", js: "uid", typ: "" },
    ], false),
    "TitreDossier": o([
        { json: "titre", js: "titre", typ: "" },
        { json: "titreChemin", js: "titreChemin", typ: u(null, "") },
        { json: "senatChemin", js: "senatChemin", typ: u(null, "") },
    ], false),
    "DossiersLegislatifs": o([
        { json: "export", js: "export", typ: r("Export") },
    ], false),
    "Export": o([
        { json: "@xmlns:xsi", js: "@xmlns:xsi", typ: "" },
        { json: "textesLegislatifs", js: "textesLegislatifs", typ: r("TextesLegislatifs") },
        { json: "dossiersLegislatifs", js: "dossiersLegislatifs", typ: r("DossiersLegislatifsClass") },
    ], false),
    "DossiersLegislatifsClass": o([
        { json: "dossier", js: "dossier", typ: a(r("Dossier")) },
    ], false),
    "Dossier": o([
        { json: "dossierParlementaire", js: "dossierParlementaire", typ: r("DossierDossierParlementaire") },
    ], false),
    "DossierDossierParlementaire": o([
        { json: "@xsi:type", js: "@xsi:type", typ: u(undefined, r("DossierParlementaireXsiType")) },
        { json: "uid", js: "uid", typ: "" },
        { json: "legislature", js: "legislature", typ: "" },
        { json: "titreDossier", js: "titreDossier", typ: r("TitreDossier") },
        { json: "procedureParlementaire", js: "procedureParlementaire", typ: r("ProcedureParlementaire") },
        { json: "initiateur", js: "initiateur", typ: u(r("DossierParlementaireInitiateur"), null) },
        { json: "actesLegislatifs", js: "actesLegislatifs", typ: r("CunningActesLegislatifs") },
        { json: "fusionDossier", js: "fusionDossier", typ: u(r("FusionDossier"), null) },
        { json: "indexation", js: "indexation", typ: u(undefined, r("Indexation")) },
        { json: "PLF", js: "PLF", typ: u(undefined, r("Plf")) },
    ], false),
    "CunningActesLegislatifs": o([
        { json: "acteLegislatif", js: "acteLegislatif", typ: u(a(r("MischievousActeLegislatif")), r("ActeLegislatif3")) },
    ], false),
    "MischievousActeLegislatif": o([
        { json: "@xsi:type", js: "@xsi:type", typ: r("PurpleXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "codeActe", js: "codeActe", typ: r("HilariousCodeActe") },
        { json: "libelleActe", js: "libelleActe", typ: r("LibelleActe") },
        { json: "organeRef", js: "organeRef", typ: u(undefined, "") },
        { json: "dateActe", js: "dateActe", typ: null },
        { json: "actesLegislatifs", js: "actesLegislatifs", typ: r("MagentaActesLegislatifs") },
    ], false),
    "MagentaActesLegislatifs": o([
        { json: "acteLegislatif", js: "acteLegislatif", typ: u(a(r("BraggadociousActeLegislatif")), r("ActeLegislatif2")) },
    ], false),
    "BraggadociousActeLegislatif": o([
        { json: "@xsi:type", js: "@xsi:type", typ: r("PurpleXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "codeActe", js: "codeActe", typ: r("IndigoCodeActe") },
        { json: "libelleActe", js: "libelleActe", typ: r("LibelleActe") },
        { json: "organeRef", js: "organeRef", typ: u(undefined, "") },
        { json: "dateActe", js: "dateActe", typ: u(Date, null) },
        { json: "actesLegislatifs", js: "actesLegislatifs", typ: u(r("FriskyActesLegislatifs"), null) },
        { json: "texteAssocie", js: "texteAssocie", typ: u(undefined, "") },
        { json: "provenance", js: "provenance", typ: u(undefined, "") },
        { json: "contributionInternaute", js: "contributionInternaute", typ: u(undefined, r("ContributionInternaute")) },
        { json: "initiateur", js: "initiateur", typ: u(undefined, r("ActeLegislatifInitiateur")) },
        { json: "statutConclusion", js: "statutConclusion", typ: u(undefined, r("CasSaisine")) },
        { json: "reunionRef", js: "reunionRef", typ: u(undefined, null) },
        { json: "voteRefs", js: "voteRefs", typ: u(undefined, null) },
        { json: "casSaisine", js: "casSaisine", typ: u(undefined, r("CasSaisine")) },
        { json: "initiateurs", js: "initiateurs", typ: u(undefined, u(r("InitiateursClass"), null)) },
        { json: "motif", js: "motif", typ: u(undefined, r("Motif")) },
        { json: "urlConclusion", js: "urlConclusion", typ: u(undefined, "") },
        { json: "numDecision", js: "numDecision", typ: u(undefined, "") },
        { json: "anneeDecision", js: "anneeDecision", typ: u(undefined, "") },
        { json: "typeDeclaration", js: "typeDeclaration", typ: u(undefined, r("CasSaisine")) },
        { json: "typeMotionCensure", js: "typeMotionCensure", typ: u(undefined, r("CasSaisine")) },
        { json: "auteurs", js: "auteurs", typ: u(undefined, r("ActeLegislatifAuteurs")) },
        { json: "dateRetrait", js: "dateRetrait", typ: u(undefined, null) },
        { json: "texteEuropeen", js: "texteEuropeen", typ: u(undefined, r("TexteEuropeen")) },
        { json: "infoJOCE", js: "infoJOCE", typ: u(undefined, r("InfoJoce")) },
        { json: "statutAdoption", js: "statutAdoption", typ: u(undefined, r("CasSaisine")) },
    ], false),
    "FriskyActesLegislatifs": o([
        { json: "acteLegislatif", js: "acteLegislatif", typ: u(a(r("ActeLegislatif1")), r("FriskyActeLegislatif")) },
    ], false),
    "ActeLegislatif1": o([
        { json: "@xsi:type", js: "@xsi:type", typ: r("FluffyXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "codeActe", js: "codeActe", typ: r("TentacledCodeActe") },
        { json: "libelleActe", js: "libelleActe", typ: r("LibelleActe") },
        { json: "organeRef", js: "organeRef", typ: "" },
        { json: "dateActe", js: "dateActe", typ: u(Date, null) },
        { json: "actesLegislatifs", js: "actesLegislatifs", typ: u(r("MischievousActesLegislatifs"), null) },
        { json: "reunionRef", js: "reunionRef", typ: u(undefined, u(null, "")) },
        { json: "odjRef", js: "odjRef", typ: u(undefined, "") },
        { json: "statutConclusion", js: "statutConclusion", typ: u(undefined, r("CasSaisine")) },
        { json: "voteRefs", js: "voteRefs", typ: u(undefined, u(r("PurpleVoteRefs"), null)) },
        { json: "textesAssocies", js: "textesAssocies", typ: u(undefined, r("PurpleTextesAssocies")) },
        { json: "typeMotion", js: "typeMotion", typ: u(undefined, r("CasSaisine")) },
        { json: "auteurMotion", js: "auteurMotion", typ: u(undefined, "") },
        { json: "dateRetrait", js: "dateRetrait", typ: u(undefined, null) },
        { json: "rapporteurs", js: "rapporteurs", typ: u(undefined, r("StickyRapporteurs")) },
        { json: "reunion", js: "reunion", typ: u(undefined, null) },
        { json: "texteAssocie", js: "texteAssocie", typ: u(undefined, "") },
        { json: "texteAdopte", js: "texteAdopte", typ: u(undefined, null) },
        { json: "auteurs", js: "auteurs", typ: u(undefined, u(r("InitiateursClass"), null)) },
        { json: "decision", js: "decision", typ: u(undefined, r("CasSaisine")) },
        { json: "odSeancejRef", js: "odSeancejRef", typ: u(undefined, null) },
    ], false),
    "MischievousActesLegislatifs": o([
        { json: "acteLegislatif", js: "acteLegislatif", typ: a(r("ActesLegislatifsActeLegislatifClass")) },
    ], false),
    "InfoJoce": o([
        { json: "refJOCE", js: "refJOCE", typ: "" },
        { json: "dateJOCE", js: "dateJOCE", typ: "" },
    ], false),
    "TexteEuropeen": o([
        { json: "typeTexteEuropeen", js: "typeTexteEuropeen", typ: "" },
        { json: "titreTexteEuropeen", js: "titreTexteEuropeen", typ: "" },
    ], false),
    "ActeLegislatif2": o([
        { json: "@xsi:type", js: "@xsi:type", typ: r("TentacledXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "codeActe", js: "codeActe", typ: r("CunningCodeActe") },
        { json: "libelleActe", js: "libelleActe", typ: r("LibelleActe") },
        { json: "organeRef", js: "organeRef", typ: "" },
        { json: "dateActe", js: "dateActe", typ: Date },
        { json: "actesLegislatifs", js: "actesLegislatifs", typ: null },
        { json: "texteLoiRef", js: "texteLoiRef", typ: u(undefined, "") },
        { json: "infoJO", js: "infoJO", typ: u(undefined, r("InfoJoElement")) },
        { json: "urlLegifrance", js: "urlLegifrance", typ: u(undefined, "") },
        { json: "referenceNOR", js: "referenceNOR", typ: u(undefined, "") },
        { json: "codeLoi", js: "codeLoi", typ: u(undefined, "") },
        { json: "titreLoi", js: "titreLoi", typ: u(undefined, "") },
        { json: "infoJORect", js: "infoJORect", typ: u(undefined, u(a(r("InfoJoElement")), r("InfoJoElement"))) },
        { json: "texteAssocie", js: "texteAssocie", typ: u(undefined, "") },
        { json: "texteAdopte", js: "texteAdopte", typ: u(undefined, null) },
    ], false),
    "InfoJoElement": o([
        { json: "typeJO", js: "typeJO", typ: r("TypeJo") },
        { json: "dateJO", js: "dateJO", typ: "" },
        { json: "pageJO", js: "pageJO", typ: null },
        { json: "numJO", js: "numJO", typ: "" },
    ], false),
    "ActeLegislatif3": o([
        { json: "@xsi:type", js: "@xsi:type", typ: r("PurpleXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "codeActe", js: "codeActe", typ: r("HilariousCodeActe") },
        { json: "libelleActe", js: "libelleActe", typ: r("LibelleActe") },
        { json: "organeRef", js: "organeRef", typ: "" },
        { json: "dateActe", js: "dateActe", typ: null },
        { json: "actesLegislatifs", js: "actesLegislatifs", typ: r("BraggadociousActesLegislatifs") },
    ], false),
    "BraggadociousActesLegislatifs": o([
        { json: "acteLegislatif", js: "acteLegislatif", typ: u(a(r("ActeLegislatif4")), r("IndigoActeLegislatif")) },
    ], false),
    "ActeLegislatif4": o([
        { json: "@xsi:type", js: "@xsi:type", typ: r("PurpleXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "codeActe", js: "codeActe", typ: r("IndigoCodeActe") },
        { json: "libelleActe", js: "libelleActe", typ: r("LibelleActe") },
        { json: "organeRef", js: "organeRef", typ: "" },
        { json: "dateActe", js: "dateActe", typ: u(Date, null) },
        { json: "actesLegislatifs", js: "actesLegislatifs", typ: u(r("ActesLegislatifs1"), null) },
        { json: "texteAssocie", js: "texteAssocie", typ: u(undefined, "") },
        { json: "contributionInternaute", js: "contributionInternaute", typ: u(undefined, r("ContributionInternaute")) },
        { json: "decision", js: "decision", typ: u(undefined, r("CasSaisine")) },
        { json: "formuleDecision", js: "formuleDecision", typ: u(undefined, "") },
        { json: "typeDeclaration", js: "typeDeclaration", typ: u(undefined, r("CasSaisine")) },
        { json: "typeMotionCensure", js: "typeMotionCensure", typ: u(undefined, r("CasSaisine")) },
        { json: "auteurs", js: "auteurs", typ: u(undefined, r("ActeLegislatifAuteurs")) },
        { json: "dateRetrait", js: "dateRetrait", typ: u(undefined, null) },
    ], false),
    "ActesLegislatifs1": o([
        { json: "acteLegislatif", js: "acteLegislatif", typ: u(a(r("ActeLegislatif5")), r("FriskyActeLegislatif")) },
    ], false),
    "ActeLegislatif5": o([
        { json: "@xsi:type", js: "@xsi:type", typ: r("FluffyXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "codeActe", js: "codeActe", typ: r("AmbitiousCodeActe") },
        { json: "libelleActe", js: "libelleActe", typ: r("LibelleActe") },
        { json: "organeRef", js: "organeRef", typ: "" },
        { json: "dateActe", js: "dateActe", typ: u(Date, null) },
        { json: "actesLegislatifs", js: "actesLegislatifs", typ: u(r("IndigoActesLegislatifs"), null) },
        { json: "reunionRef", js: "reunionRef", typ: u(undefined, u(null, "")) },
        { json: "odjRef", js: "odjRef", typ: u(undefined, "") },
        { json: "statutConclusion", js: "statutConclusion", typ: u(undefined, r("CasSaisine")) },
        { json: "voteRefs", js: "voteRefs", typ: u(undefined, u(r("FluffyVoteRefs"), null)) },
        { json: "textesAssocies", js: "textesAssocies", typ: u(undefined, r("TentacledTextesAssocies")) },
        { json: "typeMotion", js: "typeMotion", typ: u(undefined, r("CasSaisine")) },
        { json: "auteurMotion", js: "auteurMotion", typ: u(undefined, "") },
        { json: "dateRetrait", js: "dateRetrait", typ: u(undefined, null) },
        { json: "decision", js: "decision", typ: u(undefined, r("CasSaisine")) },
        { json: "odSeancejRef", js: "odSeancejRef", typ: u(undefined, null) },
    ], false),
    "TextesLegislatifs": o([
        { json: "document", js: "document", typ: a(r("DocumentElement")) },
    ], false),
    "DocumentElement": o([
        { json: "@xsi:type", js: "@xsi:type", typ: r("DocumentXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "legislature", js: "legislature", typ: u(null, "") },
        { json: "cycleDeVie", js: "cycleDeVie", typ: r("CycleDeVie") },
        { json: "denominationStructurelle", js: "denominationStructurelle", typ: r("DocumentDenominationStructurelle") },
        { json: "provenance", js: "provenance", typ: u(undefined, r("Provenance")) },
        { json: "titres", js: "titres", typ: r("Titres") },
        { json: "divisions", js: "divisions", typ: u(r("TentacledDivisions"), null) },
        { json: "dossierRef", js: "dossierRef", typ: "" },
        { json: "redacteur", js: "redacteur", typ: null },
        { json: "classification", js: "classification", typ: r("Classification") },
        { json: "auteurs", js: "auteurs", typ: r("DocumentAuteurs") },
        { json: "correction", js: "correction", typ: u(r("Correction"), null) },
        { json: "notice", js: "notice", typ: r("Notice") },
        { json: "indexation", js: "indexation", typ: u(r("Indexation"), null) },
        { json: "imprimerie", js: "imprimerie", typ: u(r("Imprimerie"), null) },
        { json: "coSignataires", js: "coSignataires", typ: u(undefined, u(r("CoSignataires"), null)) },
        { json: "depotAmendements", js: "depotAmendements", typ: u(undefined, u(r("DepotAmendements"), null)) },
    ], false),
    "TentacledDivisions": o([
        { json: "division", js: "division", typ: u(a(r("StickyDivision")), r("TentacledDivision")) },
    ], false),
    "StickyDivision": o([
        { json: "@xsi:type", js: "@xsi:type", typ: r("DocumentXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "legislature", js: "legislature", typ: u(null, "") },
        { json: "cycleDeVie", js: "cycleDeVie", typ: r("CycleDeVie") },
        { json: "denominationStructurelle", js: "denominationStructurelle", typ: "" },
        { json: "titres", js: "titres", typ: r("Titres") },
        { json: "divisions", js: "divisions", typ: u(r("StickyDivisions"), null) },
        { json: "dossierRef", js: "dossierRef", typ: "" },
        { json: "redacteur", js: "redacteur", typ: null },
        { json: "classification", js: "classification", typ: r("Classification") },
        { json: "auteurs", js: "auteurs", typ: r("PurpleAuteurs") },
        { json: "correction", js: "correction", typ: u(r("Correction"), null) },
        { json: "notice", js: "notice", typ: r("Notice") },
        { json: "indexation", js: "indexation", typ: u(r("Indexation"), null) },
        { json: "imprimerie", js: "imprimerie", typ: u(r("Imprimerie"), null) },
        { json: "coSignataires", js: "coSignataires", typ: u(undefined, null) },
        { json: "depotAmendements", js: "depotAmendements", typ: u(undefined, null) },
    ], false),
    "StickyDivisions": o([
        { json: "division", js: "division", typ: a(r("IndigoDivision")) },
    ], false),
    "IndigoDivision": o([
        { json: "@xsi:type", js: "@xsi:type", typ: r("DocumentXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "legislature", js: "legislature", typ: "" },
        { json: "cycleDeVie", js: "cycleDeVie", typ: r("CycleDeVie") },
        { json: "denominationStructurelle", js: "denominationStructurelle", typ: "" },
        { json: "titres", js: "titres", typ: r("Titres") },
        { json: "divisions", js: "divisions", typ: null },
        { json: "dossierRef", js: "dossierRef", typ: "" },
        { json: "redacteur", js: "redacteur", typ: null },
        { json: "classification", js: "classification", typ: r("Classification") },
        { json: "auteurs", js: "auteurs", typ: r("FluffyAuteurs") },
        { json: "correction", js: "correction", typ: null },
        { json: "notice", js: "notice", typ: r("Notice") },
        { json: "indexation", js: "indexation", typ: u(r("Indexation"), null) },
        { json: "imprimerie", js: "imprimerie", typ: u(r("Imprimerie"), null) },
    ], false),
    "DocumentXsiType": [
        "accordInternational_Type",
        "avisConseilEtat_Type",
        "documentEtudeImpact_Type",
        "rapportParlementaire_Type",
        "texteLoi_Type",
    ],
    "Qualite": [
        "auteur",
        "rapporteur",
        "rapporteur général",
        "rapporteur pour avis",
        "rapporteur spécial",
    ],
    "Code": [
        "ACCPRESRP",
        "APPART1515",
        "APPART341",
        "APPLOI",
        "AUE",
        "AUTRATCONV",
        "COMENQ",
        "COMPA",
        "COMSPCPTE",
        "CONST",
        "CTRLBUDG",
        "DIVERS",
        "ENGRESPTXT",
        "ENQU",
        "FIN",
        "FINRECT",
        "FINSSOC",
        "FINSSOCREC",
        "IMPACTLOIS",
        "LEGDELEGAN",
        "MINFOCOMPER",
        "MODREGLTAN",
        "OFFPARL",
        "ORG",
        "PROPACTCOM",
        "PRPDIT",
        "RECT",
        "REFART11",
        "RGLTBUDG",
        "SUPP",
        "SUSPOURS",
        "TVXINSTITEUROP",
    ],
    "StatutAdoption": [
        "ADOPTCOM",
    ],
    "TypeCorrection": [
        "Rectifié",
    ],
    "DocumentDenominationStructurelle": [
        "Avis",
        "Déclaration",
        "Lettre",
        "Projet de loi",
        "Proposition de loi",
        "Proposition de résolution",
        "Rapport",
        "Rapport d'information",
    ],
    "Provenance": [
        "Commission",
        "Texte Déposé",
    ],
    "DossierParlementaireXsiType": [
        "DossierCommissionEnquete_Type",
        "DossierIniativeExecutif_Type",
        "DossierLegislatif_Type",
        "DossierMissionControle_Type",
        "DossierMissionInformation_Type",
        "DossierResolutionAN",
    ],
    "TypeBudget": [
        "Budget annexe",
        "Budget général",
        "Compte de concours financier",
        "Compte spécial",
        "Première partie",
    ],
    "TypeMission": [
        "mission principale",
        "mission secondaire",
        "partie de mission",
    ],
    "PurpleXsiType": [
        "Adoption_Europe_Type",
        "ConclusionEtapeCC_Type",
        "DecisionRecevabiliteBureau_Type",
        "Decision_Type",
        "DeclarationGouvernement_Type",
        "DepotAccordInternational_Type",
        "DepotAvisConseilEtat_Type",
        "DepotInitiativeNavette_Type",
        "DepotInitiative_Type",
        "DepotLettreRectificative_Type",
        "DepotMotionCensure_Type",
        "Etape_Type",
        "EtudeImpact_Type",
        "ProcedureAccelere_Type",
        "RenvoiCMP_Type",
        "RetraitInitiative_Type",
        "SaisineConseilConstit_Type",
    ],
    "FluffyXsiType": [
        "CreationOrganeTemporaire_Type",
        "DecisionMotionCensure_Type",
        "Decision_Type",
        "DeclarationGouvernement_Type",
        "DepotInitiative_Type",
        "DepotMotionReferendaire_Type",
        "DepotRapport_Type",
        "DiscussionCommission_Type",
        "DiscussionSeancePublique_Type",
        "Etape_Type",
        "MotionProcedure_Type",
        "NominRapporteurs_Type",
        "RenvoiPrealable_Type",
        "SaisieComAvis_Type",
        "SaisieComFond_Type",
    ],
    "PurpleCodeActe": [
        "AN1-COM-AVIS-NOMIN",
        "AN1-COM-AVIS-RAPPORT",
        "AN1-COM-AVIS-REUNION",
        "AN1-COM-AVIS-SAISIE",
        "AN1-COM-FOND-NOMIN",
        "AN1-COM-FOND-RAPPORT",
        "AN1-COM-FOND-REUNION",
        "AN1-COM-FOND-SAISIE",
        "AN2-COM-AVIS-RAPPORT",
        "AN2-COM-AVIS-REUNION",
        "AN2-COM-AVIS-SAISIE",
        "AN2-COM-FOND-RAPPORT",
        "AN2-COM-FOND-REUNION",
        "AN2-COM-FOND-SAISIE",
        "ANLUNI-COM-CAE-NOMIN",
        "ANLUNI-COM-CAE-RAPPORT",
        "ANLUNI-COM-CAE-SAISIE",
        "ANLUNI-COM-FOND-NOMIN",
        "ANLUNI-COM-FOND-RAPPORT",
        "ANLUNI-COM-FOND-REUNION",
        "ANLUNI-COM-FOND-SAISIE",
        "ANNLEC-COM-AVIS-NOMIN",
        "ANNLEC-COM-AVIS-RAPPORT",
        "ANNLEC-COM-AVIS-REUNION",
        "ANNLEC-COM-AVIS-SAISIE",
        "ANNLEC-COM-FOND-RAPPORT",
        "ANNLEC-COM-FOND-REUNION",
        "ANNLEC-COM-FOND-SAISIE",
        "SN1-COM-AVIS-NOMIN",
        "SN1-COM-AVIS-RAPPORT",
        "SN1-COM-AVIS-SAISIE",
        "SN1-COM-FOND-NOMIN",
        "SN1-COM-FOND-RAPPORT",
        "SN1-COM-FOND-SAISIE",
        "SN2-COM-AVIS-NOMIN",
        "SN2-COM-AVIS-RAPPORT",
        "SN2-COM-AVIS-SAISIE",
        "SN2-COM-FOND-RAPPORT",
        "SN2-COM-FOND-SAISIE",
    ],
    "FluffyCodeActe": [
        "AN1-COM-AVIS-NOMIN",
        "AN1-COM-AVIS-RAPPORT",
        "AN1-COM-AVIS-REUNION",
        "AN1-COM-AVIS-SAISIE",
        "AN1-COM-FOND-NOMIN",
        "AN1-COM-FOND-RAPPORT",
        "AN1-COM-FOND-REUNION",
        "AN1-COM-FOND-SAISIE",
        "AN1-DEPOT",
        "AN20-COMENQ",
        "AN20-COMENQ-CREA",
        "AN20-COMENQ-NOMIN",
        "AN20-COMENQ-RAPPORT",
        "AN20-MISINF",
        "AN20-MISINF-CREA",
        "AN20-MISINF-NOMIN",
        "AN20-MISINF-RAPPORT",
        "AN20-RAPPORT",
        "AN21-APAN",
        "AN21-DGVT",
        "AN2-COM-FOND-NOMIN",
        "AN2-COM-FOND-RAPPORT",
        "AN2-COM-FOND-REUNION",
        "AN2-COM-FOND-SAISIE",
        "ANLDEF-COM-FOND-RAPPORT",
        "ANLDEF-COM-FOND-REUNION",
        "ANLDEF-COM-FOND-SAISIE",
        "ANLUNI-COM-CAE-DEC",
        "ANLUNI-COM-CAE-NOMIN",
        "ANLUNI-COM-CAE-RAPPORT",
        "ANLUNI-COM-CAE-REUNION",
        "ANLUNI-COM-CAE-SAISIE",
        "ANLUNI-COM-FOND-NOMIN",
        "ANLUNI-COM-FOND-RAPPORT",
        "ANLUNI-COM-FOND-REUNION",
        "ANLUNI-COM-FOND-SAISIE",
        "ANLUNI-DEPOT",
        "ANNLEC-COM-FOND-NOMIN",
        "ANNLEC-COM-FOND-RAPPORT",
        "ANNLEC-COM-FOND-REUNION",
        "ANNLEC-COM-FOND-SAISIE",
        "SN1-COM-AVIS-NOMIN",
        "SN1-COM-AVIS-RAPPORT",
        "SN1-COM-AVIS-SAISIE",
        "SN1-COM-FOND-NOMIN",
        "SN1-COM-FOND-RAPPORT",
        "SN1-COM-FOND-SAISIE",
        "SN2-COM-FOND-NOMIN",
        "SN2-COM-FOND-RAPPORT",
        "SN2-COM-FOND-SAISIE",
        "SN3-COM-FOND-RAPPORT",
        "SN3-COM-FOND-SAISIE",
        "SNNLEC-COM-FOND-NOMIN",
        "SNNLEC-COM-FOND-RAPPORT",
        "SNNLEC-COM-FOND-SAISIE",
    ],
    "FamCode": [
        "Art.49.3",
        "ETTD01",
        "TCCMP01",
        "TCCMP02",
        "TCD01",
        "TCD02",
        "TCD03",
        "TCD04",
        "02",
        "TMP02",
        "TMP03",
        "TMP05",
        "TMP06",
        "TMRC01",
        "TSCCONT01",
        "TSCCONT02",
        "TSCCONT03",
        "TSCCONT04",
        "TSCCONT05",
        "TSCCONT06",
        "TSCCONT07",
        "TSORTF01",
        "TSORTF02",
        "TSORTF03",
        "TSORTF04",
        "TSORTF05",
        "TSORTF06",
        "TSORTF07",
        "TSORTF14",
        "TSORTF18",
        "TSORTF19",
        "TSORTF20",
        "TSORTF21",
        "TSORTF23",
        "TSORTMOT01",
        "TSORTMOT02",
    ],
    "TentacledCodeActe": [
        "AN1-COM-AVIS",
        "AN1-COM-FOND",
        "AN1-DEBATS-DEC",
        "AN1-DEBATS-MOTION",
        "AN1-DEBATS-MOTION-VOTE",
        "AN1-DEBATS-SEANCE",
        "AN2-COM-AVIS",
        "AN2-COM-FOND",
        "AN2-DEBATS-DEC",
        "AN2-DEBATS-SEANCE",
        "ANLDEF-DEBATS-DEC",
        "ANLDEF-DEBATS-SEANCE",
        "ANLUNI-DEBATS-DEC",
        "ANLUNI-DEBATS-SEANCE",
        "ANNLEC-COM-AVIS",
        "ANNLEC-COM-FOND",
        "ANNLEC-DEBATS-DEC",
        "ANNLEC-DEBATS-MOTION-VOTE",
        "ANNLEC-DEBATS-SEANCE",
        "CMP-COM-NOMIN",
        "CMP-COM-RAPPORT-AN",
        "CMP-COM-RAPPORT-SN",
        "CMP-DEBATS-AN-DEC",
        "CMP-DEBATS-AN-SEANCE",
        "CMP-DEBATS-SN-DEC",
        "CMP-DEBATS-SN-SEANCE",
        "SN1-COM-AVIS",
        "SN1-COM-FOND",
        "SN1-DEBATS-DEC",
        "SN1-DEBATS-MOTION",
        "SN1-DEBATS-MOTION-VOTE",
        "SN1-DEBATS-SEANCE",
        "SN2-COM-AVIS",
        "SN2-COM-FOND",
        "SN2-DEBATS-DEC",
        "SN2-DEBATS-SEANCE",
        "SN3-DEBATS-DEC",
        "SN3-DEBATS-SEANCE",
        "SNNLEC-DEBATS-DEC",
        "SNNLEC-DEBATS-SEANCE",
    ],
    "TypeTexte": [
        "BTA",
        "TAP",
    ],
    "StickyCodeActe": [
        "AN1-COM-FOND",
        "AN1-DEBATS-DEC",
        "AN1-DEBATS-SEANCE",
        "AN21-DEBATS-MOTION-VOTE",
        "AN21-DEBATS-SEANCE",
        "AN2-COM-FOND",
        "ANLDEF-COM-FOND",
        "ANLDEF-DEBATS-DEC",
        "ANLUNI-COM-CAE",
        "ANLUNI-COM-FOND",
        "ANLUNI-DEBATS-DEC",
        "ANLUNI-DEBATS-SEANCE",
        "ANNLEC-COM-FOND",
        "CMP-DEBATS-AN-DEC",
        "CMP-DEBATS-AN-SEANCE",
        "CMP-DEBATS-SN-SEANCE",
        "SN1-COM-FOND",
        "SN1-DEBATS-DEC",
        "SN1-DEBATS-SEANCE",
        "SN2-COM-FOND",
        "SN2-DEBATS-SEANCE",
        "SN3-COM-FOND",
        "SNNLEC-COM-FOND",
    ],
    "IndigoCodeActe": [
        "AN1-ACIN",
        "AN1-AVCE",
        "AN1-COM",
        "AN1-DEBATS",
        "AN1-DEPOT",
        "AN1-DGVT",
        "AN1-DPTLETTRECT",
        "AN1-ETI",
        "AN1-MOTION",
        "AN1-PROCACC",
        "AN1-RECBUREAU",
        "AN1-RTRINI",
        "AN21-DEBATS",
        "AN21-DGVT",
        "AN21-MOTION",
        "AN2-COM",
        "AN2-DEBATS",
        "AN2-DEPOT",
        "ANLDEF-COM",
        "ANLDEF-DEBATS",
        "ANLDEF-DEPOT",
        "ANLDEF-DGVT",
        "ANLUNI-COM",
        "ANLUNI-DEBATS",
        "ANLUNI-DEPOT",
        "ANLUNI-RTRINI",
        "ANNLEC-COM",
        "ANNLEC-DEBATS",
        "ANNLEC-DEPOT",
        "ANNLEC-DGVT",
        "ANNLEC-MOTION",
        "CC-CONCLUSION",
        "CC-SAISIE-AN",
        "CC-SAISIE-DROIT",
        "CC-SAISIE-PAN",
        "CC-SAISIE-PM",
        "CC-SAISIE-PR",
        "CC-SAISIE-PSN",
        "CC-SAISIE-SN",
        "CMP-COM",
        "CMP-DEBATS-AN",
        "CMP-DEBATS-SN",
        "CMP-DEC",
        "CMP-DEPOT",
        "CMP-SAISIE",
        "EU-DEC",
        "SN1-AVCE",
        "SN1-COM",
        "SN1-DEBATS",
        "SN1-DEPOT",
        "SN1-DPTLETTRECT",
        "SN1-ETI",
        "SN1-PROCACC",
        "SN1-RTRINI",
        "SN2-COM",
        "SN2-DEBATS",
        "SN2-DEPOT",
        "SN3-COM",
        "SN3-DEBATS",
        "SN3-DEPOT",
        "SNNLEC-COM",
        "SNNLEC-DEBATS",
        "SNNLEC-DEPOT",
    ],
    "Motif": [
        "En application de l'article 61§2 de la Constitution",
    ],
    "TentacledXsiType": [
        "DepotRapport_Type",
        "Promulgation_Type",
        "SaisineConseilConstit_Type",
    ],
    "IndecentCodeActe": [
        "AN20-RAPPORT",
        "AN-APPLI-RAPPORT",
        "CC-SAISIE-SN",
        "PROM-PUB",
    ],
    "TypeJo": [
        "JO_LOI_DECRET",
    ],
    "HilariousCodeActe": [
        "AN1",
        "AN2",
        "AN20",
        "AN21",
        "AN-APPLI",
        "ANLDEF",
        "ANLUNI",
        "ANNLEC",
        "CC",
        "CMP",
        "EU",
        "PROM",
        "SN1",
        "SN2",
        "SN3",
        "SNNLEC",
    ],
    "AmbitiousCodeActe": [
        "AN1-COM-AVIS",
        "AN1-COM-FOND",
        "AN1-DEBATS-DEC",
        "AN1-DEBATS-MOTION",
        "AN1-DEBATS-MOTION-VOTE",
        "AN1-DEBATS-SEANCE",
        "AN21-DEBATS-MOTION-VOTE",
        "AN21-DEBATS-SEANCE",
        "ANLUNI-COM-CAE",
        "ANLUNI-COM-FOND",
        "ANLUNI-DEBATS-DEC",
        "ANLUNI-DEBATS-SEANCE",
        "SN1-COM-AVIS",
        "SN1-COM-FOND",
        "SN1-DEBATS-DEC",
        "SN1-DEBATS-SEANCE",
    ],
    "Cause": [
        "Dossier absorbé",
        "Examen commun",
    ],
    "CunningCodeActe": [
        "AN20-RAPPORT",
        "AN-APPLI-RAPPORT",
        "PROM-PUB",
    ],
};
