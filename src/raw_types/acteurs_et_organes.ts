// To parse this data:
//
//   import { Convert, Acteur, Organe, Mandat, ActeursEtOrganes } from "./file";
//
//   const acteur = Convert.toActeur(json);
//   const organe = Convert.toOrgane(json);
//   const mandat = Convert.toMandat(json);
//   const acteursEtOrganes = Convert.toActeursEtOrganes(json);
//
// These functions will throw an error if the JSON doesn't
// match the expected interface, even if the JSON is valid.

export interface Acteur {
    acteur: ActeurActeur;
}

export interface ActeurActeur {
    "@xmlns"?:     string;
    uid:           PurpleUid;
    etatCivil:     PurpleEtatCivil;
    profession:    Profession;
    uri_hatvp:     UriHatvpClass | null | string;
    adresses:      Adresses | null;
    mandats?:      PurpleMandats;
    "@xmlns:xsi"?: string;
}

export interface Adresses {
    adresse?:      { [key: string]: null | string }[] | { [key: string]: null | string };
    "@xmlns:xsi"?: string;
    "@xsi:nil"?:   string;
}

export interface PurpleEtatCivil {
    ident:         PurpleIdent;
    infoNaissance: PurpleInfoNaissance;
    dateDeces:     UriHatvpClass | Date | null;
}

export interface UriHatvpClass {
    "@xmlns:xsi": string;
    "@xsi:nil":   string;
}

export interface PurpleIdent {
    civ:       Civ;
    prenom:    string;
    nom:       string;
    alpha:     string;
    trigramme: UriHatvpClass | null | string;
}

export enum Civ {
    M = "M.",
    Mme = "Mme",
}

export interface PurpleInfoNaissance {
    dateNais:  Date;
    villeNais: UriHatvpClass | null | string;
    depNais:   UriHatvpClass | null | string;
    paysNais:  UriHatvpClass | PaysNaisEnum | null;
}

export enum PaysNaisEnum {
    Algérie = "Algérie",
    Allemagne = "Allemagne",
    Belgique = "Belgique",
    Brésil = "Brésil",
    BurkinaFaso = "Burkina Faso",
    Cambodge = "Cambodge",
    Canada = "Canada",
    Centrafrique = "Centrafrique",
    Chili = "Chili",
    CoréeDuSud = "Corée du Sud",
    Cuba = "Cuba",
    CôteDIvoire = "Côte d'Ivoire",
    Espagne = "Espagne",
    EtatsUnisDAmérique = "Etats-Unis d'Amérique",
    Fr = "FR",
    France = "France",
    Gabon = "Gabon",
    GrandeBretagne = "Grande Bretagne",
    Grèce = "Grèce",
    Iran = "Iran",
    Liban = "Liban",
    Madagascar = "Madagascar",
    Maroc = "Maroc",
    MarylandUsa = "Maryland (USA)",
    PaysNaisAllemagne = "ALLEMAGNE",
    PaysNaisFrance = "FRANCE",
    PolynésieFrancaise = "Polynésie Francaise",
    PolynésieFrançaise = "Polynésie française",
    PurpleFrance = "france",
    Rwanda = "Rwanda",
    SaintMartin = "Saint-Martin",
    Senegal = "Senegal",
    Serbie = "SERBIE",
    Seychelles = "Seychelles",
    Suisse = "Suisse",
    Sénégal = "Sénégal",
    Thaïlande = "Thaïlande",
    Togo = "Togo",
    Tunisie = "Tunisie",
    Turquie = "Turquie",
    Ukraine = "Ukraine",
    Vietnam = "Vietnam",
}

export interface PurpleMandats {
    mandat: PurpleMandat[] | FluffyMandat;
}

export interface PurpleMandat {
    "@xmlns:xsi"?:         string;
    "@xsi:type"?:          MandatXsiType;
    uid:                   string;
    acteurRef:             string;
    legislature:           null | string;
    typeOrgane:            CodeType;
    dateDebut:             Date;
    datePublication:       Date | null;
    dateFin:               Date | null;
    preseance:             null | string;
    nominPrincipale:       string;
    infosQualite:          InfosQualite;
    organes:               PurpleOrganes;
    libelle?:              null | string;
    missionSuivanteRef?:   null | string;
    missionPrecedenteRef?: null | string;
    suppleants?:           Suppleants | null;
    chambre?:              null;
    election?:             PurpleElection;
    mandature?:            Mandature;
    collaborateurs?:       null[] | Collaborateurs | null;
}

export enum MandatXsiType {
    MandatAvecSuppleantType = "MandatAvecSuppleant_Type",
    MandatMissionType = "MandatMission_Type",
    MandatParlementaireType = "MandatParlementaire_type",
    MandatSimpleType = "MandatSimple_Type",
}

export interface Collaborateurs {
    collaborateur: CollaborateurElement[] | CollaborateurElement;
}

export interface CollaborateurElement {
    qualite:   Civ;
    prenom:    string;
    nom:       string;
    dateDebut: null;
    dateFin:   null;
}

export interface PurpleElection {
    lieu:                Lieu;
    causeMandat:         null | string;
    refCirconscription?: null | string;
}

export interface Lieu {
    region:         Region | null;
    regionType:     RegionType | null;
    departement:    null | string;
    numDepartement: null | string;
    numCirco:       null | string;
}

export enum Region {
    AuvergneRhôneAlpes = "Auvergne-Rhône-Alpes",
    BourgogneFrancheComté = "Bourgogne-Franche-Comté",
    Bretagne = "Bretagne",
    CentreValDeLoire = "Centre-Val de Loire",
    Corse = "Corse",
    FrançaisÉtablisHorsDeFrance = "Français établis hors de France",
    GrandEst = "Grand Est",
    Guadeloupe = "Guadeloupe",
    Guyane = "Guyane",
    HautsDeFrance = "Hauts-de-France",
    IleDeFrance = "Ile-de-France",
    Martinique = "Martinique",
    Mayotte = "Mayotte",
    Normandie = "Normandie",
    NouvelleAquitaine = "Nouvelle-Aquitaine",
    NouvelleCalédonie = "Nouvelle-Calédonie",
    Occitanie = "Occitanie",
    PaysDeLaLoire = "Pays de la Loire",
    PolynésieFrançaise = "Polynésie française",
    ProvenceAlpesCôteDAzur = "Provence-Alpes-Côte d'Azur",
    Réunion = "Réunion",
    SaintBarthélemyEtSaintMartin = "Saint-Barthélemy et Saint-Martin",
    SaintPierreEtMiquelon = "Saint-Pierre-et-Miquelon",
    WallisEtFutuna = "Wallis-et-Futuna",
}

export enum RegionType {
    CollectivitésDOutreMerEtNouvelleCalédonie = "Collectivités d'outre-mer et Nouvelle-Calédonie",
    Dom = "Dom",
    FrançaisÉtablisHorsDeFrance = "Français établis hors de France",
    Métropolitain = "Métropolitain",
}

export interface InfosQualite {
    codeQualite:   null | string;
    libQualite:    string;
    libQualiteSex: null | string;
}

export interface Mandature {
    datePriseFonction:  Date | null;
    causeFin:           null | string;
    premiereElection:   null | string;
    placeHemicycle:     null | string;
    mandatRemplaceRef?: null | string;
}

export interface PurpleOrganes {
    organeRef: string[] | string;
}

export interface Suppleants {
    suppleant: Suppleant;
}

export interface Suppleant {
    dateDebut:    Date;
    dateFin:      Date | null;
    suppleantRef: string;
}

export enum CodeType {
    Api = "API",
    Assemblee = "ASSEMBLEE",
    Cjr = "CJR",
    Cmp = "CMP",
    Cnpe = "CNPE",
    Cnps = "CNPS",
    Comnl = "COMNL",
    Comper = "COMPER",
    Comsenat = "COMSENAT",
    Comspsenat = "COMSPSENAT",
    Confpt = "CONFPT",
    Constitu = "CONSTITU",
    Deleg = "DELEG",
    Delegbureau = "DELEGBUREAU",
    Delegsenat = "DELEGSENAT",
    Ga = "GA",
    Ge = "GE",
    Gevi = "GEVI",
    Gouvernement = "GOUVERNEMENT",
    Gp = "GP",
    Groupesenat = "GROUPESENAT",
    Hcj = "HCJ",
    Ministere = "MINISTERE",
    Misinfo = "MISINFO",
    Misinfocom = "MISINFOCOM",
    Misinfopre = "MISINFOPRE",
    Offpar = "OFFPAR",
    Orgaint = "ORGAINT",
    Orgextparl = "ORGEXTPARL",
    Parpol = "PARPOL",
    Presrep = "PRESREP",
    Senat = "SENAT",
}

export interface FluffyMandat {
    "@xmlns:xsi":    string;
    "@xsi:type":     MandatXsiType;
    uid:             string;
    acteurRef:       string;
    legislature:     null;
    typeOrgane:      CodeType;
    dateDebut:       Date;
    datePublication: null;
    dateFin:         Date | null;
    preseance:       string;
    nominPrincipale: string;
    infosQualite:    InfosQualite;
    organes:         FluffyOrganes;
    suppleants:      null;
    chambre:         null;
    election:        FluffyElection;
    mandature:       Mandature;
    collaborateurs:  null;
}

export interface FluffyElection {
    lieu:        Lieu;
    causeMandat: null;
}

export interface FluffyOrganes {
    organeRef: string;
}

export interface Profession {
    libelleCourant: UriHatvpClass | null | string;
    socProcINSEE:   SocProcInsee;
}

export interface SocProcInsee {
    catSocPro: UriHatvpClass | null | string;
    famSocPro: UriHatvpClass | FamSocProEnum | null;
}

export enum FamSocProEnum {
    AgriculteursExploitants = "Agriculteurs exploitants",
    ArtisansCommerçantsEtChefsDEntreprise = "Artisans, commerçants et chefs d'entreprise",
    AutresPersonnesSansActivitéProfessionnelle = "Autres personnes sans activité professionnelle",
    CadresEtProfessionsIntellectuellesSupérieures = "Cadres et professions intellectuelles supérieures",
    Employés = "Employés",
    Ouvriers = "Ouvriers",
    ProfessionsIntermédiaires = "Professions Intermédiaires",
    Retraités = "Retraités",
    SansProfessionDéclarée = "Sans profession déclarée",
}

export interface PurpleUid {
    "@xmlns:xsi"?: string;
    "@xsi:type":   UidXsiType;
    "#text":       string;
}

export enum UidXsiType {
    IdActeurType = "IdActeur_type",
}

export interface Organe {
    organe: OrganeOrgane;
}

export interface OrganeOrgane {
    "@xmlns"?:                string;
    "@xmlns:xsi":             string;
    "@xsi:type"?:             OrganeXsiType;
    uid:                      string;
    codeType:                 CodeType;
    libelle:                  string;
    libelleEdition:           null | string;
    libelleAbrege:            string;
    libelleAbrev:             string;
    viMoDe:                   ViMoDe;
    organeParent:             OrganeParent | null;
    chambre?:                 null;
    regime?:                  Regime | null;
    legislature?:             null | string;
    regimeJuridique?:         null | string;
    siteInternet?:            null | string;
    nombreReunionsAnnuelles?: null | string;
    secretariat?:             Secretariat;
    positionPolitique?:       PositionPolitique | null;
    preseance?:               null | string;
    couleurAssociee?:         null | string;
    listePays?:               ListePays | null;
}

export enum OrganeXsiType {
    GroupePolitiqueType = "GroupePolitique_type",
    OrganeExterneType = "OrganeExterne_Type",
    OrganeExtraParlementaireType = "OrganeExtraParlementaire_type",
    OrganeParlementaireInternational = "OrganeParlementaireInternational",
    OrganeParlementaireType = "OrganeParlementaire_Type",
}

export interface ListePays {
    paysRef: string;
}

export enum OrganeParent {
    Po268150 = "PO268150",
    Po300736 = "PO300736",
    Po312969 = "PO312969",
    Po382939 = "PO382939",
    Po384206 = "PO384206",
    Po419604 = "PO419604",
    Po419610 = "PO419610",
    Po419865 = "PO419865",
    Po420120 = "PO420120",
    Po425695 = "PO425695",
    Po58828 = "PO58828",
    Po58829 = "PO58829",
    Po58830 = "PO58830",
    Po58831 = "PO58831",
    Po590011 = "PO590011",
    Po59046 = "PO59046",
    Po59047 = "PO59047",
    Po59048 = "PO59048",
    Po59051 = "PO59051",
    Po653199 = "PO653199",
    Po700706 = "PO700706",
    Po702883 = "PO702883",
    Po715793 = "PO715793",
    Po717173 = "PO717173",
    Po725688 = "PO725688",
}

export enum PositionPolitique {
    Majoritaire = "Majoritaire",
    Minoritaire = "Minoritaire",
    Opposition = "Opposition",
}

export enum Regime {
    AssembléeConsultativeProvisoire = "Assemblée consultative provisoire",
    AssembléesNationalesConstituantes = "Assemblées nationales constituantes",
    The4ÈmeRépublique = "4ème République",
    The5ÈmeRépublique = "5ème République",
}

export interface Secretariat {
    secretaire01: null | string;
    secretaire02: Secretaire02 | null;
}

export enum Secretaire02 {
    MFrédéricTaillet = "M. Frédéric Taillet",
    MmeAnneRocchesani = "Mme Anne Rocchesani",
    MmeAudeMénoret = "Mme Aude Ménoret",
    MmeMarieWencker = "Mme Marie Wencker",
}

export interface ViMoDe {
    dateDebut:    Date | null;
    dateAgrement: Date | null;
    dateFin:      Date | null;
}

export interface Mandat {
    mandat: PurpleMandat;
}

export interface ActeursEtOrganes {
    export: Export;
}

export interface Export {
    "@xmlns:xsi": string;
    organes:      ExportOrganes;
    acteurs:      Acteurs;
}

export interface Acteurs {
    acteur: ActeurElement[];
}

export interface ActeurElement {
    uid:        FluffyUid;
    etatCivil:  FluffyEtatCivil;
    uri_hatvp?: null | string;
    mandats:    FluffyMandats;
}

export interface FluffyEtatCivil {
    ident:         FluffyIdent;
    infoNaissance: FluffyInfoNaissance;
    dateDeces:     Date | null;
}

export interface FluffyIdent {
    civ:       Civ;
    prenom:    string;
    nom:       string;
    alpha:     string;
    trigramme: null | string;
}

export interface FluffyInfoNaissance {
    dateNais:  Date;
    villeNais: null | string;
    depNais:   null | string;
    paysNais:  PaysNaisEnum | null;
}

export interface FluffyMandats {
    mandat: TentacledMandat[] | StickyMandat;
}

export interface TentacledMandat {
    "@xsi:type":           MandatXsiType;
    uid:                   string;
    acteurRef:             string;
    legislature:           null | string;
    typeOrgane:            CodeType;
    dateDebut:             Date;
    datePublication:       Date | null;
    dateFin:               Date | null;
    preseance:             null | string;
    nominPrincipale:       string;
    infosQualite:          InfosQualite;
    organes:               PurpleOrganes;
    suppleants?:           Suppleants | null;
    election?:             PurpleElection;
    InfosHorsSIAN?:        InfosHorsSian;
    mandature?:            Mandature;
    libelle?:              null | string;
    missionPrecedenteRef?: null | string;
    missionSuivanteRef?:   null | string;
}

export interface InfosHorsSian {
    HATVP_URI: null;
}

export interface StickyMandat {
    "@xsi:type":     MandatXsiType;
    uid:             string;
    acteurRef:       string;
    legislature:     null;
    typeOrgane:      CodeType;
    dateDebut:       Date;
    datePublication: null;
    dateFin:         Date;
    preseance:       string;
    nominPrincipale: string;
    infosQualite:    InfosQualite;
    organes:         FluffyOrganes;
    suppleants:      null;
    election:        FluffyElection;
    InfosHorsSIAN:   InfosHorsSian;
    mandature:       Mandature;
}

export interface FluffyUid {
    "@xsi:type": UidXsiType;
    "#text":     string;
}

export interface ExportOrganes {
    organe: OrganeElement[];
}

export interface OrganeElement {
    "@xsi:type":        OrganeXsiType;
    uid:                string;
    codeType:           CodeType;
    libelle:            string;
    libelleEdition:     null | string;
    libelleAbrege:      string;
    libelleAbrev:       string;
    viMoDe:             ViMoDe;
    regimeJuridique?:   null | string;
    regime?:            Regime | null;
    legislature?:       null | string;
    secretariat?:       Secretariat;
    positionPolitique?: PositionPolitique | null;
}

// Converts JSON strings to/from your types
// and asserts the results of JSON.parse at runtime
export class Convert {
    public static toActeur(json: string): Acteur {
        return cast(JSON.parse(json), r("Acteur"));
    }

    public static acteurToJson(value: Acteur): string {
        return JSON.stringify(uncast(value, r("Acteur")), null, 2);
    }

    public static toOrgane(json: string): Organe {
        return cast(JSON.parse(json), r("Organe"));
    }

    public static organeToJson(value: Organe): string {
        return JSON.stringify(uncast(value, r("Organe")), null, 2);
    }

    public static toMandat(json: string): Mandat {
        return cast(JSON.parse(json), r("Mandat"));
    }

    public static mandatToJson(value: Mandat): string {
        return JSON.stringify(uncast(value, r("Mandat")), null, 2);
    }

    public static toActeursEtOrganes(json: string): ActeursEtOrganes {
        return cast(JSON.parse(json), r("ActeursEtOrganes"));
    }

    public static acteursEtOrganesToJson(value: ActeursEtOrganes): string {
        return JSON.stringify(uncast(value, r("ActeursEtOrganes")), null, 2);
    }
}

function invalidValue(typ: any, val: any): never {
    throw Error(`Invalid value ${JSON.stringify(val)} for type ${JSON.stringify(typ)}`);
}

function jsonToJSProps(typ: any): any {
    if (typ.jsonToJS === undefined) {
        var map: any = {};
        typ.props.forEach((p: any) => map[p.json] = { key: p.js, typ: p.typ });
        typ.jsonToJS = map;
    }
    return typ.jsonToJS;
}

function jsToJSONProps(typ: any): any {
    if (typ.jsToJSON === undefined) {
        var map: any = {};
        typ.props.forEach((p: any) => map[p.js] = { key: p.json, typ: p.typ });
        typ.jsToJSON = map;
    }
    return typ.jsToJSON;
}

function transform(val: any, typ: any, getProps: any): any {
    function transformPrimitive(typ: string, val: any): any {
        if (typeof typ === typeof val) return val;
        return invalidValue(typ, val);
    }

    function transformUnion(typs: any[], val: any): any {
        // val must validate against one typ in typs
        var l = typs.length;
        for (var i = 0; i < l; i++) {
            var typ = typs[i];
            try {
                return transform(val, typ, getProps);
            } catch (_) {}
        }
        return invalidValue(typs, val);
    }

    function transformEnum(cases: string[], val: any): any {
        if (cases.indexOf(val) !== -1) return val;
        return invalidValue(cases, val);
    }

    function transformArray(typ: any, val: any): any {
        // val must be an array with no invalid elements
        if (!Array.isArray(val)) return invalidValue("array", val);
        return val.map(el => transform(el, typ, getProps));
    }

    function transformDate(_typ: any, val: any): any {
        if (val === null) {
            return null;
        }
        const d = new Date(val);
        if (isNaN(d.valueOf())) {
            return invalidValue("Date", val);
        }
        return d;
    }

    function transformObject(props: { [k: string]: any }, additional: any, val: any): any {
        if (val === null || typeof val !== "object" || Array.isArray(val)) {
            return invalidValue("object", val);
        }
        var result: any = {};
        Object.getOwnPropertyNames(props).forEach(key => {
            const prop = props[key];
            const v = Object.prototype.hasOwnProperty.call(val, key) ? val[key] : undefined;
            result[prop.key] = transform(v, prop.typ, getProps);
        });
        Object.getOwnPropertyNames(val).forEach(key => {
            if (!Object.prototype.hasOwnProperty.call(props, key)) {
                result[key] = transform(val[key], additional, getProps);
            }
        });
        return result;
    }

    if (typ === "any") return val;
    if (typ === null) {
        if (val === null) return val;
        return invalidValue(typ, val);
    }
    if (typ === false) return invalidValue(typ, val);
    while (typeof typ === "object" && typ.ref !== undefined) {
        typ = typeMap[typ.ref];
    }
    if (Array.isArray(typ)) return transformEnum(typ, val);
    if (typeof typ === "object") {
        return typ.hasOwnProperty("unionMembers") ? transformUnion(typ.unionMembers, val)
            : typ.hasOwnProperty("arrayItems")    ? transformArray(typ.arrayItems, val)
            : typ.hasOwnProperty("props")         ? transformObject(getProps(typ), typ.additional, val)
            : invalidValue(typ, val);
    }
    // Numbers can be parsed by Date but shouldn't be.
    if (typ === Date && typeof val !== "number") return transformDate(typ, val);
    return transformPrimitive(typ, val);
}

function cast<T>(val: any, typ: any): T {
    return transform(val, typ, jsonToJSProps);
}

function uncast<T>(val: T, typ: any): any {
    return transform(val, typ, jsToJSONProps);
}

function a(typ: any) {
    return { arrayItems: typ };
}

function u(...typs: any[]) {
    return { unionMembers: typs };
}

function o(props: any[], additional: any) {
    return { props, additional };
}

function m(additional: any) {
    return { props: [], additional };
}

function r(name: string) {
    return { ref: name };
}

const typeMap: any = {
    "Acteur": o([
        { json: "acteur", js: "acteur", typ: r("ActeurActeur") },
    ], false),
    "ActeurActeur": o([
        { json: "@xmlns", js: "@xmlns", typ: u(undefined, "") },
        { json: "uid", js: "uid", typ: r("PurpleUid") },
        { json: "etatCivil", js: "etatCivil", typ: r("PurpleEtatCivil") },
        { json: "profession", js: "profession", typ: r("Profession") },
        { json: "uri_hatvp", js: "uri_hatvp", typ: u(r("UriHatvpClass"), null, "") },
        { json: "adresses", js: "adresses", typ: u(r("Adresses"), null) },
        { json: "mandats", js: "mandats", typ: u(undefined, r("PurpleMandats")) },
        { json: "@xmlns:xsi", js: "@xmlns:xsi", typ: u(undefined, "") },
    ], false),
    "Adresses": o([
        { json: "adresse", js: "adresse", typ: u(undefined, u(a(m(u(null, ""))), m(u(null, "")))) },
        { json: "@xmlns:xsi", js: "@xmlns:xsi", typ: u(undefined, "") },
        { json: "@xsi:nil", js: "@xsi:nil", typ: u(undefined, "") },
    ], false),
    "PurpleEtatCivil": o([
        { json: "ident", js: "ident", typ: r("PurpleIdent") },
        { json: "infoNaissance", js: "infoNaissance", typ: r("PurpleInfoNaissance") },
        { json: "dateDeces", js: "dateDeces", typ: u(r("UriHatvpClass"), Date, null) },
    ], false),
    "UriHatvpClass": o([
        { json: "@xmlns:xsi", js: "@xmlns:xsi", typ: "" },
        { json: "@xsi:nil", js: "@xsi:nil", typ: "" },
    ], false),
    "PurpleIdent": o([
        { json: "civ", js: "civ", typ: r("Civ") },
        { json: "prenom", js: "prenom", typ: "" },
        { json: "nom", js: "nom", typ: "" },
        { json: "alpha", js: "alpha", typ: "" },
        { json: "trigramme", js: "trigramme", typ: u(r("UriHatvpClass"), null, "") },
    ], false),
    "PurpleInfoNaissance": o([
        { json: "dateNais", js: "dateNais", typ: Date },
        { json: "villeNais", js: "villeNais", typ: u(r("UriHatvpClass"), null, "") },
        { json: "depNais", js: "depNais", typ: u(r("UriHatvpClass"), null, "") },
        { json: "paysNais", js: "paysNais", typ: u(r("UriHatvpClass"), r("PaysNaisEnum"), null) },
    ], false),
    "PurpleMandats": o([
        { json: "mandat", js: "mandat", typ: u(a(r("PurpleMandat")), r("FluffyMandat")) },
    ], false),
    "PurpleMandat": o([
        { json: "@xmlns:xsi", js: "@xmlns:xsi", typ: u(undefined, "") },
        { json: "@xsi:type", js: "@xsi:type", typ: u(undefined, r("MandatXsiType")) },
        { json: "uid", js: "uid", typ: "" },
        { json: "acteurRef", js: "acteurRef", typ: "" },
        { json: "legislature", js: "legislature", typ: u(null, "") },
        { json: "typeOrgane", js: "typeOrgane", typ: r("CodeType") },
        { json: "dateDebut", js: "dateDebut", typ: Date },
        { json: "datePublication", js: "datePublication", typ: u(Date, null) },
        { json: "dateFin", js: "dateFin", typ: u(Date, null) },
        { json: "preseance", js: "preseance", typ: u(null, "") },
        { json: "nominPrincipale", js: "nominPrincipale", typ: "" },
        { json: "infosQualite", js: "infosQualite", typ: r("InfosQualite") },
        { json: "organes", js: "organes", typ: r("PurpleOrganes") },
        { json: "libelle", js: "libelle", typ: u(undefined, u(null, "")) },
        { json: "missionSuivanteRef", js: "missionSuivanteRef", typ: u(undefined, u(null, "")) },
        { json: "missionPrecedenteRef", js: "missionPrecedenteRef", typ: u(undefined, u(null, "")) },
        { json: "suppleants", js: "suppleants", typ: u(undefined, u(r("Suppleants"), null)) },
        { json: "chambre", js: "chambre", typ: u(undefined, null) },
        { json: "election", js: "election", typ: u(undefined, r("PurpleElection")) },
        { json: "mandature", js: "mandature", typ: u(undefined, r("Mandature")) },
        { json: "collaborateurs", js: "collaborateurs", typ: u(undefined, u(a(null), r("Collaborateurs"), null)) },
    ], false),
    "Collaborateurs": o([
        { json: "collaborateur", js: "collaborateur", typ: u(a(r("CollaborateurElement")), r("CollaborateurElement")) },
    ], false),
    "CollaborateurElement": o([
        { json: "qualite", js: "qualite", typ: r("Civ") },
        { json: "prenom", js: "prenom", typ: "" },
        { json: "nom", js: "nom", typ: "" },
        { json: "dateDebut", js: "dateDebut", typ: null },
        { json: "dateFin", js: "dateFin", typ: null },
    ], false),
    "PurpleElection": o([
        { json: "lieu", js: "lieu", typ: r("Lieu") },
        { json: "causeMandat", js: "causeMandat", typ: u(null, "") },
        { json: "refCirconscription", js: "refCirconscription", typ: u(undefined, u(null, "")) },
    ], false),
    "Lieu": o([
        { json: "region", js: "region", typ: u(r("Region"), null) },
        { json: "regionType", js: "regionType", typ: u(r("RegionType"), null) },
        { json: "departement", js: "departement", typ: u(null, "") },
        { json: "numDepartement", js: "numDepartement", typ: u(null, "") },
        { json: "numCirco", js: "numCirco", typ: u(null, "") },
    ], false),
    "InfosQualite": o([
        { json: "codeQualite", js: "codeQualite", typ: u(null, "") },
        { json: "libQualite", js: "libQualite", typ: "" },
        { json: "libQualiteSex", js: "libQualiteSex", typ: u(null, "") },
    ], false),
    "Mandature": o([
        { json: "datePriseFonction", js: "datePriseFonction", typ: u(Date, null) },
        { json: "causeFin", js: "causeFin", typ: u(null, "") },
        { json: "premiereElection", js: "premiereElection", typ: u(null, "") },
        { json: "placeHemicycle", js: "placeHemicycle", typ: u(null, "") },
        { json: "mandatRemplaceRef", js: "mandatRemplaceRef", typ: u(undefined, u(null, "")) },
    ], false),
    "PurpleOrganes": o([
        { json: "organeRef", js: "organeRef", typ: u(a(""), "") },
    ], false),
    "Suppleants": o([
        { json: "suppleant", js: "suppleant", typ: r("Suppleant") },
    ], false),
    "Suppleant": o([
        { json: "dateDebut", js: "dateDebut", typ: Date },
        { json: "dateFin", js: "dateFin", typ: u(Date, null) },
        { json: "suppleantRef", js: "suppleantRef", typ: "" },
    ], false),
    "FluffyMandat": o([
        { json: "@xmlns:xsi", js: "@xmlns:xsi", typ: "" },
        { json: "@xsi:type", js: "@xsi:type", typ: r("MandatXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "acteurRef", js: "acteurRef", typ: "" },
        { json: "legislature", js: "legislature", typ: null },
        { json: "typeOrgane", js: "typeOrgane", typ: r("CodeType") },
        { json: "dateDebut", js: "dateDebut", typ: Date },
        { json: "datePublication", js: "datePublication", typ: null },
        { json: "dateFin", js: "dateFin", typ: u(Date, null) },
        { json: "preseance", js: "preseance", typ: "" },
        { json: "nominPrincipale", js: "nominPrincipale", typ: "" },
        { json: "infosQualite", js: "infosQualite", typ: r("InfosQualite") },
        { json: "organes", js: "organes", typ: r("FluffyOrganes") },
        { json: "suppleants", js: "suppleants", typ: null },
        { json: "chambre", js: "chambre", typ: null },
        { json: "election", js: "election", typ: r("FluffyElection") },
        { json: "mandature", js: "mandature", typ: r("Mandature") },
        { json: "collaborateurs", js: "collaborateurs", typ: null },
    ], false),
    "FluffyElection": o([
        { json: "lieu", js: "lieu", typ: r("Lieu") },
        { json: "causeMandat", js: "causeMandat", typ: null },
    ], false),
    "FluffyOrganes": o([
        { json: "organeRef", js: "organeRef", typ: "" },
    ], false),
    "Profession": o([
        { json: "libelleCourant", js: "libelleCourant", typ: u(r("UriHatvpClass"), null, "") },
        { json: "socProcINSEE", js: "socProcINSEE", typ: r("SocProcInsee") },
    ], false),
    "SocProcInsee": o([
        { json: "catSocPro", js: "catSocPro", typ: u(r("UriHatvpClass"), null, "") },
        { json: "famSocPro", js: "famSocPro", typ: u(r("UriHatvpClass"), r("FamSocProEnum"), null) },
    ], false),
    "PurpleUid": o([
        { json: "@xmlns:xsi", js: "@xmlns:xsi", typ: u(undefined, "") },
        { json: "@xsi:type", js: "@xsi:type", typ: r("UidXsiType") },
        { json: "#text", js: "#text", typ: "" },
    ], false),
    "Organe": o([
        { json: "organe", js: "organe", typ: r("OrganeOrgane") },
    ], false),
    "OrganeOrgane": o([
        { json: "@xmlns", js: "@xmlns", typ: u(undefined, "") },
        { json: "@xmlns:xsi", js: "@xmlns:xsi", typ: "" },
        { json: "@xsi:type", js: "@xsi:type", typ: u(undefined, r("OrganeXsiType")) },
        { json: "uid", js: "uid", typ: "" },
        { json: "codeType", js: "codeType", typ: r("CodeType") },
        { json: "libelle", js: "libelle", typ: "" },
        { json: "libelleEdition", js: "libelleEdition", typ: u(null, "") },
        { json: "libelleAbrege", js: "libelleAbrege", typ: "" },
        { json: "libelleAbrev", js: "libelleAbrev", typ: "" },
        { json: "viMoDe", js: "viMoDe", typ: r("ViMoDe") },
        { json: "organeParent", js: "organeParent", typ: u(r("OrganeParent"), null) },
        { json: "chambre", js: "chambre", typ: u(undefined, null) },
        { json: "regime", js: "regime", typ: u(undefined, u(r("Regime"), null)) },
        { json: "legislature", js: "legislature", typ: u(undefined, u(null, "")) },
        { json: "regimeJuridique", js: "regimeJuridique", typ: u(undefined, u(null, "")) },
        { json: "siteInternet", js: "siteInternet", typ: u(undefined, u(null, "")) },
        { json: "nombreReunionsAnnuelles", js: "nombreReunionsAnnuelles", typ: u(undefined, u(null, "")) },
        { json: "secretariat", js: "secretariat", typ: u(undefined, r("Secretariat")) },
        { json: "positionPolitique", js: "positionPolitique", typ: u(undefined, u(r("PositionPolitique"), null)) },
        { json: "preseance", js: "preseance", typ: u(undefined, u(null, "")) },
        { json: "couleurAssociee", js: "couleurAssociee", typ: u(undefined, u(null, "")) },
        { json: "listePays", js: "listePays", typ: u(undefined, u(r("ListePays"), null)) },
    ], false),
    "ListePays": o([
        { json: "paysRef", js: "paysRef", typ: "" },
    ], false),
    "Secretariat": o([
        { json: "secretaire01", js: "secretaire01", typ: u(null, "") },
        { json: "secretaire02", js: "secretaire02", typ: u(r("Secretaire02"), null) },
    ], false),
    "ViMoDe": o([
        { json: "dateDebut", js: "dateDebut", typ: u(Date, null) },
        { json: "dateAgrement", js: "dateAgrement", typ: u(Date, null) },
        { json: "dateFin", js: "dateFin", typ: u(Date, null) },
    ], false),
    "Mandat": o([
        { json: "mandat", js: "mandat", typ: r("PurpleMandat") },
    ], false),
    "ActeursEtOrganes": o([
        { json: "export", js: "export", typ: r("Export") },
    ], false),
    "Export": o([
        { json: "@xmlns:xsi", js: "@xmlns:xsi", typ: "" },
        { json: "organes", js: "organes", typ: r("ExportOrganes") },
        { json: "acteurs", js: "acteurs", typ: r("Acteurs") },
    ], false),
    "Acteurs": o([
        { json: "acteur", js: "acteur", typ: a(r("ActeurElement")) },
    ], false),
    "ActeurElement": o([
        { json: "uid", js: "uid", typ: r("FluffyUid") },
        { json: "etatCivil", js: "etatCivil", typ: r("FluffyEtatCivil") },
        { json: "uri_hatvp", js: "uri_hatvp", typ: u(undefined, u(null, "")) },
        { json: "mandats", js: "mandats", typ: r("FluffyMandats") },
    ], false),
    "FluffyEtatCivil": o([
        { json: "ident", js: "ident", typ: r("FluffyIdent") },
        { json: "infoNaissance", js: "infoNaissance", typ: r("FluffyInfoNaissance") },
        { json: "dateDeces", js: "dateDeces", typ: u(Date, null) },
    ], false),
    "FluffyIdent": o([
        { json: "civ", js: "civ", typ: r("Civ") },
        { json: "prenom", js: "prenom", typ: "" },
        { json: "nom", js: "nom", typ: "" },
        { json: "alpha", js: "alpha", typ: "" },
        { json: "trigramme", js: "trigramme", typ: u(null, "") },
    ], false),
    "FluffyInfoNaissance": o([
        { json: "dateNais", js: "dateNais", typ: Date },
        { json: "villeNais", js: "villeNais", typ: u(null, "") },
        { json: "depNais", js: "depNais", typ: u(null, "") },
        { json: "paysNais", js: "paysNais", typ: u(r("PaysNaisEnum"), null) },
    ], false),
    "FluffyMandats": o([
        { json: "mandat", js: "mandat", typ: u(a(r("TentacledMandat")), r("StickyMandat")) },
    ], false),
    "TentacledMandat": o([
        { json: "@xsi:type", js: "@xsi:type", typ: r("MandatXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "acteurRef", js: "acteurRef", typ: "" },
        { json: "legislature", js: "legislature", typ: u(null, "") },
        { json: "typeOrgane", js: "typeOrgane", typ: r("CodeType") },
        { json: "dateDebut", js: "dateDebut", typ: Date },
        { json: "datePublication", js: "datePublication", typ: u(Date, null) },
        { json: "dateFin", js: "dateFin", typ: u(Date, null) },
        { json: "preseance", js: "preseance", typ: u(null, "") },
        { json: "nominPrincipale", js: "nominPrincipale", typ: "" },
        { json: "infosQualite", js: "infosQualite", typ: r("InfosQualite") },
        { json: "organes", js: "organes", typ: r("PurpleOrganes") },
        { json: "suppleants", js: "suppleants", typ: u(undefined, u(r("Suppleants"), null)) },
        { json: "election", js: "election", typ: u(undefined, r("PurpleElection")) },
        { json: "InfosHorsSIAN", js: "InfosHorsSIAN", typ: u(undefined, r("InfosHorsSian")) },
        { json: "mandature", js: "mandature", typ: u(undefined, r("Mandature")) },
        { json: "libelle", js: "libelle", typ: u(undefined, u(null, "")) },
        { json: "missionPrecedenteRef", js: "missionPrecedenteRef", typ: u(undefined, u(null, "")) },
        { json: "missionSuivanteRef", js: "missionSuivanteRef", typ: u(undefined, u(null, "")) },
    ], false),
    "InfosHorsSian": o([
        { json: "HATVP_URI", js: "HATVP_URI", typ: null },
    ], false),
    "StickyMandat": o([
        { json: "@xsi:type", js: "@xsi:type", typ: r("MandatXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "acteurRef", js: "acteurRef", typ: "" },
        { json: "legislature", js: "legislature", typ: null },
        { json: "typeOrgane", js: "typeOrgane", typ: r("CodeType") },
        { json: "dateDebut", js: "dateDebut", typ: Date },
        { json: "datePublication", js: "datePublication", typ: null },
        { json: "dateFin", js: "dateFin", typ: Date },
        { json: "preseance", js: "preseance", typ: "" },
        { json: "nominPrincipale", js: "nominPrincipale", typ: "" },
        { json: "infosQualite", js: "infosQualite", typ: r("InfosQualite") },
        { json: "organes", js: "organes", typ: r("FluffyOrganes") },
        { json: "suppleants", js: "suppleants", typ: null },
        { json: "election", js: "election", typ: r("FluffyElection") },
        { json: "InfosHorsSIAN", js: "InfosHorsSIAN", typ: r("InfosHorsSian") },
        { json: "mandature", js: "mandature", typ: r("Mandature") },
    ], false),
    "FluffyUid": o([
        { json: "@xsi:type", js: "@xsi:type", typ: r("UidXsiType") },
        { json: "#text", js: "#text", typ: "" },
    ], false),
    "ExportOrganes": o([
        { json: "organe", js: "organe", typ: a(r("OrganeElement")) },
    ], false),
    "OrganeElement": o([
        { json: "@xsi:type", js: "@xsi:type", typ: r("OrganeXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "codeType", js: "codeType", typ: r("CodeType") },
        { json: "libelle", js: "libelle", typ: "" },
        { json: "libelleEdition", js: "libelleEdition", typ: u(null, "") },
        { json: "libelleAbrege", js: "libelleAbrege", typ: "" },
        { json: "libelleAbrev", js: "libelleAbrev", typ: "" },
        { json: "viMoDe", js: "viMoDe", typ: r("ViMoDe") },
        { json: "regimeJuridique", js: "regimeJuridique", typ: u(undefined, u(null, "")) },
        { json: "regime", js: "regime", typ: u(undefined, u(r("Regime"), null)) },
        { json: "legislature", js: "legislature", typ: u(undefined, u(null, "")) },
        { json: "secretariat", js: "secretariat", typ: u(undefined, r("Secretariat")) },
        { json: "positionPolitique", js: "positionPolitique", typ: u(undefined, u(r("PositionPolitique"), null)) },
    ], false),
    "Civ": [
        "M.",
        "Mme",
    ],
    "PaysNaisEnum": [
        "Algérie",
        "Allemagne",
        "Belgique",
        "Brésil",
        "Burkina Faso",
        "Cambodge",
        "Canada",
        "Centrafrique",
        "Chili",
        "Corée du Sud",
        "Cuba",
        "Côte d'Ivoire",
        "Espagne",
        "Etats-Unis d'Amérique",
        "FR",
        "France",
        "Gabon",
        "Grande Bretagne",
        "Grèce",
        "Iran",
        "Liban",
        "Madagascar",
        "Maroc",
        "Maryland (USA)",
        "ALLEMAGNE",
        "FRANCE",
        "Polynésie Francaise",
        "Polynésie française",
        "france",
        "Rwanda",
        "Saint-Martin",
        "Senegal",
        "SERBIE",
        "Seychelles",
        "Suisse",
        "Sénégal",
        "Thaïlande",
        "Togo",
        "Tunisie",
        "Turquie",
        "Ukraine",
        "Vietnam",
    ],
    "MandatXsiType": [
        "MandatAvecSuppleant_Type",
        "MandatMission_Type",
        "MandatParlementaire_type",
        "MandatSimple_Type",
    ],
    "Region": [
        "Auvergne-Rhône-Alpes",
        "Bourgogne-Franche-Comté",
        "Bretagne",
        "Centre-Val de Loire",
        "Corse",
        "Français établis hors de France",
        "Grand Est",
        "Guadeloupe",
        "Guyane",
        "Hauts-de-France",
        "Ile-de-France",
        "Martinique",
        "Mayotte",
        "Normandie",
        "Nouvelle-Aquitaine",
        "Nouvelle-Calédonie",
        "Occitanie",
        "Pays de la Loire",
        "Polynésie française",
        "Provence-Alpes-Côte d'Azur",
        "Réunion",
        "Saint-Barthélemy et Saint-Martin",
        "Saint-Pierre-et-Miquelon",
        "Wallis-et-Futuna",
    ],
    "RegionType": [
        "Collectivités d'outre-mer et Nouvelle-Calédonie",
        "Dom",
        "Français établis hors de France",
        "Métropolitain",
    ],
    "CodeType": [
        "API",
        "ASSEMBLEE",
        "CJR",
        "CMP",
        "CNPE",
        "CNPS",
        "COMNL",
        "COMPER",
        "COMSENAT",
        "COMSPSENAT",
        "CONFPT",
        "CONSTITU",
        "DELEG",
        "DELEGBUREAU",
        "DELEGSENAT",
        "GA",
        "GE",
        "GEVI",
        "GOUVERNEMENT",
        "GP",
        "GROUPESENAT",
        "HCJ",
        "MINISTERE",
        "MISINFO",
        "MISINFOCOM",
        "MISINFOPRE",
        "OFFPAR",
        "ORGAINT",
        "ORGEXTPARL",
        "PARPOL",
        "PRESREP",
        "SENAT",
    ],
    "FamSocProEnum": [
        "Agriculteurs exploitants",
        "Artisans, commerçants et chefs d'entreprise",
        "Autres personnes sans activité professionnelle",
        "Cadres et professions intellectuelles supérieures",
        "Employés",
        "Ouvriers",
        "Professions Intermédiaires",
        "Retraités",
        "Sans profession déclarée",
    ],
    "UidXsiType": [
        "IdActeur_type",
    ],
    "OrganeXsiType": [
        "GroupePolitique_type",
        "OrganeExterne_Type",
        "OrganeExtraParlementaire_type",
        "OrganeParlementaireInternational",
        "OrganeParlementaire_Type",
    ],
    "OrganeParent": [
        "PO268150",
        "PO300736",
        "PO312969",
        "PO382939",
        "PO384206",
        "PO419604",
        "PO419610",
        "PO419865",
        "PO420120",
        "PO425695",
        "PO58828",
        "PO58829",
        "PO58830",
        "PO58831",
        "PO590011",
        "PO59046",
        "PO59047",
        "PO59048",
        "PO59051",
        "PO653199",
        "PO700706",
        "PO702883",
        "PO715793",
        "PO717173",
        "PO725688",
    ],
    "PositionPolitique": [
        "Majoritaire",
        "Minoritaire",
        "Opposition",
    ],
    "Regime": [
        "Assemblée consultative provisoire",
        "Assemblées nationales constituantes",
        "4ème République",
        "5ème République",
    ],
    "Secretaire02": [
        "M. Frédéric Taillet",
        "Mme Anne Rocchesani",
        "Mme Aude Ménoret",
        "Mme Marie Wencker",
    ],
};
