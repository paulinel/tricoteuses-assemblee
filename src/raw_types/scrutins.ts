// To parse this data:
//
//   import { Convert, Scrutins, ScrutinWrapper } from "./file";
//
//   const scrutins = Convert.toScrutins(json);
//   const scrutinWrapper = Convert.toScrutinWrapper(json);
//
// These functions will throw an error if the JSON doesn't
// match the expected interface, even if the JSON is valid.

export interface Scrutins {
    scrutins: ScrutinsClass;
}

export interface ScrutinsClass {
    scrutin: Scrutin[];
}

export interface ScrutinWrapper {
  scrutin: Scrutin
}

export interface Scrutin {
    "@xmlns:xsi":            string;
    uid:                     string;
    numero:                  string;
    organeRef:               string;
    legislature:             string;
    sessionRef:              string;
    seanceRef:               string;
    dateScrutin:             Date;
    quantiemeJourSeance:     string;
    typeVote:                TypeVote;
    sort:                    Sort;
    titre:                   string;
    demandeur:               Demandeur;
    objet:                   Objet;
    modePublicationDesVotes: ModePublicationDesVotes;
    syntheseVote:            SyntheseVote;
    ventilationVotes:        VentilationVotes;
    miseAuPoint:             MiseAuPoint | null;
    "@xmlns"?:               string;
}

export interface Demandeur {
    texte:                null | string;
    referenceLegislative: null;
}

export interface MiseAuPoint {
    nonVotants:            Array<Contre | null> | null;
    pour?:                 ContreClass | null;
    contre?:               ContreClass | null;
    abstentions:           Array<ContreClass | null> | ContreClass | null;
    nonVotantsVolontaires: Array<ContreClass | null> | NonVotantsVolontairesClass | null;
    pours?:                ContreClass | null;
    contres?:              ContreClass | null;
    dysfonctionnement?:    Dysfonctionnement;
}

export interface ContreClass {
    votant: NonVotantVotant[] | NonVotantVotant;
}

export interface NonVotantVotant {
    acteurRef: string;
    mandatRef: string;
}

export interface Dysfonctionnement {
    nonVotants:            null;
    pour:                  Contre | null;
    contre:                Contre | null;
    abstentions:           null;
    nonVotantsVolontaires: null;
}

export interface Contre {
    votant: NonVotantVotant;
}

export interface NonVotantsVolontairesClass {
    votant: NonVotantVotant[];
}

export enum ModePublicationDesVotes {
    DecompteDissidentsPositionGroupe = "DecompteDissidentsPositionGroupe",
    DecompteNominatif = "DecompteNominatif",
}

export interface Objet {
    libelle:              string;
    referenceLegislative: null;
}

export interface Sort {
    code:    Code;
    libelle: Libelle;
}

export enum Code {
    Adopté = "adopté",
    Rejeté = "rejeté",
}

export enum Libelle {
    LAssembléeNationaleAAdopté = "l'Assemblée nationale a adopté",
    LAssembléeNationaleNAPasAdopté = "L'Assemblée nationale n'a pas adopté",
}

export interface SyntheseVote {
    nombreVotants:      string;
    suffragesExprimes:  string;
    nbrSuffragesRequis: string;
    annonce:            Libelle;
    decompte:           Decompte;
}

export interface Decompte {
    pour:                   string;
    contre:                 string;
    abstentions:            string;
    nonVotants:             string;
    nonVotantsVolontaires?: string;
}

export interface TypeVote {
    codeTypeVote:    CodeTypeVote;
    libelleTypeVote: LibelleTypeVote;
    typeMajorite:    TypeMajorite;
}

export enum CodeTypeVote {
    Moc = "MOC",
    Sat = "SAT",
    Spo = "SPO",
    Sps = "SPS",
}

export enum LibelleTypeVote {
    MotionDeCensure = "motion de censure",
    ScrutinPublicOrdinaire = "scrutin public ordinaire",
    ScrutinPublicSolennel = "scrutin public solennel",
    ScrutinÀLaTribune = "scrutin à la tribune",
}

export enum TypeMajorite {
    MajoritéAbsolueDesSuffragesExprimés = "majorité absolue des suffrages exprimés",
    MajoritéDesMembresComposantLAssembléeNationale = "majorité des membres composant l'Assemblée nationale",
    MajoritéDesMembresComposantsComposantLAssemblée = "majorité des membres composants composant l'Assemblée",
    MajoritéDesMembresComposantsComposantLAssembléeNationale = "majorité des membres composants composant l'Assemblée nationale",
}

export interface VentilationVotes {
    organe: Organe;
}

export interface Organe {
    organeRef: string;
    groupes:   Groupes;
}

export interface Groupes {
    groupe: Groupe[];
}

export interface Groupe {
    organeRef:           string;
    nombreMembresGroupe: string;
    vote:                Vote;
}

export interface Vote {
    positionMajoritaire: PositionMajoritaire;
    decompteVoix:        DecompteVoix;
    decompteNominatif:   DecompteNominatif;
}

export interface DecompteNominatif {
    nonVotants:  NonVotantsClass | null | string;
    pour?:       ContreClass | null;
    contre?:     ContreClass | null;
    abstentions: DecompteNominatifAbstentions | null;
    pours?:      DecompteNominatifAbstentions | null;
    contres?:    DecompteNominatifAbstentions | null;
}

export interface DecompteNominatifAbstentions {
    votant: PurpleVotant[] | PurpleVotant;
}

export interface PurpleVotant {
    acteurRef:      string;
    mandatRef:      string;
    parDelegation?: string;
}

export interface NonVotantsClass {
    votant: FluffyVotant[] | FluffyVotant;
}

export interface FluffyVotant {
    acteurRef:         string;
    mandatRef:         string;
    causePositionVote: CausePositionVote;
}

export enum CausePositionVote {
    Mg = "MG",
    Pan = "PAN",
    Pse = "PSE",
}

export interface DecompteVoix {
    pour:                   string;
    contre:                 string;
    abstention?:            string;
    nonVotant?:             string;
    nonVotants?:            string;
    abstentions?:           string;
    nonVotantsVolontaires?: string;
}

export enum PositionMajoritaire {
    Abstention = "abstention",
    Contre = "contre",
    Pour = "pour",
}

// Converts JSON strings to/from your types
// and asserts the results of JSON.parse at runtime
export class Convert {
    public static toScrutins(json: string): Scrutins {
        return cast(JSON.parse(json), r("Scrutins"));
    }

    public static scrutinsToJson(value: Scrutins): string {
        return JSON.stringify(uncast(value, r("Scrutins")), null, 2);
    }

    public static toScrutinWrapper(json: string): ScrutinWrapper {
        return cast(JSON.parse(json), r("ScrutinWrapper"));
    }

    public static scrutinWrapperToJson(value: ScrutinWrapper): string {
        return JSON.stringify(uncast(value, r("ScrutinWrapper")), null, 2);
    }
}

function invalidValue(typ: any, val: any): never {
    throw Error(`Invalid value ${JSON.stringify(val)} for type ${JSON.stringify(typ)}`);
}

function jsonToJSProps(typ: any): any {
    if (typ.jsonToJS === undefined) {
        var map: any = {};
        typ.props.forEach((p: any) => map[p.json] = { key: p.js, typ: p.typ });
        typ.jsonToJS = map;
    }
    return typ.jsonToJS;
}

function jsToJSONProps(typ: any): any {
    if (typ.jsToJSON === undefined) {
        var map: any = {};
        typ.props.forEach((p: any) => map[p.js] = { key: p.json, typ: p.typ });
        typ.jsToJSON = map;
    }
    return typ.jsToJSON;
}

function transform(val: any, typ: any, getProps: any): any {
    function transformPrimitive(typ: string, val: any): any {
        if (typeof typ === typeof val) return val;
        return invalidValue(typ, val);
    }

    function transformUnion(typs: any[], val: any): any {
        // val must validate against one typ in typs
        var l = typs.length;
        for (var i = 0; i < l; i++) {
            var typ = typs[i];
            try {
                return transform(val, typ, getProps);
            } catch (_) {}
        }
        return invalidValue(typs, val);
    }

    function transformEnum(cases: string[], val: any): any {
        if (cases.indexOf(val) !== -1) return val;
        return invalidValue(cases, val);
    }

    function transformArray(typ: any, val: any): any {
        // val must be an array with no invalid elements
        if (!Array.isArray(val)) return invalidValue("array", val);
        return val.map(el => transform(el, typ, getProps));
    }

    function transformDate(_typ: any, val: any): any {
        if (val === null) {
            return null;
        }
        const d = new Date(val);
        if (isNaN(d.valueOf())) {
            return invalidValue("Date", val);
        }
        return d;
    }

    function transformObject(props: { [k: string]: any }, additional: any, val: any): any {
        if (val === null || typeof val !== "object" || Array.isArray(val)) {
            return invalidValue("object", val);
        }
        var result: any = {};
        Object.getOwnPropertyNames(props).forEach(key => {
            const prop = props[key];
            const v = Object.prototype.hasOwnProperty.call(val, key) ? val[key] : undefined;
            result[prop.key] = transform(v, prop.typ, getProps);
        });
        Object.getOwnPropertyNames(val).forEach(key => {
            if (!Object.prototype.hasOwnProperty.call(props, key)) {
                result[key] = transform(val[key], additional, getProps);
            }
        });
        return result;
    }

    if (typ === "any") return val;
    if (typ === null) {
        if (val === null) return val;
        return invalidValue(typ, val);
    }
    if (typ === false) return invalidValue(typ, val);
    while (typeof typ === "object" && typ.ref !== undefined) {
        typ = typeMap[typ.ref];
    }
    if (Array.isArray(typ)) return transformEnum(typ, val);
    if (typeof typ === "object") {
        return typ.hasOwnProperty("unionMembers") ? transformUnion(typ.unionMembers, val)
            : typ.hasOwnProperty("arrayItems")    ? transformArray(typ.arrayItems, val)
            : typ.hasOwnProperty("props")         ? transformObject(getProps(typ), typ.additional, val)
            : invalidValue(typ, val);
    }
    // Numbers can be parsed by Date but shouldn't be.
    if (typ === Date && typeof val !== "number") return transformDate(typ, val);
    return transformPrimitive(typ, val);
}

function cast<T>(val: any, typ: any): T {
    return transform(val, typ, jsonToJSProps);
}

function uncast<T>(val: T, typ: any): any {
    return transform(val, typ, jsToJSONProps);
}

function a(typ: any) {
    return { arrayItems: typ };
}

function u(...typs: any[]) {
    return { unionMembers: typs };
}

function o(props: any[], additional: any) {
    return { props, additional };
}

// function m(additional: any) {
//     return { props: [], additional };
// }

function r(name: string) {
    return { ref: name };
}

const typeMap: any = {
    "Scrutins": o([
        { json: "scrutins", js: "scrutins", typ: r("ScrutinsClass") },
    ], false),
    "ScrutinsClass": o([
        { json: "scrutin", js: "scrutin", typ: a(r("Scrutin")) },
    ], false),
    "ScrutinWrapper": o([
        { json: "scrutin", js: "scrutin", typ: r("Scrutin") },
    ], false),
    "Scrutin": o([
        { json: "@xmlns:xsi", js: "@xmlns:xsi", typ: "" },
        { json: "uid", js: "uid", typ: "" },
        { json: "numero", js: "numero", typ: "" },
        { json: "organeRef", js: "organeRef", typ: "" },
        { json: "legislature", js: "legislature", typ: "" },
        { json: "sessionRef", js: "sessionRef", typ: "" },
        { json: "seanceRef", js: "seanceRef", typ: "" },
        { json: "dateScrutin", js: "dateScrutin", typ: Date },
        { json: "quantiemeJourSeance", js: "quantiemeJourSeance", typ: "" },
        { json: "typeVote", js: "typeVote", typ: r("TypeVote") },
        { json: "sort", js: "sort", typ: r("Sort") },
        { json: "titre", js: "titre", typ: "" },
        { json: "demandeur", js: "demandeur", typ: r("Demandeur") },
        { json: "objet", js: "objet", typ: r("Objet") },
        { json: "modePublicationDesVotes", js: "modePublicationDesVotes", typ: r("ModePublicationDesVotes") },
        { json: "syntheseVote", js: "syntheseVote", typ: r("SyntheseVote") },
        { json: "ventilationVotes", js: "ventilationVotes", typ: r("VentilationVotes") },
        { json: "miseAuPoint", js: "miseAuPoint", typ: u(r("MiseAuPoint"), null) },
        { json: "@xmlns", js: "@xmlns", typ: u(undefined, "") },
    ], false),
    "Demandeur": o([
        { json: "texte", js: "texte", typ: u(null, "") },
        { json: "referenceLegislative", js: "referenceLegislative", typ: null },
    ], false),
    "MiseAuPoint": o([
        { json: "nonVotants", js: "nonVotants", typ: u(a(u(r("Contre"), null)), null) },
        { json: "pour", js: "pour", typ: u(undefined, u(r("ContreClass"), null)) },
        { json: "contre", js: "contre", typ: u(undefined, u(r("ContreClass"), null)) },
        { json: "abstentions", js: "abstentions", typ: u(a(u(r("ContreClass"), null)), r("ContreClass"), null) },
        { json: "nonVotantsVolontaires", js: "nonVotantsVolontaires", typ: u(a(u(r("ContreClass"), null)), r("NonVotantsVolontairesClass"), null) },
        { json: "pours", js: "pours", typ: u(undefined, u(r("ContreClass"), null)) },
        { json: "contres", js: "contres", typ: u(undefined, u(r("ContreClass"), null)) },
        { json: "dysfonctionnement", js: "dysfonctionnement", typ: u(undefined, r("Dysfonctionnement")) },
    ], false),
    "ContreClass": o([
        { json: "votant", js: "votant", typ: u(a(r("NonVotantVotant")), r("NonVotantVotant")) },
    ], false),
    "NonVotantVotant": o([
        { json: "acteurRef", js: "acteurRef", typ: "" },
        { json: "mandatRef", js: "mandatRef", typ: "" },
    ], false),
    "Dysfonctionnement": o([
        { json: "nonVotants", js: "nonVotants", typ: null },
        { json: "pour", js: "pour", typ: u(r("Contre"), null) },
        { json: "contre", js: "contre", typ: u(r("Contre"), null) },
        { json: "abstentions", js: "abstentions", typ: null },
        { json: "nonVotantsVolontaires", js: "nonVotantsVolontaires", typ: null },
    ], false),
    "Contre": o([
        { json: "votant", js: "votant", typ: r("NonVotantVotant") },
    ], false),
    "NonVotantsVolontairesClass": o([
        { json: "votant", js: "votant", typ: a(r("NonVotantVotant")) },
    ], false),
    "Objet": o([
        { json: "libelle", js: "libelle", typ: "" },
        { json: "referenceLegislative", js: "referenceLegislative", typ: null },
    ], false),
    "Sort": o([
        { json: "code", js: "code", typ: r("Code") },
        { json: "libelle", js: "libelle", typ: r("Libelle") },
    ], false),
    "SyntheseVote": o([
        { json: "nombreVotants", js: "nombreVotants", typ: "" },
        { json: "suffragesExprimes", js: "suffragesExprimes", typ: "" },
        { json: "nbrSuffragesRequis", js: "nbrSuffragesRequis", typ: "" },
        { json: "annonce", js: "annonce", typ: r("Libelle") },
        { json: "decompte", js: "decompte", typ: r("Decompte") },
    ], false),
    "Decompte": o([
        { json: "pour", js: "pour", typ: "" },
        { json: "contre", js: "contre", typ: "" },
        { json: "abstentions", js: "abstentions", typ: "" },
        { json: "nonVotants", js: "nonVotants", typ: "" },
        { json: "nonVotantsVolontaires", js: "nonVotantsVolontaires", typ: u(undefined, "") },
    ], false),
    "TypeVote": o([
        { json: "codeTypeVote", js: "codeTypeVote", typ: r("CodeTypeVote") },
        { json: "libelleTypeVote", js: "libelleTypeVote", typ: r("LibelleTypeVote") },
        { json: "typeMajorite", js: "typeMajorite", typ: r("TypeMajorite") },
    ], false),
    "VentilationVotes": o([
        { json: "organe", js: "organe", typ: r("Organe") },
    ], false),
    "Organe": o([
        { json: "organeRef", js: "organeRef", typ: "" },
        { json: "groupes", js: "groupes", typ: r("Groupes") },
    ], false),
    "Groupes": o([
        { json: "groupe", js: "groupe", typ: a(r("Groupe")) },
    ], false),
    "Groupe": o([
        { json: "organeRef", js: "organeRef", typ: "" },
        { json: "nombreMembresGroupe", js: "nombreMembresGroupe", typ: "" },
        { json: "vote", js: "vote", typ: r("Vote") },
    ], false),
    "Vote": o([
        { json: "positionMajoritaire", js: "positionMajoritaire", typ: r("PositionMajoritaire") },
        { json: "decompteVoix", js: "decompteVoix", typ: r("DecompteVoix") },
        { json: "decompteNominatif", js: "decompteNominatif", typ: r("DecompteNominatif") },
    ], false),
    "DecompteNominatif": o([
        { json: "nonVotants", js: "nonVotants", typ: u(r("NonVotantsClass"), null, "") },
        { json: "pour", js: "pour", typ: u(undefined, u(r("ContreClass"), null)) },
        { json: "contre", js: "contre", typ: u(undefined, u(r("ContreClass"), null)) },
        { json: "abstentions", js: "abstentions", typ: u(r("DecompteNominatifAbstentions"), null) },
        { json: "pours", js: "pours", typ: u(undefined, u(r("DecompteNominatifAbstentions"), null)) },
        { json: "contres", js: "contres", typ: u(undefined, u(r("DecompteNominatifAbstentions"), null)) },
    ], false),
    "DecompteNominatifAbstentions": o([
        { json: "votant", js: "votant", typ: u(a(r("PurpleVotant")), r("PurpleVotant")) },
    ], false),
    "PurpleVotant": o([
        { json: "acteurRef", js: "acteurRef", typ: "" },
        { json: "mandatRef", js: "mandatRef", typ: "" },
        { json: "parDelegation", js: "parDelegation", typ: u(undefined, "") },
    ], false),
    "NonVotantsClass": o([
        { json: "votant", js: "votant", typ: u(a(r("FluffyVotant")), r("FluffyVotant")) },
    ], false),
    "FluffyVotant": o([
        { json: "acteurRef", js: "acteurRef", typ: "" },
        { json: "mandatRef", js: "mandatRef", typ: "" },
        { json: "causePositionVote", js: "causePositionVote", typ: r("CausePositionVote") },
    ], false),
    "DecompteVoix": o([
        { json: "pour", js: "pour", typ: "" },
        { json: "contre", js: "contre", typ: "" },
        { json: "abstention", js: "abstention", typ: u(undefined, "") },
        { json: "nonVotant", js: "nonVotant", typ: u(undefined, "") },
        { json: "nonVotants", js: "nonVotants", typ: u(undefined, "") },
        { json: "abstentions", js: "abstentions", typ: u(undefined, "") },
        { json: "nonVotantsVolontaires", js: "nonVotantsVolontaires", typ: u(undefined, "") },
    ], false),
    "ModePublicationDesVotes": [
        "DecompteDissidentsPositionGroupe",
        "DecompteNominatif",
    ],
    "Code": [
        "adopté",
        "rejeté",
    ],
    "Libelle": [
        "l'Assemblée nationale a adopté",
        "L'Assemblée nationale n'a pas adopté",
    ],
    "CodeTypeVote": [
        "MOC",
        "SAT",
        "SPO",
        "SPS",
    ],
    "LibelleTypeVote": [
        "motion de censure",
        "scrutin public ordinaire",
        "scrutin public solennel",
        "scrutin à la tribune",
    ],
    "TypeMajorite": [
        "majorité absolue des suffrages exprimés",
        "majorité des membres composant l'Assemblée nationale",
        "majorité des membres composants composant l'Assemblée",
        "majorité des membres composants composant l'Assemblée nationale",
    ],
    "CausePositionVote": [
        "MG",
        "PAN",
        "PSE",
    ],
    "PositionMajoritaire": [
        "abstention",
        "contre",
        "pour",
    ],
};
