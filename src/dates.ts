import parseISO from "date-fns/parseISO"

// Patch Date.prototype.toJSON to ensure that dates keep their timezone when converted to strings.
// https://stackoverflow.com/questions/31096130/how-to-json-stringify-a-javascript-date-and-preserve-timezone
export const existingDateToJson = Date.prototype.toJSON
export const patchedDateToJson = function(this: Date): string {
  try {
    const timezoneOffsetInHours = -(this.getTimezoneOffset() / 60) //UTC minus local time
    const sign = timezoneOffsetInHours >= 0 ? "+" : "-"
    const leadingZero = Math.abs(timezoneOffsetInHours) < 10 ? "0" : ""

    // It's a bit unfortunate that we need to construct a new Date instance
    // (we don't want _this_ Date instance to be modified).
    const correctedDate = new Date(
      this.getFullYear(),
      this.getMonth(),
      this.getDate(),
      this.getHours(),
      this.getMinutes(),
      this.getSeconds(),
      this.getMilliseconds(),
    )
    correctedDate.setHours(this.getHours() + timezoneOffsetInHours)
    const iso = correctedDate.toISOString().replace("Z", "")
    return (
      iso +
      sign +
      leadingZero +
      Math.abs(timezoneOffsetInHours)
        .toString()
        .split(".")[0] +
      ":00"
    )
  } catch (e) {
    Date.prototype.toJSON = existingDateToJson
    const jsonDate = this.toJSON()
    Date.prototype.toJSON = patchedDateToJson
    return jsonDate
  }
}

export const frenchDateFormat = new Intl.DateTimeFormat("fr-FR", {
  day: "numeric",
  month: "long",
  weekday: "long",
  year: "numeric",
})
export const frenchDateSameYearShortFormat = new Intl.DateTimeFormat("fr-FR", {
  day: "numeric",
  month: "short",
})
export const frenchTimeFormat = new Intl.DateTimeFormat("fr-FR", {
  hour: "numeric",
  minute: "2-digit",
})

export function dateFromNullableDateOrIsoString(
  date: Date | string | undefined | null,
  defaultValue: Date | undefined | null,
): Date | undefined | null {
  if (date instanceof Date) {
    return date
  }
  if (typeof date === "string") {
    return parseISO(date)
  }
  return defaultValue
}

export function formatRelativeFrenchTime(date: Date, now: Date) {
  if (
    date.getFullYear() === now.getFullYear() &&
    date.getMonth() === now.getMonth() &&
    date.getDate() === now.getDate()
  ) {
    return frenchTimeFormat.format(date)
  }
  return frenchDateSameYearShortFormat.format(date)
}

export function localIso8601StringFromDate(date: Date) {
  return new Date(date.getTime() - date.getTimezoneOffset() * 60000)
    .toISOString()
    .split("T")[0]
}
