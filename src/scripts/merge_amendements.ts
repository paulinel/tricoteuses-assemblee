import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import path from "path"

import { datasets, DatasetStructure } from "../datasets"
import { walkDir } from "../file_systems"

const optionsDefinitions = [
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: "directory containing Assemblée open data files",
    name: "dataDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

function mergeAmendements(dataDir: string) {
  for (const dataset of datasets.amendements) {
    switch (dataset.structure) {
      case DatasetStructure.SegmentedFiles:
        {
          const originalJsonDir: string = path.join(dataDir, dataset.filename)
          if (!options.silent) {
            console.log(`Merging ${originalJsonDir}`)
          }

          const datasetMergedFilePath = path.join(
            dataDir,
            path.basename(originalJsonDir, ".json") + "_fusionne.json",
          )

          const amendementsOriginalFilesPathsByUidTextLegislatif: {
            [uid: string]: string[]
          } = {}
          for (const amendementSplitPath of walkDir(originalJsonDir)) {
            const amendementFilename = amendementSplitPath[amendementSplitPath.length - 1]
            if (!amendementFilename.endsWith(".json")) {
              continue
            }
            const uidTexteLegislatif = amendementSplitPath[amendementSplitPath.length - 2]
            let amendementsOriginalFilesPaths =
              amendementsOriginalFilesPathsByUidTextLegislatif[uidTexteLegislatif]
            if (amendementsOriginalFilesPaths === undefined) {
              amendementsOriginalFilesPaths = amendementsOriginalFilesPathsByUidTextLegislatif[
                uidTexteLegislatif
              ] = []
            }
            const amendementOriginalFilePath = path.join(
              originalJsonDir,
              ...amendementSplitPath,
            )
            amendementsOriginalFilesPaths.push(amendementOriginalFilePath)
          }

          const uidsTextesLegislatifs = Object.keys(
            amendementsOriginalFilesPathsByUidTextLegislatif,
          )
          uidsTextesLegislatifs.sort((a: string, b: string) =>
            a.length === b.length ? a.localeCompare(b) : a.length - b.length,
          )
          const textesLegislatifs = []
          for (const uidTexteLegislatif of uidsTextesLegislatifs) {
            if (options.verbose) {
              console.log(
                `  Merging "amendements du texte législatif": ${uidTexteLegislatif}…`,
              )
            }
            const amendementsOriginalFilesPaths =
              amendementsOriginalFilesPathsByUidTextLegislatif[uidTexteLegislatif]
            amendementsOriginalFilesPaths.sort((a: string, b: string) =>
              a.length === b.length ? a.localeCompare(b) : a.length - b.length,
            )
            const amendements = []
            for (const amendementOriginalFilePath of amendementsOriginalFilesPaths) {
              const amendementOriginalJson: string = fs.readFileSync(
                amendementOriginalFilePath,
                {
                  encoding: "utf8",
                },
              )
              const amendementOriginal: any = JSON.parse(amendementOriginalJson)
              const amendement = amendementOriginal.amendement
              amendements.push(amendement)
            }
            const texteLegislatif = {
              "@xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
              refTexteLegislatif: uidTexteLegislatif,
              amendements: { amendement: amendements },
            }
            textesLegislatifs.push(texteLegislatif)
          }
          fs.writeFileSync(
            datasetMergedFilePath,
            JSON.stringify(
              { textesEtAmendements: { texteleg: textesLegislatifs } },
              null,
              2,
            ),
            {
              encoding: "utf8",
            },
          )
        }
        break
    }
  }
}

mergeAmendements(options.dataDir)
