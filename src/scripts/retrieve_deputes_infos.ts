import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import fetch from "node-fetch"
import path from "path"

import { EnabledDatasets } from "../datasets"
import { loadAssembleeData } from "../loaders"
import { parsePageDepute } from "../parsers/deputes"
import { CodeTypeOrgane, Mandat, TypeMandat } from "../types/acteurs_et_organes"
import { InfosPageDepute } from "../types/deputes"

const optionsDefinitions = [
  {
    alias: "f",
    help: "fetch députés' informations instead of retrieving them from files",
    name: "fetch",
    type: Boolean,
  },
  {
    alias: "l",
    defaultValue: "15",
    name: "legislature",
    type: String,
  },
  {
    alias: "p",
    help: "parse députés' informations",
    name: "parse",
    type: Boolean,
  },
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "u",
    help: "UID of first député to retrieve",
    name: "uid",
    type: String,
  },
  {
    defaultOption: true,
    help: "directory containing Assemblée open data files",
    name: "dataDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

async function retrieveInfosDeputes(): Promise<{
  [uid: string]: InfosPageDepute
}> {
  const dataDir = options.dataDir
  const { acteurByUid } = loadAssembleeData(
    dataDir,
    EnabledDatasets.ActeursEtOrganes,
    options.legislature,
  )
  const deputesLegislature = Object.values(acteurByUid)
    .filter(acteur => {
      const { mandats } = acteur
      if (!mandats) {
        return false
      }
      const mandatDeputeLegislature = mandats.filter(
        (mandat: Mandat) =>
          mandat.xsiType === TypeMandat.MandatParlementaireType &&
          mandat.legislature === options.legislature &&
          mandat.typeOrgane === CodeTypeOrgane.Assemblee,
      )[0]
      return mandatDeputeLegislature !== undefined
    })
    .sort((a, b) =>
      a.uid.length === b.uid.length
        ? a.uid.localeCompare(b.uid)
        : a.uid.length - b.uid.length,
    )

  const firstUid = options.uid
  const infosDeputeLegislatureByUid: { [uid: string]: InfosPageDepute } = {}
  let skip = !!firstUid
  for (const depute of deputesLegislature) {
    if (skip) {
      if (depute.uid === firstUid) {
        skip = false
      } else {
        continue
      }
    }
    if (!options.silent) {
      console.log(
        "Retrieving informations on:",
        depute.uid,
        depute.etatCivil.ident.prenom,
        depute.etatCivil.ident.nom,
      )
    }
    const pageUrl = `http://www2.assemblee-nationale.fr/deputes/fiche/OMC_${depute.uid}`
    const { hostname, pathname } = new URL(pageUrl)
    const filePath = path.join(dataDir, hostname, ...pathname.split("/"))
    fs.ensureDirSync(path.dirname(filePath))

    let page = undefined
    if (options.fetch) {
      const response = await fetch(pageUrl)
      page = await response.text()
      if (!response.ok) {
        if (response.status !== 404) {
          console.error(
            `Error while getting page "${pageUrl}" (uid: ${
              depute.uid
            }):\n\nError:\n${JSON.stringify(
              { code: response.status, message: response.statusText },
              null,
              2,
            )}`,
          )
        }
        continue
      }
      fs.writeFileSync(filePath, page)
    } else {
      try {
        page = fs.readFileSync(filePath, { encoding: "utf-8" })
      } catch (e) {
        // if (e.code === "ENOENT") {
        //   continue
        // }
        throw e
      }
    }

    if (options.parse) {
      const infosPageDepute = parsePageDepute(pageUrl, page)
      // const { error } = infosPageDepute
      // if (error) {
      //   console.error(
      //     `Error while parsing page "${pageUrl}" (uid: ${depute.uid}):\n\nError:\n${
      //       JSON.stringify(error, null, 2)}`
      //   )
      //   continue
      // }
      infosDeputeLegislatureByUid[depute.uid] = infosPageDepute
    }
  }
  fs.writeFileSync(
    path.join(dataDir, `infos_deputes_${options.legislature}.json`),
    JSON.stringify(infosDeputeLegislatureByUid, null, 2),
  )

  return infosDeputeLegislatureByUid
}

retrieveInfosDeputes().catch(error => {
  console.log(error)
  process.exit(1)
})
