import assert from "assert"

import { cleanBooleanAttribute, cleanXmlArtefacts } from "./xml"

export function cleanDocumentOrDivision(d: any): void {
  cleanXmlArtefacts(d)

  const xsiType = d["@xsi:type"]
  if (xsiType !== undefined) {
    d.xsiType = xsiType
    delete d["@xsi:type"]
  }

  const cycleDeVie = d.cycleDeVie
  assert(cycleDeVie)
  {
    const chrono = cycleDeVie.chrono
    assert(chrono)
    cleanXmlArtefacts(chrono)
  }

  let divisions = d.divisions
  if (divisions !== undefined) {
    divisions = divisions.division
    if (!Array.isArray(divisions)) {
      assert(divisions)
      divisions = [divisions]
    }
    d.divisions = divisions
    for (const division of divisions) {
      cleanDocumentOrDivision(division)
    }
  }

  assert.strictEqual(d.redacteur, undefined)

  const classification = d.classification
  assert(classification)
  cleanXmlArtefacts(classification)

  let auteurs = d.auteurs
  if (auteurs !== undefined) {
    auteurs = auteurs.auteur
    if (!Array.isArray(auteurs)) {
      assert(auteurs)
      auteurs = [auteurs]
    }
    d.auteurs = auteurs
    for (const auteur of auteurs) {
      let organeRef = auteur.organe
      if (organeRef !== undefined) {
        auteur.organeRef = organeRef.organeRef
        delete auteur.organe
      }
    }
  }

  const notice = d.notice
  {
    cleanBooleanAttribute(notice, "adoptionConforme")
  }

  let coSignataires = d.coSignataires
  if (coSignataires !== undefined) {
    coSignataires = coSignataires.coSignataire
    if (!Array.isArray(coSignataires)) {
      assert(coSignataires)
      coSignataires = [coSignataires]
    }
    d.coSignataires = coSignataires
    for (const coSignataire of coSignataires) {
      cleanXmlArtefacts(coSignataire)

      cleanBooleanAttribute(coSignataire, "edite")

      let acteurRef = coSignataire.acteur
      if (acteurRef !== undefined) {
        acteurRef = acteurRef.acteurRef
        assert(acteurRef)
        delete coSignataire.acteur
        coSignataire.acteurRef = acteurRef
      }

      const organe = coSignataire.organe
      if (organe !== undefined) {
        cleanBooleanAttribute(organe, "etApparentes")
      }
    }
  }

  const depotAmendements = d.depotAmendements
  if (depotAmendements !== undefined) {
    cleanXmlArtefacts(depotAmendements)

    const amendementsSeance = depotAmendements.amendementsSeance
    {
      cleanXmlArtefacts(amendementsSeance)

      cleanBooleanAttribute(amendementsSeance, "amendable")
    }

    let amendementsCommission = depotAmendements.amendementsCommission
    if (amendementsCommission !== undefined) {
      amendementsCommission = amendementsCommission.commission
      if (!Array.isArray(amendementsCommission)) {
        assert(amendementsCommission)
        amendementsCommission = [amendementsCommission]
      }
      depotAmendements.amendementsCommission = amendementsCommission
      for (const commision of amendementsCommission) {
        cleanXmlArtefacts(commision)

        cleanBooleanAttribute(commision, "amendable")

        assert.strictEqual(commision.dateLimiteDepot, undefined)
      }
    }
  }
}
