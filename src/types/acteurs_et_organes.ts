// To parse this data:
//
//   import { Convert, Acteur, Organe, ActeursEtOrganes } from "./file";
//
//   const acteur = Convert.toActeur(json);
//   const organe = Convert.toOrgane(json);
//   const acteursEtOrganes = Convert.toActeursEtOrganes(json);
//
// These functions will throw an error if the JSON doesn't
// match the expected interface, even if the JSON is valid.

export interface ActeursEtOrganes {
  organes: Organe[]
  acteurs: Acteur[]
}

export interface Acteur {
  uid: string
  etatCivil: EtatCivil
  profession?: Profession
  uriHatvp?: string
  adresses?: Adresse[]
  mandats?: Mandat[]
  photo?: Photo
}

export interface EtatCivil {
  ident: Ident
  infoNaissance: InfoNaissance
  dateDeces?: Date
}

export interface Ident {
  civ: Civ
  prenom: string
  nom: string
  alpha: string
  trigramme?: string
}

export enum Civ {
  M = "M.",
  Mme = "Mme",
}

export interface InfoNaissance {
  dateNais: Date
  villeNais?: string
  depNais?: string
  paysNais?: string
}

export type Adresse = AdresseElectronique | AdressePostale

export interface AdresseElectronique {
  xsiType: "AdresseMail_Type" | "AdresseSiteWeb_Type" | "AdresseTelephonique_Type"
  uid: string
  type: string
  typeLibelle: string
  poids?: string
  adresseDeRattachement?: string
  valElec?: string
}

export interface AdressePostale {
  xsiType: "AdressePostale_Type"
  uid: string
  type: string
  typeLibelle: string
  poids?: string
  adresseDeRattachement?: string
  intitule?: string
  numeroRue?: string
  nomRue?: string
  complementAdresse?: string
  codePostal?: string
  ville?: string
}

export interface Mandat {
  xsiType?: TypeMandat
  uid: string
  acteurRef: string
  legislature?: string
  typeOrgane: CodeTypeOrgane
  dateDebut: Date
  datePublication?: Date
  dateFin?: Date
  preseance?: string
  nominPrincipale: string
  infosQualite: InfosQualite
  organesRefs: string[]
  organes?: Organe[] // Added by Tricoteuses
  suppleant?: Suppleant
  election?: Election
  mandature?: Mandature
  collaborateurs?: Collaborateur[]
  libelle?: string
  missionSuivanteRef?: string
  missionPrecedenteRef?: string
}

export enum TypeMandat {
  MandatAvecSuppleantType = "MandatAvecSuppleant_Type",
  MandatMissionType = "MandatMission_Type",
  MandatParlementaireType = "MandatParlementaire_type",
  MandatSimpleType = "MandatSimple_Type",
}

export interface Collaborateur {
  qualite: Civ
  prenom: string
  nom: string
}

export interface Election {
  lieu: Lieu
  causeMandat?: string
  refCirconscription?: string
}

export interface Lieu {
  region?: Region
  regionType?: RegionType
  departement?: string
  numDepartement?: string
  numCirco?: string
}

export enum Region {
  AuvergneRhôneAlpes = "Auvergne-Rhône-Alpes",
  BourgogneFrancheComté = "Bourgogne-Franche-Comté",
  Bretagne = "Bretagne",
  CentreValDeLoire = "Centre-Val de Loire",
  Corse = "Corse",
  FrançaisÉtablisHorsDeFrance = "Français établis hors de France",
  GrandEst = "Grand Est",
  Guadeloupe = "Guadeloupe",
  Guyane = "Guyane",
  HautsDeFrance = "Hauts-de-France",
  IleDeFrance = "Ile-de-France",
  Martinique = "Martinique",
  Mayotte = "Mayotte",
  Normandie = "Normandie",
  NouvelleAquitaine = "Nouvelle-Aquitaine",
  NouvelleCalédonie = "Nouvelle-Calédonie",
  Occitanie = "Occitanie",
  PaysDeLaLoire = "Pays de la Loire",
  PolynésieFrançaise = "Polynésie française",
  ProvenceAlpesCôteDAzur = "Provence-Alpes-Côte d'Azur",
  Réunion = "Réunion",
  SaintBarthélemyEtSaintMartin = "Saint-Barthélemy et Saint-Martin",
  SaintPierreEtMiquelon = "Saint-Pierre-et-Miquelon",
  WallisEtFutuna = "Wallis-et-Futuna",
}

export enum RegionType {
  CollectivitésDOutreMerEtNouvelleCalédonie = "Collectivités d'outre-mer et Nouvelle-Calédonie",
  Dom = "Dom",
  FrançaisÉtablisHorsDeFrance = "Français établis hors de France",
  Métropolitain = "Métropolitain",
}

export interface InfosQualite {
  codeQualite?: string
  libQualite: string
  libQualiteSex?: string
}

export interface Mandature {
  datePriseFonction?: Date
  causeFin?: string
  premiereElection?: string
  placeHemicycle?: string
  mandatRemplaceRef?: string
}

export interface Suppleant {
  dateDebut: Date
  dateFin?: Date
  suppleantRef: string
}

export enum CodeTypeOrgane {
  Api = "API", // Assemblée parlementaire internationale
  Assemblee = "ASSEMBLEE", // Assemblée nationale
  Cjr = "CJR", // Cour de justice de la République
  Cmp = "CMP", // Commission mixte paritaire,
  Cnpe = "CNPE", // Commission d'enquête; what does this acronym mean?
  Cnps = "CNPS", // Commission spéciale; what does this acronym mean?
  Comnl = "COMNL", // Commission spéciale autre; what does this acronym mean?
  Comper = "COMPER", // Commission permanente
  Comsenat = "COMSENAT", // Commission sénatoriale
  Comspsenat = "COMSPSENAT", // Commission spéciale sénatoriale
  Confpt = "CONFPT", // Conférence des présidents
  Constitu = "CONSTITU", // Conseil constitutionnel
  Deleg = "DELEG", // Délégation
  Delegbureau = "DELEGBUREAU", // Délégation du Bureau
  Delegsenat = "DELEGSENAT", // Délégation sénatoriale
  Ga = "GA", // Groupe d'amitié
  Ge = "GE", // Groupe d'études
  Gevi = "GEVI", // Groupe d'études à vocation internationale
  Gouvernement = "GOUVERNEMENT", // Gouvernement
  Gp = "GP", // Groupe politique
  Groupesenat = "GROUPESENAT", // Groupe sénatorial
  Hcj = "HCJ", // Haute Cour de Justice
  Ministere = "MINISTERE", // Ministère
  Misinfo = "MISINFO", // Mission d'information
  Misinfocom = "MISINFOCOM", // Mission d'information commune
  Misinfopre = "MISINFOPRE", // Mission d'information prélable
  Offpar = "OFFPAR", // Office parlementaire
  Orgaint = "ORGAINT", // Organisme international
  Orgextparl = "ORGEXTPARL", // Organisme extra-parlementaire
  Parpol = "PARPOL", // Parti politique
  Presrep = "PRESREP", // Présidence de la République
  Senat = "SENAT", // Sénat
}

export interface Profession {
  libelleCourant?: string
  socProcInsee: SocProcInsee
}

export interface SocProcInsee {
  catSocPro?: string
  famSocPro?: FamSocPro
}

export enum FamSocPro {
  AgriculteursExploitants = "Agriculteurs exploitants",
  ArtisansCommerçantsEtChefsDEntreprise = "Artisans, commerçants et chefs d'entreprise",
  AutresPersonnesSansActivitéProfessionnelle = "Autres personnes sans activité professionnelle",
  CadresEtProfessionsIntellectuellesSupérieures = "Cadres et professions intellectuelles supérieures",
  Employés = "Employés",
  Ouvriers = "Ouvriers",
  ProfessionsIntermédiaires = "Professions Intermédiaires",
  Retraités = "Retraités",
  SansProfessionDéclarée = "Sans profession déclarée",
}

export interface Organe {
  xsiType?: TypeOrgane
  uid: string
  codeType: CodeTypeOrgane
  libelle: string
  libelleEdition?: string
  libelleAbrege: string
  libelleAbrev: string
  viMoDe: ViMoDe
  organeParentRef?: string // Renamed by Tricoteuses
  organeParent?: Organe // Added by Tricoteuses
  regime?: Regime
  legislature?: string
  secretariat?: Secretariat
  regimeJuridique?: string
  siteInternet?: string
  nombreReunionsAnnuelles?: string
  positionPolitique?: PositionPolitique
  preseance?: string
  couleurAssociee?: string
  listePays?: string[]
}

export enum TypeOrgane {
  GroupePolitiqueType = "GroupePolitique_type",
  OrganeExterneType = "OrganeExterne_Type",
  OrganeExtraParlementaireType = "OrganeExtraParlementaire_type",
  OrganeParlementaireInternational = "OrganeParlementaireInternational",
  OrganeParlementaireType = "OrganeParlementaire_Type",
}

export enum PositionPolitique {
  Majoritaire = "Majoritaire",
  Minoritaire = "Minoritaire",
  Opposition = "Opposition",
}

export enum Regime {
  AssembléeConsultativeProvisoire = "Assemblée consultative provisoire",
  AssembléesNationalesConstituantes = "Assemblées nationales constituantes",
  The4ÈmeRépublique = "4ème République",
  The5ÈmeRépublique = "5ème République",
}

export interface Secretariat {
  secretaire01?: string
  secretaire02?: string
}

export interface ViMoDe {
  dateDebut?: Date
  dateAgrement?: Date
  dateFin?: Date
}

export interface Photo {
  chemin: string
  cheminMosaique: string
  hauteur: number
  largeur: number
  xMosaique: number
  yMosaique: number
}

// Converts JSON strings to/from your types
// and asserts the results of JSON.parse at runtime
export class Convert {
  public static toActeursEtOrganes(json: string): ActeursEtOrganes {
    return cast(JSON.parse(json), r("ActeursEtOrganes"))
  }

  public static acteursEtOrganesToJson(value: ActeursEtOrganes): string {
    return JSON.stringify(uncast(value, r("ActeursEtOrganes")), null, 2)
  }

  public static toActeur(json: string): Acteur {
    return cast(JSON.parse(json), r("Acteur"))
  }

  public static acteurToJson(value: Acteur): string {
    return JSON.stringify(uncast(value, r("Acteur")), null, 2)
  }

  public static toOrgane(json: string): Organe {
    return cast(JSON.parse(json), r("Organe"))
  }

  public static organeToJson(value: Organe): string {
    return JSON.stringify(uncast(value, r("Organe")), null, 2)
  }
}

function invalidValue(typ: any, val: any): never {
  throw Error(`Invalid value ${JSON.stringify(val)} for type ${JSON.stringify(typ)}`)
}

function jsonToJSProps(typ: any): any {
  if (typ.jsonToJS === undefined) {
    var map: any = {}
    typ.props.forEach((p: any) => (map[p.json] = { key: p.js, typ: p.typ }))
    typ.jsonToJS = map
  }
  return typ.jsonToJS
}

function jsToJSONProps(typ: any): any {
  if (typ.jsToJSON === undefined) {
    var map: any = {}
    typ.props.forEach((p: any) => (map[p.js] = { key: p.json, typ: p.typ }))
    typ.jsToJSON = map
  }
  return typ.jsToJSON
}

function transform(val: any, typ: any, getProps: any): any {
  function transformPrimitive(typ: string, val: any): any {
    if (typeof typ === typeof val) return val
    return invalidValue(typ, val)
  }

  function transformUnion(typs: any[], val: any): any {
    // val must validate against one typ in typs
    var l = typs.length
    for (var i = 0; i < l; i++) {
      var typ = typs[i]
      try {
        return transform(val, typ, getProps)
      } catch (_) {}
    }
    return invalidValue(typs, val)
  }

  function transformEnum(cases: string[], val: any): any {
    if (cases.indexOf(val) !== -1) return val
    return invalidValue(cases, val)
  }

  function transformArray(typ: any, val: any): any {
    // val must be an array with no invalid elements
    if (!Array.isArray(val)) return invalidValue("array", val)
    return val.map(el => transform(el, typ, getProps))
  }

  function transformDate(_typ: any, val: any): any {
    if (val === null) {
      return null
    }
    const d = new Date(val)
    if (isNaN(d.valueOf())) {
      return invalidValue("Date", val)
    }
    return d
  }

  function transformObject(props: { [k: string]: any }, additional: any, val: any): any {
    if (val === null || typeof val !== "object" || Array.isArray(val)) {
      return invalidValue("object", val)
    }
    var result: any = {}
    Object.getOwnPropertyNames(props).forEach(key => {
      const prop = props[key]
      const v = Object.prototype.hasOwnProperty.call(val, key) ? val[key] : undefined
      result[prop.key] = transform(v, prop.typ, getProps)
    })
    Object.getOwnPropertyNames(val).forEach(key => {
      if (!Object.prototype.hasOwnProperty.call(props, key)) {
        result[key] = transform(val[key], additional, getProps)
      }
    })
    return result
  }

  if (typ === "any") return val
  if (typ === null) {
    if (val === null) return val
    return invalidValue(typ, val)
  }
  // if (typ === false) return invalidValue(typ, val)
  while (typeof typ === "object" && typ.ref !== undefined) {
    typ = typeMap[typ.ref]
  }
  if (Array.isArray(typ)) return transformEnum(typ, val)
  if (typeof typ === "object") {
    return typ.hasOwnProperty("unionMembers")
      ? transformUnion(typ.unionMembers, val)
      : typ.hasOwnProperty("arrayItems")
      ? transformArray(typ.arrayItems, val)
      : typ.hasOwnProperty("props")
      ? transformObject(getProps(typ), typ.additional, val)
      : invalidValue(typ, val)
  }
  // Numbers can be parsed by Date but shouldn't be.
  if (typ === Date && typeof val !== "number") return transformDate(typ, val)
  return transformPrimitive(typ, val)
}

function cast<T>(val: any, typ: any): T {
  return transform(val, typ, jsonToJSProps)
}

function uncast<T>(val: T, typ: any): any {
  return transform(val, typ, jsToJSONProps)
}

function a(typ: any) {
  return { arrayItems: typ }
}

function u(...typs: any[]) {
  return { unionMembers: typs }
}

function o(props: any[], additional: any) {
  return { props, additional }
}

// function m(additional: any) {
//   return { props: [], additional }
// }

function r(name: string) {
  return { ref: name }
}

const typeMap: any = {
  ActeursEtOrganes: o(
    [
      { json: "organes", js: "organes", typ: a(r("Organe")) },
      { json: "acteurs", js: "acteurs", typ: a(r("Acteur")) },
    ],
    false,
  ),
  Acteur: o(
    [
      { json: "uid", js: "uid", typ: "" },
      { json: "etatCivil", js: "etatCivil", typ: r("EtatCivil") },
      {
        json: "profession",
        js: "profession",
        typ: u(undefined, r("Profession")),
      },
      { json: "uriHatvp", js: "uriHatvp", typ: u(undefined, "") },
      {
        json: "adresses",
        js: "adresses",
        typ: u(undefined, a(u(r("AdresseElectronique"), r("AdressePostale")))),
      },
      { json: "mandats", js: "mandats", typ: u(undefined, a(r("Mandat"))) },
      { json: "photo", js: "photo", typ: u(undefined, r("Photo")) },
    ],
    false,
  ),
  AdresseElectronique: o(
    [
      { json: "xsiType", js: "xsiType", typ: "" },
      { json: "uid", js: "uid", typ: "" },
      { json: "type", js: "type", typ: "" },
      { json: "typeLibelle", js: "typeLibelle", typ: "" },
      { json: "poids", js: "poids", typ: u(undefined, "") },
      {
        json: "adresseDeRattachement",
        js: "adresseDeRattachement",
        typ: u(undefined, ""),
      },
      { json: "valElec", js: "valElec", typ: u(undefined, "") },
    ],
    false,
  ),
  AdressePostale: o(
    [
      { json: "xsiType", js: "xsiType", typ: "" },
      { json: "uid", js: "uid", typ: "" },
      { json: "type", js: "type", typ: "" },
      { json: "typeLibelle", js: "typeLibelle", typ: "" },
      { json: "poids", js: "poids", typ: u(undefined, "") },
      {
        json: "adresseDeRattachement",
        js: "adresseDeRattachement",
        typ: u(undefined, ""),
      },
      { json: "intitule", js: "intitule", typ: u(undefined, "") },
      { json: "numeroRue", js: "numeroRue", typ: u(undefined, "") },
      { json: "nomRue", js: "nomRue", typ: u(undefined, "") },
      {
        json: "complementAdresse",
        js: "complementAdresse",
        typ: u(undefined, ""),
      },
      { json: "codePostal", js: "codePostal", typ: u(undefined, "") },
      { json: "ville", js: "ville", typ: u(undefined, "") },
    ],
    false,
  ),
  EtatCivil: o(
    [
      { json: "ident", js: "ident", typ: r("Ident") },
      { json: "infoNaissance", js: "infoNaissance", typ: r("InfoNaissance") },
      { json: "dateDeces", js: "dateDeces", typ: u(undefined, Date) },
    ],
    false,
  ),
  Ident: o(
    [
      { json: "civ", js: "civ", typ: r("Civ") },
      { json: "prenom", js: "prenom", typ: "" },
      { json: "nom", js: "nom", typ: "" },
      { json: "alpha", js: "alpha", typ: "" },
      { json: "trigramme", js: "trigramme", typ: u(undefined, "") },
    ],
    false,
  ),
  InfoNaissance: o(
    [
      { json: "dateNais", js: "dateNais", typ: Date },
      { json: "villeNais", js: "villeNais", typ: u(undefined, "") },
      { json: "depNais", js: "depNais", typ: u(undefined, "") },
      { json: "paysNais", js: "paysNais", typ: u(undefined, "") },
    ],
    false,
  ),
  Mandat: o(
    [
      { json: "xsiType", js: "xsiType", typ: u(undefined, r("TypeMandat")) },
      { json: "uid", js: "uid", typ: "" },
      { json: "acteurRef", js: "acteurRef", typ: "" },
      { json: "legislature", js: "legislature", typ: u(undefined, "") },
      { json: "typeOrgane", js: "typeOrgane", typ: r("CodeTypeOrgane") },
      { json: "dateDebut", js: "dateDebut", typ: Date },
      {
        json: "datePublication",
        js: "datePublication",
        typ: u(undefined, Date),
      },
      { json: "dateFin", js: "dateFin", typ: u(undefined, Date) },
      { json: "preseance", js: "preseance", typ: u(undefined, "") },
      { json: "nominPrincipale", js: "nominPrincipale", typ: "" },
      { json: "infosQualite", js: "infosQualite", typ: r("InfosQualite") },
      { json: "organesRefs", js: "organesRefs", typ: a("") },
      { json: "suppleant", js: "suppleant", typ: u(undefined, r("Suppleant")) },
      { json: "election", js: "election", typ: u(undefined, r("Election")) },
      { json: "mandature", js: "mandature", typ: u(undefined, r("Mandature")) },
      {
        json: "collaborateurs",
        js: "collaborateurs",
        typ: u(undefined, a(r("Collaborateur"))),
      },
      { json: "libelle", js: "libelle", typ: u(undefined, "") },
      {
        json: "missionSuivanteRef",
        js: "missionSuivanteRef",
        typ: u(undefined, ""),
      },
      {
        json: "missionPrecedenteRef",
        js: "missionPrecedenteRef",
        typ: u(undefined, ""),
      },
    ],
    false,
  ),
  Collaborateur: o(
    [
      { json: "qualite", js: "qualite", typ: r("Civ") },
      { json: "prenom", js: "prenom", typ: "" },
      { json: "nom", js: "nom", typ: "" },
    ],
    false,
  ),
  Election: o(
    [
      { json: "lieu", js: "lieu", typ: r("Lieu") },
      { json: "causeMandat", js: "causeMandat", typ: u(undefined, "") },
      {
        json: "refCirconscription",
        js: "refCirconscription",
        typ: u(undefined, ""),
      },
    ],
    false,
  ),
  Lieu: o(
    [
      { json: "region", js: "region", typ: u(undefined, r("Region")) },
      {
        json: "regionType",
        js: "regionType",
        typ: u(undefined, r("RegionType")),
      },
      { json: "departement", js: "departement", typ: u(undefined, "") },
      { json: "numDepartement", js: "numDepartement", typ: u(undefined, "") },
      { json: "numCirco", js: "numCirco", typ: u(undefined, "") },
    ],
    false,
  ),
  InfosQualite: o(
    [
      { json: "codeQualite", js: "codeQualite", typ: u(undefined, "") },
      { json: "libQualite", js: "libQualite", typ: "" },
      { json: "libQualiteSex", js: "libQualiteSex", typ: u(undefined, "") },
    ],
    false,
  ),
  Mandature: o(
    [
      {
        json: "datePriseFonction",
        js: "datePriseFonction",
        typ: u(undefined, Date),
      },
      { json: "causeFin", js: "causeFin", typ: u(undefined, "") },
      {
        json: "premiereElection",
        js: "premiereElection",
        typ: u(undefined, ""),
      },
      { json: "placeHemicycle", js: "placeHemicycle", typ: u(undefined, "") },
      {
        json: "mandatRemplaceRef",
        js: "mandatRemplaceRef",
        typ: u(undefined, ""),
      },
    ],
    false,
  ),
  Suppleant: o(
    [
      { json: "dateDebut", js: "dateDebut", typ: Date },
      { json: "dateFin", js: "dateFin", typ: u(undefined, Date) },
      { json: "suppleantRef", js: "suppleantRef", typ: "" },
    ],
    false,
  ),
  Profession: o(
    [
      { json: "libelleCourant", js: "libelleCourant", typ: u(undefined, "") },
      { json: "socProcInsee", js: "socProcInsee", typ: r("SocProcInsee") },
    ],
    false,
  ),
  SocProcInsee: o(
    [
      { json: "catSocPro", js: "catSocPro", typ: u(undefined, "") },
      { json: "famSocPro", js: "famSocPro", typ: u(undefined, r("FamSocPro")) },
    ],
    false,
  ),
  Organe: o(
    [
      { json: "xsiType", js: "xsiType", typ: u(undefined, r("TypeOrgane")) },
      { json: "uid", js: "uid", typ: "" },
      { json: "codeType", js: "codeType", typ: r("CodeTypeOrgane") },
      { json: "libelle", js: "libelle", typ: "" },
      { json: "libelleEdition", js: "libelleEdition", typ: u(undefined, "") },
      { json: "libelleAbrege", js: "libelleAbrege", typ: "" },
      { json: "libelleAbrev", js: "libelleAbrev", typ: "" },
      { json: "viMoDe", js: "viMoDe", typ: r("ViMoDe") },
      {
        json: "organeParentRef",
        js: "organeParentRef",
        typ: u(undefined, ""),
      },
      { json: "regime", js: "regime", typ: u(undefined, r("Regime")) },
      {
        json: "legislature",
        js: "legislature",
        typ: u(undefined, ""),
      },
      {
        json: "secretariat",
        js: "secretariat",
        typ: u(undefined, r("Secretariat")),
      },
      {
        json: "regimeJuridique",
        js: "regimeJuridique",
        typ: u(undefined, ""),
      },
      {
        json: "siteInternet",
        js: "siteInternet",
        typ: u(undefined, ""),
      },
      {
        json: "nombreReunionsAnnuelles",
        js: "nombreReunionsAnnuelles",
        typ: u(undefined, ""),
      },
      {
        json: "positionPolitique",
        js: "positionPolitique",
        typ: u(undefined, r("PositionPolitique")),
      },
      { json: "preseance", js: "preseance", typ: u(undefined, "") },
      { json: "couleurAssociee", js: "couleurAssociee", typ: u(undefined, "") },
      {
        json: "listePays",
        js: "listePays",
        typ: u(undefined, a("")),
      },
    ],
    false,
  ),
  Secretariat: o(
    [
      { json: "secretaire01", js: "secretaire01", typ: u(undefined, "") },
      { json: "secretaire02", js: "secretaire02", typ: u(undefined, "") },
    ],
    false,
  ),
  ViMoDe: o(
    [
      { json: "dateDebut", js: "dateDebut", typ: u(undefined, Date) },
      { json: "dateAgrement", js: "dateAgrement", typ: u(undefined, Date) },
      { json: "dateFin", js: "dateFin", typ: u(undefined, Date) },
    ],
    false,
  ),
  Photo: o(
    [
      { json: "chemin", js: "chemin", typ: "" },
      { json: "cheminMosaique", js: "cheminMosaique", typ: "" },
      { json: "hauteur", js: "hauteur", typ: 0 },
      { json: "largeur", js: "largeur", typ: 0 },
      { json: "xMosaique", js: "xMosaique", typ: 0 },
      { json: "yMosaique", js: "yMosaique", typ: 0 },
    ],
    false,
  ),
  Civ: ["M.", "Mme"],
  TypeMandat: [
    "MandatAvecSuppleant_Type",
    "MandatMission_Type",
    "MandatParlementaire_type",
    "MandatSimple_Type",
  ],
  Region: [
    "Auvergne-Rhône-Alpes",
    "Bourgogne-Franche-Comté",
    "Bretagne",
    "Centre-Val de Loire",
    "Corse",
    "Français établis hors de France",
    "Grand Est",
    "Guadeloupe",
    "Guyane",
    "Hauts-de-France",
    "Ile-de-France",
    "Martinique",
    "Mayotte",
    "Normandie",
    "Nouvelle-Aquitaine",
    "Nouvelle-Calédonie",
    "Occitanie",
    "Pays de la Loire",
    "Polynésie française",
    "Provence-Alpes-Côte d'Azur",
    "Réunion",
    "Saint-Barthélemy et Saint-Martin",
    "Saint-Pierre-et-Miquelon",
    "Wallis-et-Futuna",
  ],
  RegionType: [
    "Collectivités d'outre-mer et Nouvelle-Calédonie",
    "Dom",
    "Français établis hors de France",
    "Métropolitain",
  ],
  CodeTypeOrgane: [
    "API",
    "ASSEMBLEE",
    "CJR",
    "CMP",
    "CNPE",
    "CNPS",
    "COMNL",
    "COMPER",
    "COMSENAT",
    "COMSPSENAT",
    "CONFPT",
    "CONSTITU",
    "DELEG",
    "DELEGBUREAU",
    "DELEGSENAT",
    "GA",
    "GE",
    "GEVI",
    "GOUVERNEMENT",
    "GP",
    "GROUPESENAT",
    "HCJ",
    "MINISTERE",
    "MISINFO",
    "MISINFOCOM",
    "MISINFOPRE",
    "OFFPAR",
    "ORGAINT",
    "ORGEXTPARL",
    "PARPOL",
    "PRESREP",
    "SENAT",
  ],
  FamSocPro: [
    "Agriculteurs exploitants",
    "Artisans, commerçants et chefs d'entreprise",
    "Autres personnes sans activité professionnelle",
    "Cadres et professions intellectuelles supérieures",
    "Employés",
    "Ouvriers",
    "Professions Intermédiaires",
    "Retraités",
    "Sans profession déclarée",
  ],
  TypeOrgane: [
    "GroupePolitique_type",
    "OrganeExterne_Type",
    "OrganeExtraParlementaire_type",
    "OrganeParlementaireInternational",
    "OrganeParlementaire_Type",
  ],
  PositionPolitique: ["Majoritaire", "Minoritaire", "Opposition"],
  Regime: [
    "Assemblée consultative provisoire",
    "Assemblées nationales constituantes",
    "4ème République",
    "5ème République",
  ],
}
