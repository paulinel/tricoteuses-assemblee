export interface InfosPageDepute {
  emails: string[]
  pageFacebook?: string
  pageTwitter?: string
  siteWeb?: string
}
